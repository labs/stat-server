===============================
Statistics server documentation
===============================

------------
Installation
------------

Required packages
=================

* Apache 2 (apache2)
* mod-wsgi (libapache2-mod-wsgi)
* PostgreSQL (postgresql) - it is possible to use a different database supported
  by Django, such as MySQL or Oracle, but it was not tested and is not
  supported.
* Psycopg 2 (python-psycopg2)
* Python setuptools (python-setuptools)
* Memcached (memcached) - needed for Memcached cache backend
* Python memcache library (python-memcache) - needed for Memcached cache backend
* Python YAML library (python-yaml)
* Python markdown library (python-markdown)
* Python dateutil library (python-dateutil)

All the packages can be installed on Ubuntu using the following command as root::

  apt-get install apache2 libapache2-mod-wsgi postgresql python-psycopg2 \
  python-setuptools memcached python-memcache python-yaml python-markdown \
  python-dateutil
  
Django and django-related packages:

* Django (requires version >=1.5.1 to support django-mojeid, version >=1.6 is not tested)
* South Django migrations library (python-django-south)
* django-transmeta - not in repositories, must be installed otherwise (pip, easy_install,...)
* django-guardian
* django-mojeid

Unfortunatelly Django is too old in Ubuntu LTS release repositories
and should be installed from python sources using either easy_install, pip
or manually. As a consequence, none of its dependencies can be installed from system apt repository,
but the same way as django itself.

All this can be installed using (as root)::

  pip install 'django<1.6' south django-transmeta
  
You can alternatively install all Python-related requirements from PyPI repository::

  pip install -r requirements.txt

Or the same with packages for development::

  pip install -r requirements-devel.txt

Furthermore, it uses ogra javascript library, which can be downloaded from
https://gitlab.labs.nic.cz/labs/ogra (ogra.js file) and placed into
unifiedstatistics/static/js/ogra/ directory. This is only needed when you
obtained the project by cloning git repo. Simply run following commands::

  git submodule init
  git submodule update

Settings file
=============

The directory ``stat_server/settings`` contains a file called
``development.py.example``. In order to set the server up for development,
rename this file to ``development.py``::

  cd stat_server/settings
  cp development.py.example development.py

Edit the file ``development.py`` using your favorite editor. The file
contains hints about the purpose of individual settings.


Database initialization
========================

Once the settings for the database are ready, you can initialize the database
using::

  python manage.py syncdb
  python manage.py migrate guardian
  python manage.py migrate unifiedstatistics

You will be asked for the admin name, email and password. These values will be
used when accessing the administrative interface.

When the database is initialized, it is necessary to import basic data type,
statistics data and menu items into the system. To do so, run the following commands::

  python manage.py load_type_defs data/data_types.json
  python manage.py load_stats_defs data/statistics.yaml
  python manage.py load_reports_defs data/reports.yaml
  python manage.py load_group_perms data/permissions.yaml
  python manage.py loaddata unifiedstatistics/fixtures/dashboard.json
  python manage.py update_menu

Import of some data
===================

Obtain a dump of CSV data, extract it into the directory set up in
``settings_local.py`` as ``INCOMING_DATA_DIR`` and run::

  python manage.py update_db


Testing the setup
=================

After installing the application and setting up the database, one can verify
if everything has been set up properly by running unittest test suite
To do so run the following command::
  
  python manage.py test unifiedstatistics

.. note:: Running whole test suite for all installed django applications
          (`python manage.py test`) is discouraged since you may get errors
          and failures from installed middleware classes (on django 1.4.5,
          python 2.7.3 I got 4 failures and 25 errors)


You can run run a development server as well. To do so, in the command line type::

  python manage.py runserver

It will start an HTTP server at http://localhost:8000 to which you can connect.


Deployment
==========

For production usage, install stat_server using setup.py (with root priviledges)::

  python setup.py install

Configuration files are placed into ``/etc/stat_server/settings``. To setup production
version rename file ``production.py.example`` to ``production.py`` inside this directory
and edit this file to match your needs.

To integrate stat_server with Apache2 using mod_wsgi there is example configuration file
``/etc/stat_server/apache2.conf``. Include this file with::

   Include /etc/stat_server/apache2.conf
   
into your Apache configuration file, in a virtual host section. Reload Apache configuration and visit
``http://localhost/stats`` in your web broswer. Note: this configuration needs following settings
in your ``production.py`` file::

  STATIC_URL= "/stats/static/"
  MEDIA_URL= "/stats/media/"

To update database with incoming CSV files, run the following command regularly from cron (incoming directory
defined in your configuration file must exist)::

   django-admin.py update_db --settings=settings.production --pythonpath=/etc/stat_server

