# encoding: utf-8
# built-in modules
from __future__ import print_function
import datetime
import logging
import cPickle as pickle
import zlib
import locale

# django imports
from django.utils.translation import ugettext as _
from django.db.models import Max, Min, Sum
from django.utils import translation
from django.core.cache import cache
from django.conf import settings

# local imports
from models import DataType, Dimension, DataPoint, DimensionToDataType
from common import DateManipulation, Consts, create_cache_key
from aggregation import Aggregator

logger = logging.getLogger("unifiedstatistics.data_mining")

class DataRetreivalException(Exception):
    pass


class DataQuery(object):
    """
    This class is responsible for fetching and processing data from the database
    based on the input parameters
    """
    
    TIME_AGGREG_VALUES = (Consts.TIME_AGGREG_NONE,
                          Consts.TIME_AGGREG_LAST,
                          Consts.TIME_AGGREG_AVG,
                          Consts.TIME_AGGREG_MIN,
                          Consts.TIME_AGGREG_MAX,
                          Consts.TIME_AGGREG_DIFF,
                          Consts.TIME_AGGREG_DIFF_REL,
                          Consts.TIME_AGGREG_SUM,
                          )
    

    def __init__(self, data_type, from_date, to_date,
                 fixed_values=None,
                 time_aggregate=Consts.TIME_AGGREG_NONE,
                 series_num_limit=None,
                 time_sampling=None,
                 grouped_dims=None,
                 relative_scale_base=None):
        """
        fixed_value - list of length = len(dimensions) with fixed values; 
                      this reduces the effective number of dimensions
        time_aggregate - how to aggregate in time dimension
        series_num_limit - limit for number of series
        time_sampling - if data should be sampled in the time dimension
                        and processed using the aggregation function
        grouped_dims - list of grouped dimensions 
        relative_scale_base - dimension used as base when calculating relative shares
        """
        self._preprocessed = False
        self.set_params(data_type=data_type)
        self.dimensions = self.data_type.get_dimensions()
        self.set_params(from_date=from_date)
        self.set_params(to_date=to_date)
        self.set_params(time_aggregate=time_aggregate,
                        time_sampling=time_sampling)
        self.set_params(fixed_values=fixed_values)
        self.set_params(grouped_dims=grouped_dims)
        self.set_params(relative_scale_base=relative_scale_base)
        
        
    def set_params(self, **kwargs):
        for key, value in kwargs.iteritems():
            # do checks
            if key == 'data_type':
                assert(isinstance(value, DataType))
            elif key == 'from_date':
                assert(value is None or \
                       isinstance(value, datetime.datetime) or \
                       isinstance(value, datetime.date))
            elif key == 'to_date':
                assert(value is None or \
                       isinstance(value, datetime.datetime) or \
                       isinstance(value, datetime.date))
            elif key == 'fixed_values':
                if value is not None:
                    assert(type(value) in (list, tuple))
                    assert(len(value) == len(self.dimensions))
            elif key == 'grouped_dims':
                if value is not None:
                    assert(type(value) in (list, tuple))
                    assert(len(value) == len(self.dimensions))
            elif key == "time_aggregate":
                assert(value in self.TIME_AGGREG_VALUES)
            elif key == "time_sampling":
                if value is not None:
                    assert(type(value) in (int, str, unicode))
                    if type(value) in (str, unicode):
                        # it must be properly formatted
                        if not DateManipulation.DATE_RANGE_MATCHER.match(value):
                            raise ValueError("Wrong format of time_sampling.")
            elif key == "relative_scale_base":
                assert(value is None or \
                       isinstance(value, Dimension))
            else:
                raise ValueError("Unknown parameter '%s'='%s'" % (key, value))
            setattr(self, key, value)

            
    def preprocess_input(self):
        """
        Takes the input and prepares it for direct usage in queries,
        does value remapping, etc.
        """
        if self.fixed_values:
            fixed_ints = []
            for i, value in enumerate(self.fixed_values):
                if value is not None and type(value) is not int:
                    # int value is presumably already remapped
                    dim = self.dimensions[i]
                    int_val = dim.value_to_int(value)
                else:
                    int_val = value
                fixed_ints.append(int_val)
            self.fixed_values = fixed_ints
        else:
            self.fixed_values = len(self.dimensions) * [None]
        self._preprocessed = True
        
    def _adjust_date(self, date, query_base):
        # get nearest lower value
        try:
            before = query_base.filter(date__lte=date).values("date")\
                                                        .order_by("-date")[0]
        except IndexError:
            before = None
        else:
            before = before['date'].date()
        # get nearest higher value
        try:
            after = query_base.filter(date__gte=date).values("date")\
                                                        .order_by("date")[0]
        except IndexError:
            after = None
        else:
            after = after['date'].date()
        # based on what we got decide what to use
        if not before:
            return after
        elif not after:
            return before
        elif (date-before) < (after-date):
            return before
        else:
            return after 
        
            
    def _fetch_from_db_or_cache(self, queryset):
        cache_key = create_cache_key("db_data", queryset.query)
        ret = cache.get(cache_key)
        if not ret:
            ret = list(queryset)
            # we compress the data because it sometimes goes over the limit
            # of memcached
            cache.set(cache_key, zlib.compress(pickle.dumps(ret, protocol=-1)))
        else:
            ret = pickle.loads(zlib.decompress(ret))
        return ret
    
            
    def fetch_results(self):
        """
        Does the actual querying and data processing
        """
        if not self._preprocessed:
            self.preprocess_input()
        order = None
        nonfixed_dimensions = len(self.dimensions) - len([x for x in self.fixed_values if x]) - len([x for x in self.grouped_dims if x])
        if self.from_date == self.to_date and (nonfixed_dimensions > 0):
            axes = ["_amount"]
            returned_fields = ["amount"]
        else:
            axes = ["_time","_amount"]
            returned_fields = ["date","amount"]
            order = "date"

        limits = {}
        for i, dim in enumerate(self.dimensions):
            attr_name = "value%d" % (i+1)
            fixed_value = self.fixed_values[i]
            if fixed_value is not None:
                # if it is fixed, it belongs to limits, not output
                limits[attr_name] = fixed_value
            elif not self.grouped_dims[i]:
                # this value will be part of the output
                axes.append(dim)
                returned_fields.append(attr_name)
                # order by first dimension if there is no date axis
                if not order:
                    order = attr_name

        # fallback, if order is still not set
        if not order:
            order = "amount" 
        base_data = []
        # time aggregation
        if self.time_sampling is None and \
           self.time_aggregate in (Consts.TIME_AGGREG_DIFF,
                                   Consts.TIME_AGGREG_DIFF_REL):
            # we do not need to make special query for the relative shares, as
            # relative scale is incompatible with Consts.TIME_AGGREG_DIFF and
            # Consts.TIME_AGGREG_DIFF_REL options
            
            # base for the query
            query = self.data_type.datapoint_set.order_by(order)
            if not self.from_date or not self.to_date:
                # we need to obtain the from data from the database
                dates = DataPoint.objects.filter(data_type=self.data_type)\
                        .aggregate(min_date=Min('date'), max_date=Max('date'))
            if not self.from_date:
                if dates['min_date']:
                    self.from_date = dates['min_date'].date()
                else:
                    raise DataRetreivalException("No data for data_type")
            if not self.to_date:
                if dates['max_date']:
                    self.to_date = dates['max_date'].date()
                else:
                    raise DataRetreivalException("No data for data_type")
            # we query both dates separately and attempt to fix the date if
            # it is not available
            query_base = query.filter(**limits)
            query = query_base.values_list(*returned_fields)
            start_data = list(query.filter(date=self.from_date))
            if not start_data:
                self.from_date = self._adjust_date(self.from_date, query_base)
                start_data = list(query.filter(date=self.from_date))
            end_data = list(query.filter(date=self.to_date))
            if not end_data:
                self.to_date = self._adjust_date(self.to_date, query_base)
                end_data = list(query.filter(date=self.to_date))
            result = start_data + end_data
        else:
            # base for the query
            if self.time_sampling in ("1m", "1y"):
                query = self.data_type.datapointaggreg_set\
                                    .filter(time_period="1m").order_by(order)
            else:
                query = self.data_type.datapoint_set.order_by(order)
            if self.from_date:
                limits['date__gte'] = self.from_date
            if self.to_date:
                limits['date__lte'] = self.to_date
            # determine if separate query for the relative shares base is necessary
            if self.relative_scale_base and (not self.relative_scale_base in axes):
                # make query the same, just sum data point values for all different
                # relative_scale_base and ignore fixed value for it
                base_query = query.all()
                base_attr_name = "value%d" % (self.dimensions.index(self.relative_scale_base) + 1)
                limits_base = limits.copy()
                if base_attr_name in limits_base:
                    limits_base.pop(base_attr_name)
                base_query = base_query.filter(**limits_base)
                base_values = returned_fields[:]
                base_values.pop(returned_fields.index('amount'))
                base_query = base_query.values(*base_values).annotate(amount=Sum('amount'))
                list_base_query = base_query.values_list(*returned_fields)
                base_data = self._fetch_from_db_or_cache(list_base_query)
            query = query.filter(**limits)
            list_query = query.values_list(*returned_fields)
            result = self._fetch_from_db_or_cache(list_query)
        # return the result as a DataBundle instance
        kwargs = dict(amount_dim=self.data_type.amount_dim,
                      data_time_aggreg=self.time_aggregate,
                      time_sampling=self.time_sampling,
                      base_data=base_data)
        if len(axes) == 2:
            bundle = DataBundle2d(axes, result, **kwargs)
            if self.time_sampling is None and \
               self.time_aggregate in (Consts.TIME_AGGREG_DIFF,
                                       Consts.TIME_AGGREG_DIFF_REL):
                bundle = bundle.transform(self.time_aggregate)
        elif len(axes) == 3:
            bundle = DataBundle3d(axes, result, **kwargs)
            if self.time_sampling is None and \
               self.time_aggregate in (Consts.TIME_AGGREG_DIFF,
                                       Consts.TIME_AGGREG_DIFF_REL):
                bundle = bundle.transform(self.time_aggregate)
        else:
            raise NotImplementedError("Not yet implemented")
        return bundle
    
    
class DataBundle(object):
    
    def __init__(self, dimensions, data, amount_dim=None,
                 data_time_aggreg=Consts.TIME_AGGREG_NONE, time_sampling=None, base_data=None):
        self.raw_data = data
        self.dimensions = dimensions
        self.amount_dim = amount_dim
        self.data_time_aggreg = data_time_aggreg
        self.time_sampling = time_sampling
        self.data = []
        self.series_dim = None
        self.key_dim = None
        self.raw_base_data = base_data
        
        
    def __str__(self):
        return unicode(self).encode('utf-8')
        
    def __unicode__(self):
        dim_num = len(self.dimensions)
        rec_num = len(self.data)
        dims = u",".join([unicode(x) for x in self.dimensions])
        return u"{0:d} {1:d}D records; dimensions: {2}".format(rec_num, dim_num,
                                                               dims)
        
    def _remap_values(self):
        if isinstance(self.series_dim, Dimension) and \
           self.series_dim.type == Dimension.TYPE_TEXT:
            series_remap = self.series_dim.\
                get_int_to_str_and_custom_values_remap(
                                                lang=translation.get_language())
        else:
            series_remap = {}
        series_remap["other"] = (_("Other"), "", 0)
        # main key remap
        if isinstance(self.key_dim, Dimension) and \
           self.key_dim.type == Dimension.TYPE_TEXT:
            key_remap = {"other": (_("Other"), "", -255)}
            key_remap.update(self.key_dim.get_int_to_str_and_custom_values_remap(
                                                lang=translation.get_language()))
        elif isinstance(self.key_dim, Dimension):
            # key dimension is TYPE_INT, dont convert ints to str for correct sorting
            key_remap = {"other": (_("Other"), "", -255)}
        else:
            # key dimension is time, no remapping
            key_remap = None
        # do the remap if self.data is iterable, otherwise there just single value
        try:
            new_data = []
            for main_key, record in self.data:
                new_record = {}
                if key_remap and self.key_dim.type == Dimension.TYPE_TEXT:
                    new_main_key = key_remap.get(main_key, (str(main_key), "", 0))
                elif key_remap and self.key_dim.type != Dimension.TYPE_TEXT:
                    new_main_key = key_remap.get(main_key, (main_key, "", 0))
                else:
                    # time 
                    new_main_key = main_key
                
                for key, value in record.iteritems():
                    # for dimensions with TYPE_INT series_remap dictionary has just one item `other`
                    # keys None has special meaning and are not translated to triads (key, color, order)
                    new_record[series_remap.get(key, (str(key), "", 0) if key else key)] = value
                new_data.append((new_main_key, new_record))
            self.data = new_data
        except TypeError:
            # single value, for example {None: 2494.7621}, no need to remap
            pass
    
    def _sort_data(self, sorting=None):
        """Sort data if the key dimension is not time and sorting is defined in
        DimensionToDataType connection. 
        """
        pass
            
    @classmethod
    def group_data(cls, records, grouping_idx, value_idx):
        """
        Input data must be sorted by grouping_idx.
        
        Groups raw_data and raw_base_data (results of query for datapoint_set
        or datapointaggreg_set in DataQuery.fetch_results) by non _amount
        dimension in case of DataBundle2d or by time dimension in case of
        DataBundle3d. Used to initialize data from raw_data and raw_base_data
        before postprocessing.
        
        Example result for 2d data without time dimension:
        [(194, {None: 16628L}),
         (195, {None: 10538L}),
         (196, {None: 6522L}),
         (197, {None: 3454L}),
         (200, {None: 3016L})]
        
        Example result for 3d data:
        [(datetime.datetime(2012, 11, 1, 0, 0),
          {269: 37876L, 270: 323079L, 271: 182273L, 272: 440501L}),
         (datetime.datetime(2013, 1, 1, 0, 0),
          {269: 40391L, 270: 273054L, 271: 112306L, 272: 571170L}),
         (datetime.datetime(2013, 2, 1, 0, 0),
          {269: 38927L, 270: 260961L, 271: 113074L, 272: 601235L})]         
        """
        if not records:
            raise StopIteration
        series_idxs = []
        if len(records[0]) > 2:
            for i in range(len(records[0])):
                if i != grouping_idx and i != value_idx:
                    series_idxs.append(i)
        last_grouping_value = None
        row = {}
        for record in records:
            grouping_value = record[grouping_idx]
            value_value = record[value_idx]
            if len(series_idxs) == 0:
                series_value = None
            elif len(series_idxs) == 1:
                series_value = record[series_idxs[0]]
            else:
                series_value = tuple([record[idx] for idx in series_idxs])
            if not last_grouping_value:
                last_grouping_value = grouping_value
            if grouping_value != last_grouping_value:
                yield (last_grouping_value, row)
                last_grouping_value = grouping_value
                row = {}
            # sum entries with same grouping value - this occurs because
            # of slicing dimension set to DataRequest.GROUPED_DIM_VALUE 
            if series_value not in row:
                row[series_value] = 0
            row[series_value] += value_value
        yield (last_grouping_value, row)
        
        
    def postprocess(self, limit_series=0, series_set=None, create_other=None,
                    scale=Consts.SCALE_NORMAL, sorting=None):
        """
        This method references some internal methods that have to be 
        overridden in a child
        """
        self._postprocess_sampling(sampling=self.time_sampling)
        self._postprocess_scale(scale=scale)
        self._postprocess_limit_series(limit_series=limit_series,
                                       series_set=series_set,
                                       create_other=create_other)
        self._remap_values()
        self._sort_data(sorting=sorting)
        

    def _postprocess_limit_series(self, limit_series=0, series_set=None,
                                  create_other=None):
        pass
    
    
    def _postprocess_scale(self, scale=Consts.SCALE_NORMAL):
        pass


    def _postprocess_sampling(self, sampling=None):
        """
        """
        if self.key_dim != "_time" or sampling is None:
            # no sampling for data not depending on time
            # or if there is not sampling method
            return
        group_breaks = DateManipulation.get_time_breaker_fn(sampling)
        aggreg_fn = Aggregator.get_aggregate_fn(self.data_time_aggreg)
        for dataset_name in ['data', 'base_data']:
            grouped = {}
            last_main_key = None
            new_data = []
            dataset = getattr(self, dataset_name)
            for main_key, record in dataset:
                if last_main_key is None or group_breaks(last_main_key, main_key):
                    new_record = {}
                    for key, values in grouped.iteritems():
                        new_value = aggreg_fn(values)
                        new_record[key] = new_value
                    if new_record:
                        new_data.append((last_main_key, new_record))
                    grouped = {}
                for key, value in record.iteritems():
                    if key not in grouped:
                        grouped[key] = []
                    grouped[key].append(value)
                last_main_key = main_key
            if grouped:
                new_record = {}
                for key, values in grouped.iteritems():
                    new_value = aggreg_fn(values)
                    new_record[key] = new_value
                if new_record:
                    new_data.append((last_main_key, new_record))
            setattr(self, dataset_name, new_data)

        
    def gen_data_table(self):
        """
        Generator for individual rows of a data table, should be overriden
        in children
        """
        return None
    
    def get_stored_time_range(self):
        if self.key_dim != "_time":
            return None, None
        if not self.data:
            return None, None
        else:
            return self.data[0][0], self.data[-1][0]

    
    def get_series_description(self):
        """
        Should return a list of series in form (name, color, order) and a
        dictionary with a DataTable compatible description of the series
        Should be implemented in a child.
        
        Works with remaped values in self.data (managed by self._remap_values
        called from self.postprocess).
        """
        return None, None
    
    def transform(self, transformation):
        """
        Performs a transformation on the data and returns a new DataBundle
        with the transformed data
        """
        return None
    
    def _get_time_dim_description(self):
        """
        A common place for children to get description of a time dimension
        based on sampling method
        """
        if not self.time_sampling:
            return ("date", _('Time'))
        elif self.time_sampling.endswith("m"):
            return ("string", _('Month'))
        elif self.time_sampling.endswith("y"):
            return ("string", _('Year'))
        else:
            raise ValueError("Unknown sampling '%s'" % self.time_sampling)
        
    def _format_main_key(self, key):
        if isinstance(key, datetime.date) or isinstance(key, datetime.datetime):
            if self.time_sampling:
                return DateManipulation.str_date(key, 
                                            rounding=self.time_sampling[-1])
        return key


class DataBundle1d(DataBundle):
    """
    DataBundle specialized for 1D data
    """
    
    def __init__(self, dimensions, data, amount_dim=None,
                 data_time_aggreg=Consts.TIME_AGGREG_NONE,
                 time_sampling=None, base_data=None):
        super(DataBundle1d, self).__init__(dimensions, data,
                                           amount_dim=amount_dim,
                                           data_time_aggreg=data_time_aggreg,
                                           time_sampling=time_sampling,
                                           base_data=base_data)
        if len(self.dimensions) != 1:
            raise Exception("This class can handle only 1D data, use a "
                            "different DataBundle subclass for %dD data" %\
                            len(self.dimensions))
        if not "_amount" in self.dimensions:
            raise Exception("There must be an amount dimension "
                            "between dimensions")
        self.idx_amount = 0
        self.data = dict(data)
        try:
            self.base_data = dict(base_data)
        except TypeError:
            pass
        self.series_dim = None
    
    
    def gen_data_table(self):
        amount = self.data[None]
        yield {"_amount": amount, "_main_key": ""}

    
    def get_series_description(self):
        description = {}
        if isinstance(self.amount_dim, Dimension):
            amount_name = self.amount_dim.name
        else:
            amount_name = self.amount_dim or _("Amount")
        description["_amount"] = ('number', amount_name)
        description["_main_key"] = ('string', "")
        return None, description
    

class DataBundle2d(DataBundle):
    """
    DataBundle specialized for 2D data
    """
    
    def __init__(self, dimensions, data, amount_dim=None,
                 data_time_aggreg=Consts.TIME_AGGREG_NONE,
                 time_sampling=None, base_data=None):
        super(DataBundle2d, self).__init__(dimensions, data,
                                           amount_dim=amount_dim,
                                           data_time_aggreg=data_time_aggreg,
                                           time_sampling=time_sampling,
                                           base_data=base_data)
        if len(self.dimensions) != 2:
            raise Exception("This class can handle only 2D data, use a "
                            "different DataBundle subclass for %dD data" %\
                            len(self.dimensions))
        if not "_amount" in self.dimensions:
            raise Exception("There must be an amount dimension "
                            "between dimensions")
        self.idx_amount = self.dimensions.index("_amount")
        self.idx_var = 1 if self.idx_amount == 0 else 0
        self.data = list(self.group_data(self.raw_data, self.idx_var,
                                         self.idx_amount))
        self.base_data = list(self.group_data(self.raw_base_data, self.idx_var,
                                              self.idx_amount))
        self.series_dim = None
        self.key_dim = self.dimensions[self.idx_var]
    
    
    def _postprocess_limit_series(self, limit_series=0, series_set=None,
                                  create_other=None):
        # time dependent data is never limited in series
        if self.key_dim != "_time" and (series_set or limit_series > 0):
            # prepare some parameters
            if create_other is False or \
               self.data_time_aggreg == Consts.TIME_AGGREG_DIFF_REL:
                # in case of relative difference we do not compute other
                # as it would not make sense
                series_num = limit_series
                do_other = False
            else:
                series_num = limit_series-1
                do_other = True
            # check the data
            series_sums = {}
            for main_key, record in self.data:
                value = record[None]
                series_sums[main_key] = series_sums.get(main_key, 0) + value
            all_series = set(series_sums.keys())
            if not series_set and len(all_series) > limit_series:
                # no preselection, but more series than limit
                series_set = set(series_sums.keys())
            if series_set and \
               (limit_series == 0 or len(series_set) > len(all_series)):
                do_other = False
                series_num = limit_series
            # else the series_set remains None
            # if we do not have a series_set, we can short-circuit, otherwise
            # we do the work
            if series_set:
                if limit_series > 0 and len(series_set) > limit_series:
                    # do not bother if we are in the limit
                    to_sort = [(series_sums[series], series)
                               for series in series_set]
                    to_sort.sort(reverse=True)
                    allowed_series = set([series for val_sum,series
                                          in to_sort[:series_num]])
                else:
                    allowed_series = series_set
                # the actual data filtering
                new_data = []
                other = 0
                for main_key, record in self.data:
                    if main_key in allowed_series:
                        new_data.append((main_key, record))
                    else:
                        other += record[None]
                if do_other:
                    new_data.append(("other", {None: other}))
                self.data = new_data
                

    def _postprocess_scale(self, scale=Consts.SCALE_NORMAL):
        """
        We distinct two different cases - in basic case shares are calculated
        relative to sum of values in current series.
        
        The second case is when model ChartParamSet uses relative_scale_base
        option to change this calculation to some other dimension.
        In this case bundle should have received option self.base_data
        in init and transform it with group_data equally as self.data.
        Shares are then computed relative to the corresponding value in
        self.base_data.
        """
        if scale in (Consts.SCALE_RELATIVE, Consts.SCALE_RELATIVE_100):
            # relative scaling is the same in both cases
            if scale == Consts.SCALE_RELATIVE_100:
                factor = 100.0
                round_to = 4
            else:
                factor = 1.0
                round_to = 6
                
            new_data = []
            if self.base_data:
                # use special set of data for calculation of relative share
                for i, (main_key, record) in enumerate(self.data):
                    base_value = self.base_data[i][1][None]
                    if base_value != 0:
                        value = round(factor*record[None]/base_value, round_to)
                        new_data.append((main_key, {None: value}))
                self.data = new_data
            elif self.key_dim == "_time":
                # this would not work with time dependent data
                for main_key, record in self.data:
                    new_data.append((main_key, {None: factor}))
                self.data = new_data
            else:
                # calculation of relative share using current series
                total = 0
                for main_key, record in self.data:
                    total += record[None]
                if total > 0:
                    new_data = []
                    for main_key, record in self.data:
                        value = round(factor*record[None]/total, round_to)
                        new_data.append((main_key, {None: value}))
                    self.data = new_data

    def gen_data_table(self):
        for main_key, rec in self.data:
            amount = rec[None]
            yield {"_main_key": self._format_main_key(main_key),
                   "_amount": amount}

    
    def get_series_description(self):
        description = {}
        """
        series_names = set()
        series = set()
        for main_key, record in self.data:
            series_names |= set([key[0] for key in record.keys()])
            series |= set(record.keys())
        """
        if isinstance(self.amount_dim, Dimension):
            amount_name = self.amount_dim.name
        else:
            amount_name = self.amount_dim or _("Amount")
        description["_amount"] = ('number', amount_name)
        var_dim = self.dimensions[self.idx_var]
        if var_dim == '_time':
            description["_main_key"] = self._get_time_dim_description()
        else:
            # description["_main_key"] = (var_dim.get_type_string(), var_dim.name)
            description["_main_key"] = ("string", var_dim.name)
        return None, description
    
    def _sort_data(self, sorting=None):
        """Sorting only when time dimension is not present"""
        if isinstance(self.key_dim, Dimension) and (sorting != None):
            try:
                locale_name = settings.LANGUAGE_LOCALES.get(translation.get_language())
                locale.setlocale(locale.LC_ALL, locale_name)
            except locale.Error as e:
                logger.warning("Locale error: %s", e.message)
            if sorting == DimensionToDataType.ORDER_BY_AMOUNT_ASC:
                self.data.sort(key=lambda x: (-x[0][2],
                                              x[1][None]))
            elif sorting == DimensionToDataType.ORDER_BY_AMOUNT_DESC:
                self.data.sort(key=lambda x: (x[0][2],
                                              x[1][None]),
                               reverse=True)
            elif sorting == DimensionToDataType.ORDER_BY_LABEL_ASC:
                self.data.sort(key=lambda x: (-x[0][2],
                                              locale.strxfrm(x[0][0].lower().encode('utf-8')) if hasattr(x[0][0], 'lower') else x[0][0]))
            elif sorting == DimensionToDataType.ORDER_BY_LABEL_DESC:
                self.data.sort(key=lambda x: (x[0][2],
                                              locale.strxfrm(x[0][0].lower().encode('utf-8')) if hasattr(x[0][0], 'lower') else x[0][0]),
                               reverse=True)
        if isinstance(self.key_dim, Dimension):
            # we need to remove auxiliary information from key, that are useful only for sorting
            data_without_ordering_info = map(lambda x: (x[0][0], x[1]), self.data)
            self.data = data_without_ordering_info
    
    def transform(self, transformation):
        """
        Performs a transformation on the data and returns a new DataBundle
        with the transformed data
        """
        if transformation in (Consts.TIME_AGGREG_DIFF,
                              Consts.TIME_AGGREG_DIFF_REL):
            if len(self.data) < 2:
                raise DataRetreivalException(
                    "Cannot do difference with %d intervals" % len(self.data))
            elif len(self.data) > 2:
                logging.warn("Ineffective difference with %d time points" %\
                             len(self.data))
            start = self.data[0][1]
            end = self.data[-1][1]
            result = []
            for key, value1 in start.iteritems():
                value2 = end.get(key)
                if value2 is not None:
                    if transformation == Consts.TIME_AGGREG_DIFF:
                        result.append((key, value2-value1))
                    elif transformation == Consts.TIME_AGGREG_DIFF_REL:
                        if value1 != 0:
                            # ignore values starting with 0 at day 1
                            result.append((key,
                                           round(100.0*(value2-value1)/value1, 4)))
            if transformation == Consts.TIME_AGGREG_DIFF:
                amount_dim = self.amount_dim
            elif transformation == Consts.TIME_AGGREG_DIFF_REL:
                amount_dim = self.amount_dim
                #amount_dim = _("Percent difference")
            return DataBundle1d([self.dimensions[self.idx_amount]],
                                 result,
                                 amount_dim=amount_dim,
                                 data_time_aggreg=transformation)
    

class DataBundle3d(DataBundle):
    """
    DataBundle specialized for 3D data
    """
    
    def __init__(self, dimensions, data, amount_dim=None,
                 data_time_aggreg=Consts.TIME_AGGREG_NONE,
                 time_sampling=None, base_data=None):
        super(DataBundle3d, self).__init__(dimensions, data,
                                           amount_dim=amount_dim,
                                           data_time_aggreg=data_time_aggreg,
                                           time_sampling=time_sampling,
                                           base_data=base_data)
        if len(self.dimensions) != 3:
            raise Exception("This class can handle only 3D data, use a "
                            "different DataBundle subclass for %dD data" %\
                            len(self.dimensions))
        self.idx_series = None
        self.idx_time = None
        self.idx_amount = None
        self.series_dim = None
        self.key_dim = None
        for i, dim in enumerate(self.dimensions):
            if isinstance(dim, Dimension):
                self.idx_series = i
                self.series_dim = dim
            elif dim == "_time":
                self.idx_time = i
                self.key_dim = "_time"
            elif dim == "_amount":
                self.idx_amount = i
            else:
                raise Exception("Unknown dimension '%s'" % dim)
        if self.idx_series is None:
            raise Exception("There must be at one Dimension instance "
                            "between dimensions")
        if self.idx_time is None:
            raise Exception("There must be a time dimension "
                            "between dimensions")
        if self.idx_amount is None:
            raise Exception("There must be an amount dimension "
                            "between dimensions")
        self.data = list(self.group_data(self.raw_data, self.idx_time,
                                         self.idx_amount))
        self.base_data = list(self.group_data(self.raw_base_data, self.idx_time,
                                              self.idx_amount))
        

    def __unicode__(self):
        dim_num = len(self.dimensions)
        rec_num = len(self.data)
        dims = u",".join([unicode(x) for x in self.dimensions])
        return u"{0:d} {1:d}D records; dimensions: {2}".format(rec_num, dim_num,
                                                               dims)


    def _postprocess_limit_series(self, limit_series=0, series_set=None,
                                  create_other=None):
        """
        Does value remapping, etc.
        limit_series - 0 means all series, anything else means X series with
        series_set - defines a set of allowed series values, rest is discarded
        """
        if series_set or limit_series > 0:
            if series_set:
                series_set = set(series_set)
            do_other = True
            # lets find all the cases where we do not want to count 'other'
            if create_other is False or \
               self.data_time_aggreg == Consts.TIME_AGGREG_DIFF_REL:
                # in case of relative difference we do not compute other
                # as it would not make sense
                do_other = False
            # prepare some parameters
            if do_other:
                series_num = limit_series-1
            else:
                series_num = limit_series
            # check the data
            series_sums = {}
            for main_key, record in self.data:
                for key, value in record.iteritems():
                    series_sums[key] = series_sums.get(key, 0) + value
            usable_series = set(series_sums.keys())
            if series_set:
                # we discard things in series set which are not in usable_series 
                series_set &= usable_series
                usable_series = series_set
            if not usable_series:
                # no usable series - probably no data for the current selection
                self.data = []
                return
            if not series_set and len(usable_series) > limit_series:
                # no preselection, but more series than limit
                series_set = usable_series
            # else the series_set remains None
            # if we do not have a series_set, we can short-circuit, otherwise
            # we do the work
            if series_set:
                if limit_series > 0 and len(series_set) > limit_series:
                    # do not bother if we are in the limit
                    to_sort = [(series_sums[series], series)
                               for series in series_set]
                    to_sort.sort(reverse=True)
                    allowed_series = set([series for _val_sum,series
                                          in to_sort[:series_num]])
                else:
                    allowed_series = series_set
                    do_other = False
                # the actual data filtering
                new_data = []
                for main_key, record in self.data:
                    new_record = {}
                    other = 0
                    for key, value in record.iteritems():
                        if key in allowed_series:
                            new_record[key] = value
                        elif key in usable_series:
                            other += value
                        else:
                            # we discard this value
                            pass
                    if do_other:
                        new_record["other"] = other
                    new_data.append((main_key, new_record))
                self.data = new_data


    def _postprocess_scale(self, scale=Consts.SCALE_NORMAL):
        """
        The same two cases as in DataBundle2d, check for detailed description.
        """
        if scale in (Consts.SCALE_RELATIVE, Consts.SCALE_RELATIVE_100):
            # relative scaling is the same in both cases
            if scale == Consts.SCALE_RELATIVE_100:
                factor = 100.0
                round_to = 4
            else:
                factor = 1.0
                round_to = 6
            new_data = []
            if self.base_data:
                # use special set of data for calculation of relative share
                for i, (main_key, record) in enumerate(self.data):
                    new_record = {}
                    for key, value in record.iteritems():
                        base_value = self.base_data[i][1][key]
                        if base_value != 0:
                            new_record[key] = round(factor*value/base_value, round_to)
                    new_data.append((main_key, new_record))
                self.data = new_data
            else:
                # calculation of relative share using current series
                for main_key, record in self.data:
                    total = sum(record.values(), 0)
                    if total == 0:
                        new_record = record
                    else:
                        new_record = {}
                        for key, value in record.iteritems():
                            new_record[key] = round(factor*value/total, round_to)
                    new_data.append((main_key, new_record))
                self.data = new_data

    
    def gen_data_table(self):
        for main_key, record in self.data:
            new_record = {}
            for key, value in record.iteritems():
                new_record[key[0]] = value
            new_record['_main_key'] = self._format_main_key(main_key)
            yield new_record


    def _sort_data(self, sorting=None):
        if isinstance(self.key_dim, Dimension) and (sorting != None):
            # user interface does not implement option to slice by date now,
            # so this is not needed
            raise NotImplementedError("Not yet implemented")

    
    def get_series_description(self):
        description = {}
        series_names = set()
        series = set()
        for main_key, record in self.data:
            series_names |= set([key[0] for key in record.keys()])
            series |= set(record.keys())
        for name in series_names:
            description[name] = ('number', name)
        if "_time" in self.dimensions:
            description["_main_key"] = self._get_time_dim_description()
        else:
            description["_main_key"] = ("string", _('Who knows'))
        return series, description

    
    def transform(self, transformation):
        """
        Performs a transformation on the data and returns a new DataBundle
        with the transformed data
        """
        if transformation in (Consts.TIME_AGGREG_DIFF,
                              Consts.TIME_AGGREG_DIFF_REL):
            if len(self.data) < 2:
                raise DataRetreivalException(
                    "Cannot do difference with %d intervals" % len(self.data))
            elif len(self.data) > 2:
                logging.warn("Ineffective difference with %d time points" %\
                             len(self.data))
            start = self.data[0][1]
            end = self.data[-1][1]
            result = []
            all_keys = set(start.keys()) | set(end.keys())
            for key in all_keys:
                value1 = start.get(key, 0)
                value2 = end.get(key, 0)
                if transformation == Consts.TIME_AGGREG_DIFF:
                    result.append((key, value2-value1))
                elif transformation == Consts.TIME_AGGREG_DIFF_REL:
                    if value1 != 0:
                        # ignore values starting with 0 at day 1
                        result.append((key,
                                       round(100.0*(value2-value1)/value1, 4)))
            if transformation == Consts.TIME_AGGREG_DIFF:
                amount_dim = self.amount_dim
            elif transformation == Consts.TIME_AGGREG_DIFF_REL:
                amount_dim = self.amount_dim
                #amount_dim = _("Percent difference")
            return DataBundle2d([self.dimensions[self.idx_series],
                                 self.dimensions[self.idx_amount]],
                                 result,
                                 amount_dim=amount_dim,
                                 data_time_aggreg=transformation)
                
        
    
if __name__ == "__main__":
    from pprint import pprint
    import time
    
    data_type = DataType.objects.get(code_name="hosting")
    from_time = datetime.date(2011, 9, 1)
    to_time = datetime.date(2012, 10, 10)
    #from_time = to_time
    drq = DataQuery(data_type, from_time, to_time,
                    time_aggregate=Consts.TIME_AGGREG_DIFF_REL,
                    time_sampling="1m")
    #drq.set_params(fixed_values=[u'Počet domén'])
    #drq.set_params(fixed_values=['mx',None])
    drq.set_params(fixed_values=['mx',"ACTIVE 24"])
    #drq.set_params(fixed_values=['mx',"ACTIVE 24"])
    t = time.time()
    bundle = drq.fetch_results()
    print("Retreive: {0:.1f}ms".format(1000*(time.time() - t)))
    print(bundle)
    t = time.time()
    bundle.postprocess()
    #pprint(bundle.raw_data)
    #t = time.time()
    #bundle.postprocess(series_set=set([92]))
    #print(bundle)
    #print("Postprocess: {0:.1f}ms".format(1000*(time.time() - t)))
    pprint(bundle.get_series_description())
    for rec in bundle.gen_data_table():
        print(rec)
    
