# built-in modules
import copy
import logging

# django imports
from django.db import transaction

# local imports
from common import Consts, DateManipulation
import models

logger = logging.getLogger("unifiedstatistics.aggregation")

    
class Aggregator(object):
    
    
    TIME_PERIODS = ("1m",)
    
    # (method_id, field_suffix, recurrent_compatible, nonrecurrent_compatible)
    AGGREG_METHODS = ((Consts.TIME_AGGREG_SUM, "sum", True, False),
                      (Consts.TIME_AGGREG_LAST, "last", False, True),
                      (Consts.TIME_AGGREG_FIRST, "first", False, True),
                      (Consts.TIME_AGGREG_AVG, "avg", True, True),
                      (Consts.TIME_AGGREG_MIN, "min", True, True), 
                      (Consts.TIME_AGGREG_MAX, "max", True, True),
                      (Consts.TIME_AGGREG_MEDIAN, "median", True, True),  
                      (Consts.TIME_AGGREG_MIDDLE, "middle", False, True),  
                      )

    @classmethod
    def get_aggregate_fn(cls, method):
        if method == Consts.TIME_AGGREG_LAST:
            return lambda xs: xs[-1]
        elif method == Consts.TIME_AGGREG_FIRST:
            return lambda xs: xs[0]
        elif method == Consts.TIME_AGGREG_AVG:
            return lambda xs: 1.0*sum(xs) / len(xs)
        elif method == Consts.TIME_AGGREG_MIN:
            return lambda xs: min(xs)
        elif method == Consts.TIME_AGGREG_MAX:
            return lambda xs: max(xs)
        elif method == Consts.TIME_AGGREG_DIFF:
            return lambda xs: xs[-1] - xs[0]
        elif method == Consts.TIME_AGGREG_DIFF_REL:
            return lambda xs: 1.0*(xs[-1] - xs[0]) / xs[0]
        elif method == Consts.TIME_AGGREG_SUM:
            return lambda xs: sum(xs)
        elif method == Consts.TIME_AGGREG_MEDIAN:
            return cls._median
        elif method == Consts.TIME_AGGREG_MIDDLE:
            return lambda xs: xs[len(xs)//2]
        else:
            raise ValueError("Unsupported aggregate method '%s'", method)     
        
    @classmethod
    def _median(cls, xs):
        data = copy.copy(xs)
        data.sort()
        length = len(data)
        if len(data) % 2 == 1:
            return data[length//2]
        else:
            return (data[(length//2)-1] + data[(length//2)]) // 2
        
    
    @classmethod
    def create_aggregs(cls, time_period=TIME_PERIODS[0],
                       start_date=None, end_date=None, data_type=None):
        if isinstance(data_type, models.DataType):
            to_process = [data_type]
        elif isinstance(data_type, str):
            try:
                to_process = [models.DataType.objects.get(code_name=data_type)]
            except models.DataType.DoesNotExist:
                raise ValueError("DataType with code_name '%s' does not exist '%s'", data_type)
        else:
            to_process = models.DataType.objects.all()
        for data_type in to_process:
            if data_type.time_sampling_compatible():
                logger.debug("Processing '%s'", data_type)
                cls.create_aggregs_for_data_type(data_type, time_period,
                                                 start_date=start_date,
                                                 end_date=end_date)
            else:
                logger.debug("Skipping aggregation incompatible '%s'",
                             data_type)
            
    @classmethod
    def create_aggregs_for_data_type(cls, data_type, time_period, 
                                     start_date=None, end_date=None):
        columns = ["value1","value2","value3","value4","date","amount"]
        filter_def = {}
        if start_date:
            start_date = DateManipulation.round_time(start_date, time_period[-1],
                                                     up=False)
            filter_def["date__gte"] = start_date
        if end_date:
            end_date = DateManipulation.round_time(end_date, time_period[-1],
                                                   up=True)
            filter_def["date__lte"] = end_date
        
        data = data_type.datapoint_set.filter(**filter_def)\
                                .order_by(*columns[:-1]).values_list(*columns)
        last_key = None
        amounts = []
        for value1, value2, value3, value4, date, amount in data:
            key = (value1, value2, value3, value4)
            if last_key != key:
                if last_key:
                    cls.process_one_time_seq(data_type, time_period, last_key,
                                             amounts)
                last_key = key
                amounts = []
            amounts.append((date, amount))
        if amounts:
            cls.process_one_time_seq(data_type, time_period, key, amounts)
        

    @classmethod
    @transaction.commit_on_success()
    def process_one_time_seq(cls, data_type, time_period, key, data):
        logger.debug("Data type %s, key %s: %d values, time period %s",
                     data_type.code_name, key, len(data), time_period)
        time_break = DateManipulation.get_time_breaker_fn(time_period)
        amounts = []
        last_date = None
        for date, amount in data:
            if not last_date:
                last_date = date
            elif time_break(last_date, date):
                cls.create_aggreg_record(data_type, time_period,
                                         key, last_date, amounts)
                last_date = date
                amounts = []
            amounts.append(amount)
        cls.create_aggreg_record(data_type, time_period,
                                 key, date, amounts)
    
                    
    @classmethod
    def create_aggreg_record(cls, data_type, time_period, key, date, amounts):
        #logger.debug("Key %s, date: %s: %d values", key, date, len(amounts))
        if amounts:
            aggreg_kw = {}
            for method_id, suffix, recurr, nonrecurr in cls.AGGREG_METHODS:
                if (data_type.recurrent and recurr) or \
                   (not data_type.recurrent and nonrecurr):
                    method = Aggregator.get_aggregate_fn(method_id)
                    aggreg_kw["amount_"+suffix] = int(method(amounts))
            if data_type.recurrent:
                aggreg_kw['amount'] = aggreg_kw['amount_sum']
            else:
                aggreg_kw['amount'] = aggreg_kw['amount_last']
            value1, value2, value3, value4 = key
            # round date down, otherwise it would lead to new caching record
            # being created each day
            date = DateManipulation.round_time(date, time_period[-1], up=False)
            get_args = dict(data_type=data_type, date=date,
                            time_period=time_period,
                            value1=value1, value2=value2, value3=value3, value4=value4)
            try:
                point = models.DataPointAggreg.objects.get(**get_args)
            except models.DataPointAggreg.DoesNotExist:
                get_args.update(aggreg_kw)
                point = models.DataPointAggreg(**get_args)
                point.save()
            else:
                save = False
                for key, value in aggreg_kw.iteritems():
                    if getattr(point, key) != value:
                        setattr(point, key, value)
                        save = True
                if save:
                    #logger.debug("Aggreg was updated.")
                    point.save()
                    
