from django.conf import settings
from django.core.cache import cache
from django.utils.translation import get_language

from unifiedstatistics.models import MenuItem

def piwik(context):
    return {'PIWIK_URL': settings.PIWIK_URL,
            'PIWIK_ID': settings.PIWIK_ID}

def navigation_levels(request):
    cache_key = 'navigation_levels-' + get_language()
    navigation_levels = cache.get(cache_key)
    if not navigation_levels:
        menu = MenuItem.objects.all()
        navigation_levels = []
        last_level = 0
        menu_items = []
        item_counter = 1
        for item in menu:
            if item.level != last_level:
                navigation_levels.append({'id': last_level,
                                          'items': menu_items})
                last_level = item.level
                menu_items = []
                item_counter = 1
            menu_items.append({'label': item.label,
                               'href': item.href,
                               'active': item.active,
                               'id': item_counter})
            item_counter += 1
        navigation_levels.append({'id': last_level,
                                  'items': menu_items})
        cache.set(cache_key, navigation_levels)
    return {'navigation_levels': navigation_levels}
