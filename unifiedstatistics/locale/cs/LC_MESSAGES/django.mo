��    �        �   
      �  �   �  �   [  s     �   �  ~   V  I   �  �     M   �  q   H  \   �               4     =     A  *   H     s     �     �     �     �     �     �     �  	   �     �               %  	   ,     6  
   F     Q     h     t  	   �  
   �  
   �  	   �     �     �  
   �  
     :        G     V     _     l     q  "   v     �  7   �     �     �     �     �          
          )     7     =     D     P     V  
   [     f  &   �  /   �  $   �     �          
               ,  T   >     �     �     �     �     �     �                          *     :     W     u     �     �  5   �  7   �     (     .     5     <     I  
   M  <   X     �  2   �  4   �           "  
   C     N     _     f     v     �     �  B   �     �     �     �  #   	  	   -     7     C     H  G   h  C   �     �               #     (     E     c          �  
   �  #   �     �     �     �                          '  	   .     8  	   A     K     _     m     |  
   �  
   �     �     �     �     �     �  y  �  �   P  �   8   �   �   �   s!  �   ="  Q   �"  �   #  T   �#  x   S$  t   �$     A%     N%     b%     k%  
   t%  /   %     �%     �%     �%     �%     �%     &     &     &&     3&     <&     D&     J&     b&     i&     r&     �&     �&     �&     �&     �&     �&     �&     �&  !   �&  #   '     ='     E'  5   S'     �'  	   �'     �'     �'     �'  "   �'     �'  ,   �'     (     (  	   #(     -(     G(     M(     d(     r(     �(  	   �(     �(     �(     �(     �(     �(  5   �(  <   )  3   M)  
   �)     �)     �)     �)     �)     �)  L   �)     **     <*     S*     f*     }*     �*     �*     �*     �*     �*     �*  !   �*  !   +  &   '+  %   N+     t+  9   |+  >   �+     �+     �+     ,     ,     ,     ",  G   5,     },  ,   �,  4   �,  *   �,  )   -  
   ;-     F-     X-     `-     |-     �-     �-  U   �-     .     .     .  !   ,.     N.     V.     d.     h.  L   �.  V   �.     */     ?/     N/     W/     ]/     v/     �/     �/     �/     �/  !   �/     �/     �/     0     "0     00     >0     F0  
   M0     X0  	   f0     p0     �0     �0     �0     �0     �0  
   �0     �0     �0     �0  	   �0     1     �       3   K          �       �   H   e   Z   E   4               	   k   �           \      �      ]       1          {                 >       6       0   �   R           q   �   �      7   x   �   P       �   F           5   }   g   -   a          �           B   [   S   d       f   9      /   W                              L   z   M           h           .   �   (   j           �   �   p   Q   `   #   ~   2              ?   %   w      Y   r   G      ;   �           t      C   :   �   ^         �   J       v   *   c   n   =   �   u   O   )              �       �               
       N   �   s   y             �   &   �           o   $   �   �       �   �   +       X   '   D       U       |           m      �       !   T      8   �   �          l   I      _          A   b      �                      @      �       V   i      "   ,          <    
Hello, welcome to your private area. To have access to private graphs, please enter private key you received.
If you do not have one and still think you should have access, please contact site administrator.
 
JavaScript is necessary to view the charts displayed on this website.
If you do not have JavaScript turned on in your browser, you will not be able to
see the presented statistical data.
 
Our admins were notified about the problem and will
attend to it as soon as possible. Please try this link later.
 
The charts displayed on these pages are rendered by the <a href="http://www.highcharts.com/">Highcharts library</a> using the <a href="https://gitlab.labs.nic.cz/labs/ogra">OGRA adaptor library</a>. 
 
The statistics are divided into several categories and may be easily navigated
using the menu on the left side of this page.
 
This report is not available for online viewing, but can be downloaded.
 
This site requires JavaScript in order to display charts. If this message
does not go away, it means that JavaScript has been disabled, either by
a plugin or extension in your browser, or by explicit browser setting.
 
This statistics is not available for online viewing, but can be downloaded.
 
This website contains a compilation of statistical data published by CZ.NIC, z.s.p.o., the
.cz domain registry.
 
You can visit the <a href="%(mainpage)s">main page</a>
for a list of available statistics.
 - all - About .cz statistics Akademie All Amount An error occurred when preparing this page Available statistics Back to nic.cz home page CSIRT.CZ CZ. statistics CZ.NIC Edition CZ.NIC LABS CZ.NIC Statistics CZ.NIC-CSIRT Chart for Contact Content Create a mojeID account DNSSEC Dashboard Dashboard chart Data point Data point aggregation Data points Data points aggregations Data type Data types Difference Dimension Dimension to data type mapping Dimension to data type mappings Dimensions Display as Does this dimension partition the data into summable parts Domain browser Download Download CSV FRED File Free Registry for ENUM and Domains From Highlighted information connected with given statistics Home Info Information JavaScript required! Language Link to this chart Logarithmic Logged in as: Login Logout Methodology Month Name Navigation Nic.cz | CZ Domain Registry No data available for selected report. No data available for selected time and series. No data available for selected time. Normal Note Other Other sites: Page not found Permission denied Preferred sorting of connected dimension when used in charts without time dimensions Private area Private statistics Relative difference Relative difference [%] Relative in % Relative share [%] Reload Report Report group Report section Report sections Report to Statistics mapping Report to Statistics mappings ReportGroup to Report mapping ReportGroup to Report mappings Reports Requested end time could not have been fully matched. Requested start time could not have been fully matched. Scale Search Series Server error Set Set access Should 'other' be created when limiting the number of series Show Sorry, the page you are looking for was not found. Sorry, you do not have permission to view this page. StatGroup to Statistics mapping StatGroup to Statistics mappings Statistics Statistics group Submit Text to int map Text to int maps Textual description of chart  Time Title of the chart which overwrites related data type name if set. Top %d Up Used technologies We apologize for the inconvenience. Who knows Why mojeID? Year You entered invalid access key. You entered valid access key and gained access to following statistics: You have already access to all statistics connected with given key. Your access key ^ To the Table of Contents all back by amount in ascending order by amount in descending order by label in ascending order by label in descending order chart difference from %(start_date)s to %(end_date)s individual days individual months individual years integer logarithmic maximum mojeID normal one month one year pie chart relative difference relative in % search&hellip; selected day six months statistics table text three months three years until %(end_date)s Project-Id-Version: git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-14 17:20+0200
PO-Revision-Date: 2013-01-28 08:08+0100
Last-Translator: Jiri Machalek <jiri.machalek@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Vítejte v privátní části. Pro zpřístupnění neveřejných statistik zadejte privátní klíč, který jste obdrželi. Pokud jej nemáte a domníváte se, že máte mít přístup, kontaktujte prosím administrátora webu.
 
Tyto stránky využívají ke zobrazení grafů JavaScript.
Pokud nemáte ve svém prohlížeči JavaScript zapnut, prezentovaná
statistická data se vám nezobrazí.
 
Naši administrátoři byli informováni o problému a budou
se mu věnovat v nejbližší možné době. Prosím zkuste tento odkaz později.
 
Grafy zobrazené na těchto stránkách jsou vykreslovány <a href="http://www.highcharts.com/">knihovnou Highcharts</a> s použitím <a href="https://gitlab.labs.nic.cz/labs/ogra">knihovny OGRA</a>.
 
Statistiky jsou rozděleny do několika kategorií k jejichž snadnému procházení slouží
menu na levé straně této stránky.
 
Tento report není možné prohlížet online, ale je k dispozici ke stažení.
 
Tato stránka vyžaduje pro zobrazení grafů JavaScript. Pokud je tato zpráva
viditelná, znamená to, že JavaScript je pro tuto stránku zakázán, buď
pluginem nebo rozšířením prohlížeče, nebo změnou jeho nastavení.
 
Tuto statistiku není možné prohlížet online, ale je k dispozici ke stažení.
 
Tyto stránky obsahují souhrn statistických dat publikovaných
společností CZ.NIC, z.s.p.o, správcem domény .cz.
 
Můžete přejít na <a href="%(mainpage)s">hlavní stránku</a>,
kde najdete seznam všech dostupných statistik.
 - všechny - O statistikách .cz Akademie Všechny Množství Při přípravě této stránky došlo k chybě Dostupné statistiky Zpět na hlavní stranu nic.cz CSIRT.CZ Statistiky CZ. Edice CZ.NIC Laboratoře Statistiky CZ.NIC CZ.NIC-CSIRT Graf pro Kontakt Obsah Vytvořit mojeID účet DNSSEC Přehled Graf z přehledu Datový bod Agregace datových bodů Datové body Agregace datových bodů Datový typ Datové typy Rozdíl Dimenze Mapování dimenze na datový typ Mapování dimenzí na datové typy Dimenze Zobrazit jako Rozděluje tato dimenze data na sčítatelné čísti Doménový prohlížeč Stažení Stáhnout CSV FRED Soubor Free Registry for ENUM and Domains Od Zvýrazněná informace pro danou statistiku Domů Info Informace JavaScript není povolen! Jazyk Odkaz na tuto stránku Logaritmická Přihlášen jako: Přihlášení Odhlásit Metodika Měsíc Jméno Navigace Nic.cz | CZ Registr domén Pro vybraný report nejsou k dispozici žádná data. Pro vybraný čas a série nejsou k dispozici žádná data. Pro vybraný čas nejsou k dispozici žádná data. Normální Upozornění Ostatní Ostatní weby: Stránka nebyla nalezena Přístup odepřen Preferované řazení svázené dimenze použité pro grafy bez časové osy Privátní část Neveřejná statistika Relativní rozdíl Relativní rozdíl [%] Relativní v % Relativní zastoupení [%] Obnovit Report Skupina reportů Sekce reportu Sekce reportu Mapování reportů na statistiky Mapování reportů na statistiky Mapování skupiny reportů na reporty Mapování skupin reportů na reporty Reporty Požadavek na koncový čas nemohl být přesně splněn. Požadavek na počáteční čas nemohl být přesně splněn. Škála Hledat Série Chyba serveru Nastavit Nastavit přístup Má být vytvořena položka 'ostatní' když se omezuje počet sérií Zobraz Je nám líto, ale tato stránka neexistuje. Nemáte oprávnění k prohlížení této stránky. Mapování skupiny statistik na statistiky Mapování skupin statistik na statistiky Statistiky Skupina statistik Odeslat Mapování textu na číslo Mapování textů na čísla Slovní popis grafu Čas Jméno grafu, které přepisuje výchozí jméno z datového typu, pokud je nastaveno Top %d Nahoru Použité technologie Omlouváme se za nepříjemnosti. Kdo ví Proč mojeID? Rok Zadali jste neplatný klíč. Zadali jste platný klíč a máte přístup k následujícím statistikám: Již máte přístup ke všem statistikám, které jsou spojené se zadaným klíčem. Přístupový klíč ^ Zpět nahoru všechny zpět podle hodnoty vzestupně podle hodnoty sestupně podle popisu vzestupně podle popisu sestupně graf rozdíl od %(start_date)s do %(end_date)s jednotlivé dny jednotlivé měsíce jednotlivé roky celé číslo logaritmická maximum mojeID normální jeden měsíc jeden rok výsečový graf relativní rozdíl relativní v % vyhledat&hellip; vybraný den šest měsíců statistiky tabulka text tři měsíce tři roky do %(end_date)s 