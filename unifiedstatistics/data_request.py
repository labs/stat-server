# encoding: utf-8

# built-in modules
import datetime
import logging
import locale

# Django imports
from django.http import Http404
from django.utils.translation import ugettext_lazy as _, ugettext
from django.db.models import Max, Min
from django.utils import translation
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import AnonymousUser

# third party modules
import gviz_api

# local imports
from unifiedstatistics import models
from data_mining import DataQuery, DataRetreivalException
from common import DateManipulation, Consts, DateInterval, querydict_to_dict

logger = logging.getLogger("unifiedstatistics.data_request")

class DataRequest(object):
    
    KEY_SLICING_DIM = "sd"
    KEY_SLICING_VALUE = "sv"
    KEY_SLICING_DIM_2 = "sd2"
    KEY_SLICING_VALUE_2 = "sv2"
    KEY_SLICING_DIM_3 = "sd3"
    KEY_SLICING_VALUE_3 = "sv3"
    KEY_REF_DATE = "rd"
    KEY_DATE_RANGE = "dr"
    KEY_TIME_PROCESSING = "tp"
    KEY_SERIES_SET = "ss"
    KEY_DATA_SCALING = "ds" 
    KEY_DISPLAY_AS = "da"
    KEY_SERIES_LIMIT = "sl"
    KEY_INCLUDE_OTHER = "io"
    KEY_RELATIVE_SCALE_BASE = "rb"
    
    # date range definitions
    DEFAULT_DATE_RANGE = "1y"
    DATE_RANGES = (("0d", _("selected day")),
                   ("1m", _("one month")),
                   ("3m", _("three months")),
                   ("6m", _("six months")),
                   ("1y", _("one year")),
                   ("3y", _("three years")),
                   ("all", _("maximum")),
                   )
    VALID_DATE_RANGE_KEYS = [v[0] for v in DATE_RANGES]
    
    # time processing definitions
    DEFAULT_TIME_PROCESSING = "i-1m"
    TIME_PROCESSINGS = (("i-1d", _("individual days")),
                        ("i-1m", _("individual months")),
                        ("i-1y", _("individual years")),
                        ("diff", _("difference")),
                        ("reldiff", _("relative difference")),
                        )
    VALID_TIME_PROCESSING_KEYS = [v[0] for v in TIME_PROCESSINGS]
    
    # display as
    DEFAULT_DISPLAY_AS = "chart"
    DISPLAY_AS_OPTS = (("chart", _("chart")),
                       ("table", _("table")),
                       ("pie_chart", _("pie chart")),
                       )
    VALID_DISPLAY_AS_KEYS = [v[0] for v in DISPLAY_AS_OPTS] + ["column_chart", "line_chart", "bar_chart", "area_chart"]
    
    # scaling methods
    VALID_SCALING_KEYS = ["normal","relative","log"]
    DEFAULT_SCALING = "normal"
    
    DIM_KEY_REMAP = {"_time": _("Time")}
    
    SERIES_LIMIT = 15
    
    GROUPED_DIM_VALUE = -1
    
    class IncompatibleSettingsError(Exception):
        pass
    
    class ResultPackage:
        pass

    def __init__(self, params, statistics, report=None, position=None, user=None):
        if isinstance(statistics, models.Statistics):
            self.statistics = statistics
        elif type(statistics) in (str, unicode):
            try:
                self.statistics = models.Statistics.objects\
                                                .select_related("data_type")\
                                                .get(code_name=statistics)
            except models.DataType.DoesNotExist:
                raise Http404
        else:
            raise ValueError("statistics arg must be either Statistics "
                             "instance or code name")

        # Statistics must be public or user must be authenticated and has permission to view statistics,
        # otherwise raise HTTP 403
        if not user:
            user = AnonymousUser()
        if (not self.statistics.get_public()) and \
                (not user.is_staff) and \
                (not user.is_authenticated() or not user.has_perm('view_statistics', self.statistics)):
            raise PermissionDenied

        if self.statistics.data_type is None:
            # this is offline statistics, cannot process it
            raise Http404
        if report:
            limits = {'position':position}
            if isinstance(report, models.Report):
                limits['report'] = report
            elif type(report) in (str, unicode):
                limits['report__code_name'] = report
            else:
                raise Http404
            try:
                self.r2s = models.ReportToStatistics.objects.select_related('params').\
                                                             get(**limits)
            except models.ReportToStatistics.DoesNotExist:
                raise Http404
        else:
            self.r2s = None

        self.data_type = self.statistics.data_type
        # check if there is some data at all
        if not self.data_type.has_data():
            raise Http404
        self.dimensions = self.data_type.get_dimensions()

        # default_params from Statistics object or from ReportToStatistics if defined
        if self.r2s and self.r2s.params:
            def_params = self.r2s.params.get_data_request_params()
        elif self.statistics.default_params:
            def_params = self.statistics.default_params.get_data_request_params()
        else:
            def_params = {}
        # merge default_params with params
        new_params = querydict_to_dict(params)
        # don't overwrite default display as parameter if set
        if (new_params.get(self.KEY_DISPLAY_AS, None) == self.DEFAULT_DISPLAY_AS) and \
            (def_params.get(self.KEY_DISPLAY_AS, None) in self.VALID_DISPLAY_AS_KEYS):
            new_params.pop(self.KEY_DISPLAY_AS)
        def_params.update(new_params)
        params = def_params

        # enforced parameters        
        if self.statistics.enforced_params:
            enforced_params = self.statistics.enforced_params.get_data_request_params()
        else:
            enforced_params = {}
        #enforced_slicing_dim_name = enforced_params.get(self.KEY_SLICING_DIM)
        #enforced_slicing_value = enforced_params.get(self.KEY_SLICING_VALUE)
        enforced_slicing_dim_names = [enforced_params.get(self.KEY_SLICING_DIM), enforced_params.get(self.KEY_SLICING_DIM_2), enforced_params.get(self.KEY_SLICING_DIM_3)]
        enforced_slicing_values = [enforced_params.get(self.KEY_SLICING_VALUE), enforced_params.get(self.KEY_SLICING_VALUE_2), enforced_params.get(self.KEY_SLICING_VALUE_3)]
        
        # read parameters
        req_slicing_dim_names = [params.get(self.KEY_SLICING_DIM), params.get(self.KEY_SLICING_DIM_2), params.get(self.KEY_SLICING_DIM_3)]
        req_slicing_values = [params.get(self.KEY_SLICING_VALUE), params.get(self.KEY_SLICING_VALUE_2), params.get(self.KEY_SLICING_VALUE_3)]
        req_ref_date = params.get(self.KEY_REF_DATE)
        req_date_range = params.get(self.KEY_DATE_RANGE,
                                    self.DEFAULT_DATE_RANGE)
        req_time_processing = params.get(self.KEY_TIME_PROCESSING,
                                         self.DEFAULT_TIME_PROCESSING)
        req_series_set = params.get(self.KEY_SERIES_SET, 0)
        if type(req_series_set) is not list:
            req_series_set = [req_series_set]
        req_data_scaling = params.get(self.KEY_DATA_SCALING,
                                      self.DEFAULT_SCALING)
        req_display_as = params.get(self.KEY_DISPLAY_AS,
                                    self.DEFAULT_DISPLAY_AS)
        req_series_limit = params.get(self.KEY_SERIES_LIMIT)
        req_include_other = params.get(self.KEY_INCLUDE_OTHER)
        # sanitize the parameters
        if type(req_include_other) is bool:
            self.include_other = req_include_other
        elif type(req_include_other) is int:
            self.include_other = bool(req_include_other)
        elif req_include_other in ("yes", "true"):
            self.include_other = True
        elif req_include_other in ("no", "false"):
            self.include_other = False
        else:
            self.include_other = None
        req_relative_scale_base_name = params.get(self.KEY_RELATIVE_SCALE_BASE)
        # series limit
        if req_series_limit:
            try:
                self.series_limit = int(req_series_limit)
            except ValueError:
                self.series_limit = self.SERIES_LIMIT
        else:
            self.series_limit = self.SERIES_LIMIT
        for i, v in enumerate(req_slicing_values):
            if v:
                try:
                    req_slicing_values[i] = int(v)
                except ValueError:
                    pass
        for i, v in enumerate(enforced_slicing_values):
            if v:
                try:
                    enforced_slicing_values[i]= int(v)
                except ValueError:
                    pass
        #if enforced_slicing_value:
        #    try:
        #        enforced_slicing_value = int(enforced_slicing_value)
        #    except ValueError:
        #        pass
        # enforced slicing (name is either None or int)
        #if type(enforced_slicing_dim_name) is int and \
        #   len(self.dimensions) > enforced_slicing_dim_name:
        #    enforced_slicing_dim = self.dimensions[enforced_slicing_dim_name]
        #    if type(enforced_slicing_value) is not int:
        #        enforced_slicing_value = enforced_slicing_dim.value_to_int(enforced_slicing_value)
        #else:
        #    enforced_slicing_dim = None
        enforced_slicing_dims = []
        enforced_slicing_dims_values = {}
        for i, enforced_slicing_dim_name in enumerate(enforced_slicing_dim_names):
            if type(enforced_slicing_dim_name) is int and \
               len(self.dimensions) > enforced_slicing_dim_name:
                enforced_slicing_dims.append(self.dimensions[enforced_slicing_dim_name])
                if type(enforced_slicing_values[i]) is not int:
                    enforced_slicing_values[i] = enforced_slicing_dims[-1].value_to_int(enforced_slicing_values[i])
                enforced_slicing_dims_values[enforced_slicing_dims[-1]] = enforced_slicing_values[i]
        
        # calculate number of dimensions which need to be sliced    
        dims_to_be_sliced = len(self.dimensions) - 1 - len(enforced_slicing_dims)
        
        #if enforced_slicing_dim:
        #    dims_to_be_sliced -= 1
            
        slicing_dims = []
        slicing_dims_values = {}
        
        # returns tuple with:
        # 1/ legal values for slicing dimensions (for integer
        # dimensions this is enough, for text dimensions we need to remap them)
        # 2/ slicing value actually used to slice the dimension, with respect to
        # preferred slicing 
        def get_slicing_dim_values(slicing_dim, preferred_slicing=None):
            # first get the legal values
            ix = self.dimensions.index(slicing_dim) + 1
            value_name = "value%d" % ix
            dtt = slicing_dim.dimensiontodatatype_set.get(
                                    data_type=self.data_type)
            if self.data_type.time_sampling_compatible():
                slicing_dim_values = list(self.data_type.datapointaggreg_set.distinct().\
                                            values_list(value_name, flat=True).\
                                            order_by(value_name))
            else:
                slicing_dim_values = list(self.data_type.datapoint_set.distinct().\
                                            values_list(value_name, flat=True).\
                                            order_by(value_name))
                
            if slicing_dim.type == models.Dimension.TYPE_TEXT:
                present_keys = set(slicing_dim_values)
                slicing_dim_values = slicing_dim.get_int_to_str_remap(
                    lang=translation.get_language())
                # select keys, whose mappings are not used and remove them from possible series 
                unused_keys = set(slicing_dim_values) - present_keys
                for key in unused_keys:
                    slicing_dim_values.pop(key)
            # second get the slicing value
            slicing_value = None
            if slicing_dim_values and isinstance(slicing_dim_values, dict):
                if dtt.partitioning:
                    slicing_dim_values[self.GROUPED_DIM_VALUE] = ugettext("- all -")
                if preferred_slicing:
                    if type(preferred_slicing) is int:
                        if preferred_slicing in slicing_dim_values:
                            slicing_value = preferred_slicing
                    else:
                        # it is a string, we need to try to map it back to int
                        slicing_value = slicing_dim.value_to_int(preferred_slicing)
                # we did not get any slicing value, but we need one
                if slicing_value is None:
                    slicing_value = sorted(filter(lambda x: x != self.GROUPED_DIM_VALUE, slicing_dim_values.keys()))[0]
            elif slicing_dim_values and isinstance(slicing_dim_values, list):
                # Slicing dimension of integer type => slicing_dim_values
                # is sorted list with valid values and we expect integer for
                # slicing value
                if dtt.partitioning:
                    slicing_dim_values.append(self.GROUPED_DIM_VALUE)
                if preferred_slicing and (preferred_slicing in slicing_dim_values):
                    slicing_value = preferred_slicing
                else:
                    slicing_value = slicing_dim_values[0]
            return (slicing_dim_values, slicing_value)
            
        # process preferred slicing dimensions and values        
        for i, req_slicing_dim_name in enumerate(req_slicing_dim_names):
            if type(req_slicing_dim_name) is int and \
               len(self.dimensions) > req_slicing_dim_name and \
               self.dimensions[req_slicing_dim_name] not in slicing_dims and \
               self.dimensions[req_slicing_dim_name] not in enforced_slicing_dims:
                slicing_dims.append(self.dimensions[req_slicing_dim_name])
                slicing_dims_values[slicing_dims[-1]] = get_slicing_dim_values(slicing_dims[-1], req_slicing_values[i])
            elif req_slicing_dim_name:
                for dim in self.dimensions:
                    if dim.code_name == req_slicing_dim_name and \
                       dim not in enforced_slicing_dims and \
                       dim not in slicing_dims:
                        slicing_dims.append(dim)
                        slicing_dims_values[slicing_dims[-1]] = get_slicing_dim_values(slicing_dims[-1], req_slicing_values[i])

        # relative_scale_base
        relative_scale_base = None
        if type(req_relative_scale_base_name) is int and \
           len(self.dimensions) > req_relative_scale_base_name:
            relative_scale_base = self.dimensions[req_relative_scale_base_name]
        elif req_relative_scale_base_name:
            try:
                relative_scale_base = models.Dimension.objects.get(code_name=req_relative_scale_base_name)
            except models.Dimension.DoesNotExist:
                pass

        # avoid unnecessary slicing
        if len(slicing_dims) > dims_to_be_sliced:
            slicing_dims = slicing_dims[:dims_to_be_sliced]
            
        # add slicing dimensions if needed 
        for i in range(dims_to_be_sliced - len(slicing_dims)):
            for dim in self.dimensions:
                if dim not in slicing_dims and \
                   dim not in enforced_slicing_dims:
                    slicing_dims.append(dim)
                    slicing_dims_values[slicing_dims[-1]] = get_slicing_dim_values(slicing_dims[-1])
                    break

        # possible series
        possible_series = {}
        series_dim = None
        if self.dimensions:
            for dim in self.dimensions:
                if (dim not in slicing_dims) and (dim not in enforced_slicing_dims):
                    series_dim = dim
                    break
            if series_dim:
                # select present keys for given series
                # for integer dimensions this is enough, for text we need to remap them
                ix = self.dimensions.index(series_dim) + 1
                value_name = "value%d" % ix
                if self.data_type.time_sampling_compatible():
                    possible_series = list(self.data_type.datapointaggreg_set.distinct().\
                                                values_list(value_name, flat=True).\
                                                order_by(value_name))
                else:
                    possible_series = list(self.data_type.datapoint_set.distinct().\
                                                values_list(value_name, flat=True).\
                                                order_by(value_name))
                if  series_dim.type == models.Dimension.TYPE_TEXT:
                    # for text dimension use mapping and remove unused keys
                    present_keys = set(possible_series)
                    possible_series = series_dim.get_int_to_str_remap(
                                                    lang=translation.get_language())
                    # select keys, whose mappings are not used and remove them from possible series 
                    unused_keys = set(possible_series) - present_keys
                    for key in unused_keys:
                        possible_series.pop(key)
                
        # series_set
        series_set = None
        if req_series_set:
            if (0 in req_series_set) or '0' in req_series_set:
                # 0 means top X or all (if there is less than X)
                series_set = None
            elif (-1 in req_series_set) or '-1' in req_series_set:
                # -1 means 'all' - without any limit
                series_set = None
                self.series_limit = None
            else:
                # we process the set normally
                series_set = set()
                for series_id in req_series_set:
                    try:
                        series_id = int(series_id)
                    except:
                        pass
                    else:
                        if series_id in possible_series:
                            series_set.add(series_id)
                            continue
                    if series_dim and (series_dim.type == models.Dimension.TYPE_TEXT):
                        # try to remap it if series_dim is TYPE_TEXT
                        try:
                            texttoint = series_dim.texttointmap_set.get(
                                text=series_id)
                        except models.TextToIntMap.DoesNotExist:
                            pass
                        else:
                            series_set.add(texttoint.id)
        # time processing - display individual points, do a difference, etc.
        time_sampling = None
        time_aggregate = None
        if not self.data_type.time_sampling_compatible():
            time_aggregate = Consts.TIME_AGGREG_NONE
            time_sampling = None
        else:
            if req_time_processing in DataQuery.TIME_AGGREG_VALUES:
                # all known integer values
                time_aggregate = req_time_processing
            else:
                # it is not a known integer value
                if req_time_processing not in self.VALID_TIME_PROCESSING_KEYS:
                    req_time_processing = self.DEFAULT_TIME_PROCESSING
            if time_aggregate is None:
                if req_time_processing == "diff":
                    time_aggregate = Consts.TIME_AGGREG_DIFF
                elif req_time_processing == "reldiff":
                    time_aggregate = Consts.TIME_AGGREG_DIFF_REL
                elif type(req_time_processing) in (str, unicode) and \
                        req_time_processing.startswith("i-"):
                    # parse individual definitions
                    sample_def = req_time_processing[2:]
                    if sample_def == "1d":
                        time_aggregate = Consts.TIME_AGGREG_NONE
                    else:
                        time_sampling = sample_def
                        if self.data_type.recurrent:
                            time_aggregate = Consts.TIME_AGGREG_SUM
                        else:
                            time_aggregate = Consts.TIME_AGGREG_LAST
                else:
                    logging.info("strange time processing requested '%s'",
                                 req_time_processing)
                    time_aggregate = Consts.TIME_AGGREG_NONE

        # reference time
        fixed_values = {}
        for i, dim in enumerate(self.dimensions):
            if dim in slicing_dims and \
               slicing_dims_values[dim][1] != self.GROUPED_DIM_VALUE:
                attr_name = "value%d" % (i+1)
                fixed_values[attr_name] = slicing_dims_values[dim][1]
            if dim in enforced_slicing_dims and \
               enforced_slicing_dims_values[dim] != self.GROUPED_DIM_VALUE:
                attr_name = "value%d" % (i+1)
                fixed_values[attr_name] = enforced_slicing_dims_values[dim]

        available_dates = models.DataPoint.objects\
            .filter(data_type=self.data_type, **fixed_values).aggregate(max_date=Max('date'),
                                                                        min_date=Min('date'))
        end_date = None
        if req_ref_date:
            try:
                end_date = datetime.datetime.strptime(req_ref_date, "%Y-%m-%d")
            except Exception:
                logging.debug("Cannot parse date '%s'", req_ref_date)
                end_date = None
            else:
                end_date = end_date.date()
        if not end_date:
            end_date = available_dates['max_date']
            if end_date:
                end_date = end_date.date()
        # start time based on the selected range
        self.time_sampling = time_sampling
        rounding = self.get_time_rounding()
#        if req_date_range not in self.VALID_DATE_RANGE_KEYS:
#            req_date_range = self.DEFAULT_DATE_RANGE
        if not req_date_range:
            req_date_range = self.DEFAULT_DATE_RANGE
        start_date = DateManipulation.parse_time_range(end_date, req_date_range,
                                                       rounding=rounding)
        if rounding:
            end_date = DateManipulation.round_time(end_date, rounding, up=True)
                        
        logger.debug("selected date: %s - %s", start_date, end_date)
        # data scaling
        client_side_scaling = None
        if req_data_scaling in ("percent", Consts.SCALE_RELATIVE_100):
            data_scaling = Consts.SCALE_RELATIVE_100
            req_data_scaling = "percent"
        elif req_data_scaling in ("log",):
            data_scaling = Consts.SCALE_NORMAL
            client_side_scaling = "log"
        else:
            data_scaling = Consts.SCALE_NORMAL
            req_data_scaling = "normal"
            
        # enforce normal scale for Consts.TIME_AGGREG_DIFF and
        # Consts.TIME_AGGREG_DIFF_REL time aggregation options
        if time_aggregate != Consts.TIME_AGGREG_NONE and not time_sampling:
            data_scaling = Consts.SCALE_NORMAL
            req_data_scaling = "normal"
            
        # summable_series
        # determine whether to use stacked chart:
        # - series dimension must allow summing
        # - client side scaling cannot be log
        summable_series = False
        if series_dim and isinstance(series_dim, models.Dimension) and \
            (client_side_scaling != "log") and \
            ((data_scaling != Consts.SCALE_RELATIVE_100) or
             (not relative_scale_base) or
             (relative_scale_base == series_dim)):
            try:
                dtt = series_dim.dimensiontodatatype_set.get(data_type=self.data_type)
            except models.DimensionToDataType.DoesNotExist:
                summable_series = False
            else:
                summable_series = dtt.partitioning
        # display as
        if req_display_as not in self.VALID_DISPLAY_AS_KEYS:
            logging.warn("Unsupported value for '%s' requested: %s",
                         self.KEY_DISPLAY_AS, req_display_as)
            display_as = self.DEFAULT_DISPLAY_AS
        else:
            display_as = req_display_as

        # set locale for correct sorting of unicode strings
        # for missing locales locale_name is None, which defaults to locale C
        try:
            locale_name = settings.LANGUAGE_LOCALES.get(translation.get_language())
            locale.setlocale(locale.LC_ALL, locale_name)
        except locale.Error as e:
            logger.warning("Locale error: %s", e.message)

        # store the results in attributes
        self.slicing_dims_values = slicing_dims_values
        self.slicing_dims = slicing_dims
        self.enforced_slicing_dims = enforced_slicing_dims
        self.enforced_slicing_dims_values = enforced_slicing_dims_values
        self.relative_scale_base = relative_scale_base
        self.start_date = start_date
        self.end_date = end_date
        self.time_aggregate = time_aggregate
        self.possible_series = possible_series
        self.series_set = series_set
        self.data_scaling = data_scaling
        self.summable_series = summable_series
        self.display_as = display_as
        self.client_side_scaling = client_side_scaling
        self.available_dates = available_dates
        # also store some input values
        self.req_date_range = req_date_range
        self.req_time_processing = req_time_processing
        self.req_data_scaling = req_data_scaling
        
    def get_time_rounding(self):
        if self.time_sampling and self.time_sampling[-1] in ("my"):
            return self.time_sampling[-1]
        return None
    
        
    def get_valid_time_ranges(self):
        max_date = self.available_dates['max_date']
        min_date = self.available_dates['min_date']
        if min_date and max_date:
            available_range = max_date - min_date
        else:
            available_range = datetime.timedelta(days=0)
        was_selected = False
        for value, name in self.DATE_RANGES:
            selected = False
            if value != "all":
                interval = DateInterval.from_string(value).get_approx_timedelta()
                if interval > available_range:
                    # this interval is not interesting
                    continue
            else:
                if not was_selected:
                    # this is 'all' and nothing else was selected
                    selected = True
            if available_range == datetime.timedelta(days=0) and value == "0d" \
               and self.req_date_range == self.DEFAULT_DATE_RANGE:
                # we use 0d as default when only one time record is present
                was_selected = True
                selected = True
            if value == self.req_date_range:
                was_selected = True
                selected = True
            yield (value, name, selected)

    
    def get_valid_time_processings(self):
        max_date = self.available_dates['max_date']
        min_date = self.available_dates['min_date']
        if min_date and max_date:
            available_days = (max_date - min_date).days + 1
        else:
            available_days = 0
        for value, name in self.TIME_PROCESSINGS:
            selected = False
            if value.startswith("i-"):
                days = DateInterval.from_string(value[2:]).get_approx_days()
                if days > available_days:
                    # this interval is not interesting
                    continue
            else:
                if available_days == 1:
                    # if only one day is available, we cannot do diff
                    continue
                elif self.data_type.recurrent:
                    # for recurrent data, there is not point in computing diff
                    continue
            if value == self.req_time_processing:
                selected = True
            yield (value, name, selected)
            
    
    def get_valid_display_as_options(self):
        # disable pie chart option for charts using relative_scale_base,
        # because it hardly makes sense when sum would not be always 100%.
        if not self.relative_scale_base:
            return self.DISPLAY_AS_OPTS
        else:
            return self.DISPLAY_AS_OPTS[:-1]
    
    def get_valid_scaling_options(self):
        result = [("normal", ugettext("normal"))]
        log_ok = True
        # when relative_scale_base is set, we can calculate relative shares
        # even with no free dimensions displayed
        rel_ok = (self.dim_count() > 0) or self.relative_scale_base 
        if not self.time_sampling:
            if self.time_aggregate == Consts.TIME_AGGREG_DIFF:
                log_ok = False # there can be negative values in diff
            elif self.time_aggregate == Consts.TIME_AGGREG_DIFF_REL:
                log_ok = False # negative values again
                rel_ok = False # relative from relative data is too strange
        if not self.summable_series and not self.relative_scale_base:
            rel_ok = False
        if rel_ok:
            result.append(("percent", ugettext("relative in %")))
        if log_ok:
            result.append(("log", ugettext("logarithmic")))
        return result

    def get_non_slicing_dims(self):
        # return valid dimensions to select for slicing
        return list(set(self.dimensions) - set(self.slicing_dims) - set(self.enforced_slicing_dims))
    
    def get_slicing(self):
        # returns list of 6-tuples for generating slicing selects in following form
        # (dimension_code_name, dimension_name, dimension_key, value_key, legal_value_opts, preferred_slicing)
        result = []
        def get_values_as_pairs(slicing_dim_values):
            series = []
            if isinstance(slicing_dim_values, dict):
                # For slicing_dim.type == models.Dimension.TYPE_TEXT
                revd = [(value.lower(), value, key) for key,value in slicing_dim_values.iteritems()]
                revd.sort(key=lambda x: (x[2] == self.GROUPED_DIM_VALUE, locale.strxfrm(x[0].encode('utf-8'))))
                for _ignore, key, value in revd:
                    series.append((value, key))
            else:
                # For slicing_dim.type == models.Dimension.TYPE_INT
                # slicing_dim_values should be list of integers,
                # we will keep the values and keys the same except of mapping
                # special value self.GROUPED_DIM_VALUE as 'All'
                series = map(lambda x: (x, x if x != self.GROUPED_DIM_VALUE else ugettext("- all -")), slicing_dim_values)
            return series
        if len(self.slicing_dims) > 0:
            slicing_dim_values, selected_slicing = self.slicing_dims_values[self.slicing_dims[0]]
            result.append((self.slicing_dims[0].code_name,
                           self.slicing_dims[0].name,
                           self.KEY_SLICING_DIM,
                           self.KEY_SLICING_VALUE,
                           get_values_as_pairs(slicing_dim_values),
                           selected_slicing))
        if len(self.slicing_dims) > 1:
            slicing_dim_values, selected_slicing = self.slicing_dims_values[self.slicing_dims[1]]
            result.append((self.slicing_dims[1].code_name,
                           self.slicing_dims[1].name,
                           self.KEY_SLICING_DIM_2,
                           self.KEY_SLICING_VALUE_2,
                           get_values_as_pairs(slicing_dim_values),
                           selected_slicing))
        if len(self.slicing_dims) > 2:
            slicing_dim_values, selected_slicing = self.slicing_dims_values[self.slicing_dims[2]]
            result.append((self.slicing_dims[2].code_name,
                           self.slicing_dims[2].name,
                           self.KEY_SLICING_DIM_3,
                           self.KEY_SLICING_VALUE_3,
                           get_values_as_pairs(slicing_dim_values),
                           selected_slicing))
        return result

    def get_possible_series(self):
        if isinstance(self.possible_series, dict):
            # For self.series_dim.type == models.Dimension.TYPE_TEXT
            revd = [(value, key) for key,value in self.possible_series.iteritems()]
            revd.sort(key=lambda x: locale.strxfrm(x[0].encode('utf-8')))
        else:
            revd = map(lambda x: (x, x), self.possible_series)
        if self.series_set:
            zero_active = False
            series_set = self.series_set
        else:
            zero_active = True
            series_set = set()
        if len(revd) <= self.SERIES_LIMIT:
            series = [(0, ugettext("All"), zero_active)]
        else:
            if self.series_limit or series_set:
                series = [(-1, ugettext("All"), False),
                          (0, ugettext("Top %d") % self.SERIES_LIMIT, zero_active)]
            else:
                # case when all series is chosen
                series = [(-1, ugettext("All"), True),
                          (0, ugettext("Top %d") % self.SERIES_LIMIT, False)]
        for key, value in revd:
            series.append((value, key, value in series_set))
        return series
        
    def create_data_query(self):
        # fixed values for 4D data slicing
        fixed_values = []
        grouped_dims = []
        for dim in self.dimensions:
            if dim in self.slicing_dims:
                if self.slicing_dims_values[dim][1] != self.GROUPED_DIM_VALUE:
                    fixed_values.append(self.slicing_dims_values[dim][1])
                    grouped_dims.append(False)
                else:
                    fixed_values.append(None)
                    grouped_dims.append(True)
            elif dim in self.enforced_slicing_dims:
                if self.enforced_slicing_dims_values[dim] != self.GROUPED_DIM_VALUE:
                    fixed_values.append(self.enforced_slicing_dims_values[dim])
                    grouped_dims.append(False)
                else:
                    fixed_values.append(None)
                    grouped_dims.append(True)
            else:
                fixed_values.append(None)
                grouped_dims.append(False)
        args = (self.data_type, self.start_date, self.end_date)
        kwargs = dict(fixed_values=fixed_values,
                      time_aggregate=self.time_aggregate,
                      time_sampling=self.time_sampling,
                      grouped_dims=grouped_dims,
                      relative_scale_base=self.relative_scale_base)
        dqr = DataQuery(*args, **kwargs)
        return dqr
    
    def dim_count(self):
        return len(self.dimensions) - len(self.enforced_slicing_dims)
    
    def get_data(self):
        # preliminary checks
        if self.time_aggregate != Consts.TIME_AGGREG_NONE and \
           self.data_scaling != Consts.SCALE_NORMAL and \
           not self.time_sampling:
            # there are incompatible
            raise self.IncompatibleSettingsError()
        if not self.available_dates['min_date']:
            # there are not data for given data type and parameters
            raise DataRetreivalException("No data for data type %s and fixed values" % (self.data_type.code_name))
        dquery = self.create_data_query()
        bundle = dquery.fetch_results()
        # we get is here before any sampling may mangle the data
        real_start_time, real_end_time = bundle.get_stored_time_range() 
        logger.debug(bundle)
        # do postprocessing
        postprocessing_attrs = {}
        if self.series_set:
            postprocessing_attrs['series_set'] = self.series_set
        elif self.display_as == "table_all":
            # no series limit here
            pass
        elif self.series_limit:
            # there could be different series limits for different types of 
            # data, but for now, we use one value for all
            if "_time" in bundle.dimensions:
                postprocessing_attrs['limit_series'] = self.series_limit
            else:
                if self.time_aggregate == Consts.TIME_AGGREG_NONE:
                    postprocessing_attrs['limit_series'] = self.series_limit
                else:
                    postprocessing_attrs['limit_series'] = self.series_limit
        postprocessing_attrs['scale'] = self.data_scaling
        postprocessing_attrs['create_other'] = self.include_other
        if isinstance(bundle.key_dim, models.Dimension):
            postprocessing_attrs['sorting'] = bundle.key_dim.\
                dimensiontodatatype_set.get(data_type=self.data_type).sorting
        bundle.postprocess(**postprocessing_attrs)
        #pprint(bundle.data)
        # create dataTable
        series, series_desc = bundle.get_series_description()
        data_table = gviz_api.DataTable(series_desc)
        table_data = list(bundle.gen_data_table())
        data_table.LoadData(table_data)
        min_time = None
        max_time = None
        colors = None
        if bundle.key_dim == "_time": #series_desc["_main_key"][0] == "date":
            has_time_dim = True
            col_order, colors = self._sort_series_and_extract_color(series,
                                                                    series_desc)
            if table_data:
                min_time = real_start_time.date()
                max_time = real_end_time.date()
            order_data_by = "_main_key"
            dimension_x = series_desc["_main_key"][1]
            #self.dim_to_text(bundle.key_dim)
        else:
            has_time_dim = False
            col_order = sorted([key for key in series_desc if key != "_amount"],
                               reverse=True)
            col_order.append("_amount")
        if not min_time:
            if dquery.from_date:
                min_time = dquery.from_date
            else:
                min_time = self.start_date
        if not max_time:
            if dquery.to_date:
                max_time = dquery.to_date
            else:
                max_time = self.end_date
            order_data_by = None
            dimension_x = self.dim_to_text(bundle.key_dim)
        # amount dim modifier
        dim_y_mod = ""
        value_suffix = None
        if self.data_scaling == Consts.SCALE_RELATIVE_100:
            dim_y_mod = " - " + ugettext("Relative share [%]")
            value_suffix = "%"
        if self.time_aggregate == Consts.TIME_AGGREG_DIFF:
            dim_y_mod = " - "+ ugettext("Difference")
        elif self.time_aggregate == Consts.TIME_AGGREG_DIFF_REL:
            dim_y_mod = " - " + ugettext("Relative difference [%]")
            value_suffix = "%"
        result = self.ResultPackage()
        result.drq = self
        result.data_bundle = bundle
        result.data_table = data_table
        result.min_time = min_time
        result.max_time = max_time
        result.has_time_dim = has_time_dim
        result.column_order = col_order
        result.order_data_by = order_data_by
        result.dimension_x = dimension_x
        result.dimension_y = self.dim_to_text(bundle.amount_dim) + dim_y_mod
        result.value_suffix = value_suffix
        result.custom_colors = colors
        return result


    def _sort_series_and_extract_color(self, series, series_desc):
        if series:
            # lets see if we have some custom ordering
            # series are only set with 3D bundles
            to_sort = []
            for name, color, order in series:
                to_sort.append((order, name, color))
            # here we sort in reverse order because hicharts does the display
            # in opposite order
            to_sort.sort(
                    key=lambda x: (-x[0],
                                   locale.strxfrm(x[1].lower().encode('utf-8')))
                    )
            sorted_series = []
            colors = []
            some_color = False
            for _ord, name, color in to_sort:
                sorted_series.append(name)
                colors.append(color)
                if color:
                    some_color = True
            if not some_color:
                colors = []
        else:
            # 2D bundle and 1D bundle case
            sorted_series = sorted(
                    [key for key in series_desc if key != "_main_key"],
                    key=lambda x: locale.strxfrm(x.lower().encode('utf-8'))
                    )
            colors = []
        col_order = ["_main_key"] + sorted_series
        return col_order, colors
    

    def get_json_data(self):
        """
        Returns data suitable for json reply - does some more processing
        on the result of self.get_date
        """
        try:
            package = self.get_data()
        except DataRetreivalException:
            error = "Not enough data to display selected graph"
            ret = {"ok": False, "error": error}
            return ret
        except DataRequest.IncompatibleSettingsError:
            error = "Incompatible options selected"
            ret = {"ok": False, "error": error}
            return ret
        # serialize data
        json_data = package.data_table.ToJSon(columns_order=package.column_order,
                                              order_by=package.order_data_by)
        # chart title
        chart_title_parts = []
        for slicing_dim in self.slicing_dims:
            subvalue = self.slicing_dims_values[slicing_dim][1]
            if subvalue:
                try:
                    if subvalue == self.GROUPED_DIM_VALUE:
                        subvalue_text = slicing_dim.name + ': ' + ugettext("all")
                    elif slicing_dim and slicing_dim.type == models.Dimension.TYPE_TEXT:
                        subvalue_text = unicode(models.TextToIntMap.objects.get(id=subvalue))
                    else:
                        # dont try to remap integer dimensions
                        subvalue_text = str(subvalue)
                except models.TextToIntMap.DoesNotExist:
                    subvalue_text = str(subvalue)
                subvalue_text = subvalue_text.replace('IPv6', '<span style="text-transform: none">IPv6</span>')
                chart_title_parts.append(subvalue_text)

        if (package.min_time == package.max_time):
            chart_title_parts.append(str(package.max_time))
        elif package.min_time and package.max_time:
            # min_time != max_time and both are defined
            t_rounding = self.get_time_rounding()
            part = _("from %(start_date)s to %(end_date)s") %\
                    {"start_date": DateManipulation.str_date(package.min_time,
                                                        rounding=t_rounding),
                    "end_date":  DateManipulation.str_date(package.max_time,
                                                        rounding=t_rounding)}
            if not chart_title_parts:
                part = part.capitalize()
            chart_title_parts.append(part)
        elif package.max_time:
            # the only case when min_time is None is when there are no data at all the given time range
            t_rounding = self.get_time_rounding()
            part = _("until %(end_date)s") %\
                    {"end_date":  DateManipulation.str_date(package.max_time,
                                                        rounding=t_rounding)}
            if not chart_title_parts:
                part = part.capitalize()
            chart_title_parts.append(part)
            
        chart_title = ", ".join(chart_title_parts)
        if self.r2s:
            chart_title = self.r2s.name or self.data_type.name 
            
        # chart type
        chart_type = None
        if self.display_as in ("table","table_all"):
            chart_type = "table"
        else:
            if package.has_time_dim:
                if self.display_as in ("column_chart", "line_chart", "area_chart"):
                    chart_type = self.display_as
                    if (self.client_side_scaling == 'log') and (self.display_as == 'area_chart'):
                        # don't use area chart with log scale as it cannot use
                        # stacking and the series will cover each other, use line chart  
                        chart_type = "line_chart"
                    elif (self.display_as == 'area_chart') and (len(package.data_bundle.data) == 1):
                        # don't use area chart for one time moment, only for time range
                        chart_type = "column_chart"
                elif len(package.data_bundle.data) < 10:
                    chart_type = "column_chart"
                else:
                    chart_type = "line_chart"
            else:
                if self.display_as in ("pie_chart", "column_chart", "bar_chart"):
                    chart_type = self.display_as
                elif self.time_aggregate == Consts.TIME_AGGREG_NONE:
                    chart_type = "column_chart"
                else:
                    chart_type = "bar_chart"
        # notes
        notes = []
        if self.time_aggregate in (Consts.TIME_AGGREG_DIFF,
                                   Consts.TIME_AGGREG_DIFF_REL):
            # in case of computing difference, it is important to warn that the
            # data does not fully match
            if self.end_date and self.end_date != package.max_time:
                notes.append(ugettext("Requested end time could not have "
                                      "been fully matched."))
            if self.start_date and self.start_date != package.min_time:
                notes.append(ugettext("Requested start time could not have "
                                      "been fully matched."))
        # problem description
        if package.data_table.NumberOfRows() == 0:
            if self.r2s:
                chart_text = ugettext("No data available for selected report.")
            elif self.series_set:
                chart_text = ugettext("No data available for selected time "
                                      "and series.")
            else:
                chart_text = ugettext("No data available for selected time.")
        else:
            chart_text = None
        # serialize and return the data
        t_rounding = self.get_time_rounding()
        ret = {"ok": True,
               "data": json_data,
               "has_time": package.has_time_dim,
               "dimension_x": package.dimension_x if chart_type != "bar_chart" else package.dimension_y,
               "dimension_y": package.dimension_y if chart_type != "bar_chart" else package.dimension_x,
               "value_suffix": package.value_suffix,
               "summable_series": self.summable_series,
               "percent": (self.req_data_scaling == "percent") or (self.req_time_processing == "reldiff"),
               "chart_title": chart_title,
               "chart_type": chart_type,
               "notes": notes,
               "chart_text": chart_text,
               "custom_colors": package.custom_colors,
               "available_min_date": self.available_dates['min_date'].date(),
               "available_max_date": self.available_dates['max_date'].date(),
               "client_side_scaling": self.client_side_scaling
               }
        return ret


    @classmethod
    def dim_to_text(cls, dim):
        if isinstance(dim, models.Dimension):
            return dim.name
        elif dim is None:
            return ""
        else:
            return unicode(cls.DIM_KEY_REMAP.get(dim, dim))

if __name__ == "__main__":
    from pprint import pprint
    from unifiedstatistics.common import querydict_to_dict
    from urlparse import parse_qs
    from django.utils.translation import activate
    
    activate('cs')
    params = querydict_to_dict(parse_qs('lang=en&sd=country_code&sv=10&sd2=geo_level&sv2=2&sd3=service&sv3=3&rd=2013-12-31&dr=0d&tp=i-1d&ss=0&ds=normal&da=table'))
    dr = DataRequest(params, 'gen6') 
    pprint(params)
    pprint(dr.slicing_dims_values)
    
