"""
Code that is to be shared between different parts of the data processing
workflow.
"""

import re
import datetime
import calendar
import hashlib
from dateutil.relativedelta import relativedelta

# Django imports
from django.core.cache import cache
from django.contrib.auth.models import Group
from django.utils.translation import check_for_language
from django.conf import settings

def create_cache_key(group, *params):
    hasher = hashlib.md5()
    for param in params:
        hasher.update(unicode(param).encode('utf-8'))
    key = group + "__" + hasher.hexdigest()
    return key

def get_public_group():
    """Returns cached public group"""
    cache_key = create_cache_key("main", "public_group")
    public_group = cache.get(cache_key)
    if not public_group:
        public_group, _unused = Group.objects.get_or_create(name='public')
        cache.set(cache_key, public_group)
    return public_group

def querydict_to_dict(qdict):
    result = dict(qdict)
    for key in result.keys():
        value = result[key]
        if type(value) is list and len(value) == 1:
            result[key] = value[0]
        else:
            result[key] = value
    return result

def get_language_from_request(request):
    """
    Got from django.utils.tranlations.trans_real and rewritten to not use
    browser settings and instead of django.session it uses cookie language_code.
    Analyzes the request to find what language the user wants the system to
    show. Only languages listed in settings.LANGUAGES are taken into account.
    If the user requests a sublanguage where we have a main language, we send
    out the main language.
    """
    supported = dict(settings.LANGUAGES)
    lang_code = request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME)
    if lang_code and lang_code not in supported:
        lang_code = lang_code.split('-')[0] # e.g. if fr-ca is not supported fallback to fr
    if lang_code and lang_code in supported and check_for_language(lang_code):
        return lang_code
    return settings.LANGUAGE_CODE

class Consts:
    
    # constants for data scaling
    SCALE_NORMAL = 0
    SCALE_RELATIVE = 1
    SCALE_RELATIVE_100 = 2
    
    # client scaling methods
    CLIENT_SCALE_NORMAL = 0
    CLIENT_SCALE_LOG = 1
    
    # constants for TIME_AGGREGATION
    TIME_AGGREG_NONE = 0  # do not aggregate, output separate values
    TIME_AGGREG_LAST = 1  # take last value
    TIME_AGGREG_AVG = 2   # compute average value
    TIME_AGGREG_MIN = 3   # compute min value
    TIME_AGGREG_MAX = 4   # compute max value
    TIME_AGGREG_DIFF = 5  # compute difference last-first
    TIME_AGGREG_DIFF_REL = 6 # compute difference last-first in relative numbers
    TIME_AGGREG_SUM = 7   # sum all values
    TIME_AGGREG_MEDIAN = 8  # median
    TIME_AGGREG_MIDDLE = 9  # value from the middle of the interval
    TIME_AGGREG_FIRST = 10  # take first value


class DateManipulation(object):
    DATE_RANGE_MATCHER = re.compile("^(\d+)([dwmy])$")
    ISO_DATE_MATCHER = re.compile("^(\d{4})-(\d{2})-(\d{2})$")
    
    
    @classmethod
    def round_time(cls, date, rounding, up=False):
        """
        rounds to the start of the rounding span; if up is given, rounds to
        the end;
        """
        if not date:
            return date
        if rounding == "m":
            if up:
                _wd, max_day = calendar.monthrange(date.year, date.month)
                date = date.replace(day=max_day)
            else:
                date = date.replace(day=1)
        elif rounding == "y":
            if up:
                date = date.replace(day=31, month=12)
            else:
                date = date.replace(day=1, month=1)
        elif rounding == "d":
            pass
        else:
            raise ValueError("Wrong rounding definition '%s'", rounding) 
        return date
    
    @classmethod
    def str_date(cls, date, rounding=None):
        if isinstance(date, datetime.datetime):
            date = date.date()
        if rounding in (None, 'd'):
            return date.isoformat()
        elif rounding == "m":
            return date.strftime("%Y-%m")
        elif rounding == "y":
            return date.strftime("%Y")
        return date.isoformat()
    
    @classmethod
    def parse_time_range(cls, ref_date, range_def, rounding=None):
        """
        rounding can be one of (d,m,y) or None
        """
        if ref_date is None:
            return None
        if range_def == "all":
            return None
        iso_date_match = cls.ISO_DATE_MATCHER.match(range_def)
        if iso_date_match:
            # it is an absolute date
            year, month, day = map(int, iso_date_match.groups())
            start_date = datetime.date(year=year, month=month, day=day)
        else:
            try:
                interval = DateInterval.from_string(range_def)
            except ValueError:
                # default all if we cannot parse given interval
                return None
            if rounding:
                ref_date = cls.round_time(ref_date, rounding, up=True)
            if range_def != "0d":
                # Add one day to have exactly one month/year/day after adjusting
                # Ranges 0d and 1d have the same meaning, 0d is kept for
                # backward compatibility with existing URLs
                ref_date += relativedelta(days=1) 
            start_date = interval.adjust_date(ref_date, direction=-1)
        # now do rounding
        if rounding:
            start_date = cls.round_time(start_date, rounding)
        return start_date
    
    @classmethod
    def get_time_breaker_fn(cls, sampling_method):
        fn_name = "_breaker_"+sampling_method
        if hasattr(cls, fn_name):
            return getattr(cls, fn_name)
        raise ValueError("no breaker function implemented for this "
                         "sampling_method")
    
    @staticmethod
    def _breaker_1w(last_date, new_date):
        # (ISO year, ISO week number, ISO weekday)
        last_year, last_week_num, _last_weekday = last_date.isocalendar()
        new_year, new_week_num, _new_weekday = new_date.isocalendar()
        if last_week_num != new_week_num:
            return True
        elif last_year != new_year:
            return True
        return False
    
    @staticmethod
    def _breaker_1m(last_date, new_date):
        if last_date.month != new_date.month:
            return True
        elif last_date.year != new_date.year:
            return True
        return False
    
    @staticmethod
    def _breaker_1y(last_date, new_date):
        if last_date.year != new_date.year:
            return True
        return False

        
class DateInterval(object):
    """
    Represents a human friendly definition of date interval, such as 1 week,
    3 month
    """
    
    DATE_RANGE_MATCHER = re.compile("^(\d+)([dwmy])$")
    UNIT_TO_DAYS = {'d': 1, 'w': 7, 'm': 30, 'y': 365}
    
    def __init__(self, count, unit):
        self.count = count
        self.unit = unit
        assert(type(self.count) is int)
        assert(self.unit in "dwmy")
        
    @classmethod
    def from_string(cls, text):
        match = cls.DATE_RANGE_MATCHER.match(text)
        if match:
            count = int(match.group(1))
            unit = match.group(2)
        else:
            raise ValueError("Cannot parse input value '%s'" % text)
        new = cls(count, unit)
        return new
    
    def adjust_date(self, start_date, direction=1):
        """
        >>> x = DateInterval.from_string("3m")
        >>> x.adjust_date(datetime.date(2010, 2, 12), direction=1)
        datetime.date(2010, 5, 12)
        >>> x.adjust_date(datetime.date(2010, 2, 12), direction=-1)
        datetime.date(2009, 11, 12)
        >>> x.adjust_date(datetime.date(2010, 12, 12), direction=1)
        datetime.date(2011, 3, 12)
        >>> x.adjust_date(datetime.date(2010, 8, 31), direction=1)
        datetime.date(2010, 11, 30)
        """
        if self.unit == "d":
            start_date += relativedelta(days=direction*self.count)
        elif self.unit == "w":
            start_date += relativedelta(weeks=direction*self.count)
        elif self.unit == "m":
            start_date += relativedelta(months=direction*self.count)
        elif self.unit == "y":
            start_date += relativedelta(years=direction*self.count)
        return start_date
    
    def get_approx_days(self):
        return self.count * self.UNIT_TO_DAYS.get(self.unit, 1)
 
    def get_approx_timedelta(self):
        return datetime.timedelta(days=self.get_approx_days())