function state_params(exclude_lang) {
	if (exclude_lang === false)
		return $('#chart_params *').not('#lang_dummy_param').serialize();
	else
		return $('#chart_params *').serialize();
}

function show_error(text) {
	chart_package.show_error(text);
}

function clean_error() {
	chart_package.clean_error();
}

function time_processing_changed() {
	var time_processing = $("#time_processing").find(":selected").val();
    if (typeof(time_processing) != 'undefined') {
        if (time_processing.slice(0, 2) != "i-") {
            /* scaling is not compatible with time_processing other than normal */
            $("#scale_select > option[value='normal']").prop('selected', true);
            $("#scale_select").prop('disabled', true);
        } else {
            $("#scale_select").prop('disabled', false);
        }
    }
	update_chart();
}

function setup_series_select() {
    $("#series_select").dropdownchecklist({
        maxDropHeight: 400,
        width: 78,
        onComplete: update_chart,
        onItemClick: function (checkbox, selector) {
            var enabled = checkbox.prop("checked");
            var current_val = $(checkbox).val();
            if (current_val == "0" || current_val == "-1") {
                /* disable other when all is selected */
                $('option[value=' + current_val + ']', selector).siblings().prop('disabled', enabled);
                $('option[value=' + current_val + ']', selector).prop('selected', enabled);
                if (enabled) {
                    var other_val = current_val == "0" ? "-1" : "0";
                    $('option[value=' + other_val + ']', selector).prop('selected', '');
                }
                $(selector).dropdownchecklist("refresh");
            } else if (enabled) {
                /* deselect all if some thing else is selected */
                $('option[value=0]', selector).prop('selected', false);
                $('option[value=-1]', selector).prop('selected', false);
                /* propagate the select */
                $("option[value='" + current_val + "']", selector).prop('selected', true);
                $(selector).dropdownchecklist("refresh");
            }
        },
        textFormatFunction: function (options) {
            var selector = "#series_select";
            var selected = $(selector).val();
            if (selected == null)
                return "&nbsp;";
            for (var i = 0; i < selected.length; i++) {
                if (selected[i] == "0")
                    return $(selector).find("option[value='0']").html();
                else if (selected[i] == "-1")
                    return $(selector).find("option[value='-1']").html();
            }
            /* normal processing */
            var text = "";
            $(selector).find("option:selected").each(function () {
                if (text != "") {
                    text += ", ";
                }
                var optCss = $(this).attr('style');
                var tempspan = $('<span/>');
                tempspan.html($(this).html());
                if (optCss == null) {
                    text += tempspan.html();
                } else {
                    tempspan.attr('style', optCss);
                    text += $("<span/>").append(tempspan).html();
                }
            });
            return text || "&nbsp;";
        }
    });

    /* initial setting of disabled options after loading page */
    $('#series_select').find(':selected').each(
        function (idx, el) {
            var current_val = $(el).val();
            if (current_val == "0" || current_val == "-1") {
                $('#series_select').find('option[value=' + current_val + ']').siblings().prop('disabled', true);
                $('#series_select').dropdownchecklist("refresh");
            }
        }
    );
}

function update_series() {
	$("#series_select").prop('disabled', true);
	jQuery.ajax({
		dataType: "json",
		url: "json/series/?"+state_params(),
		success: function(response){
			var data = response.data;
			var series = data.series;
			if (response.ok && data != null) {
				var opts = '';
				for (var i=0; i<series.length; i++) {
					if (series[i][2] == true)
						opts += '<option value="'+series[i][0]+'" selected="selected">'+series[i][1]+'</option>';
					else
						opts += '<option value="'+series[i][0]+'">'+series[i][1]+'</option>';
				}
				$("#series_select").dropdownchecklist("destroy");
				$("#series_select").html(opts);
				$("#series_select").prop('disabled', false);
				setup_series_select();
				/* use the scales from the response */
				var new_scales = "";
				var old_scale = $("#scale_select").val();
				for (i=0; i<data.scales.length; i++) {
					new_scales += "<option value='"+data.scales[i][0]+"'>"+
						data.scales[i][1]+"</option>";
				}
				$("#scale_select").html(new_scales);
				$("#scale_select").val(old_scale);
				update_chart();
			} else {
				show_error("<b>No data returned from server.</b>");
			}
		},
		error:  function(jqXHR, textStatus, errorThrown) {
			show_error("<b>Error occured when fetching data</b>: "+
						errorThrown);
		}});
	
}


function date_range_changed(update) {
	var date_range= $("#date_range").val();
	var current_tp = $("#time_processing").val();
	var diff_ok = true;
	var m_ok = true;
	var y_ok = true;
	var pie_ok = false;
	if (date_range == "0d") {
		diff_ok = false;
		m_ok = false;
		y_ok = false;
                pie_ok = true;
	} else if (date_range.charAt(date_range.length-1) == 'm') {
		y_ok = false;
	}
	// disable incompatible time_processing options
	// diff
	if (!diff_ok)	 
		$("#time_processing > option[value*='diff']").prop('disabled', true);
	else
		$("#time_processing > option[value*='diff']").prop('disabled', false);
	// months
	if (!m_ok)
 		$("#time_processing > option[value='i-1m']").prop('disabled', true);
	else
		$("#time_processing > option[value='i-1m']").prop('disabled', false);
	// years
	if (!y_ok)
 		$("#time_processing > option[value='i-1y']").prop('disabled', true);
	else
		$("#time_processing > option[value='i-1y']").prop('disabled', false);
	// pie chart
	if (!pie_ok)
 		$("#display_as_select > option[value='pie_chart']").prop('disabled', true);
	else
		$("#display_as_select > option[value='pie_chart']").prop('disabled', false);
	// reselect
	if ($("#time_processing > option[value='"+current_tp+"']").prop('disabled'))
		$("#time_processing > option[value='i-1d']").prop('selected', true);
	// update
	if (update !== false) {
		update_chart();
	}
}

function slice_dim_changed() {
    var url = "json/slices/?" + state_params();
    $('#header_select select').prop('disabled', true);
	jQuery.ajax({
		dataType: "json",
		url: url,
		success: function(response) {
			var data = response.data;
			var non_slicing_opts = '';
			for (var i=0; i < response.non_slicing.length; i++) {
			    non_slicing_opts += '<option value="'+response.non_slicing[i][0]+'">'+response.non_slicing[i][1]+'</option>';
			}
			if (response.ok && data != null) {
				for (var i=0; i < data.length; i++) {
				    var dim_opts = '<option value="'+data[i][0]+'">'+data[i][1]+'</option>' + non_slicing_opts;
				    var value_opts = '';
                    for (var j=0; j < data[i][4].length; j++) {
                        value_opts += '<option value="'+data[i][4][j][0]+'">'+data[i][4][j][1]+'</option>';
                    }
				    $("#" + data[i][2] + "_select").html(dim_opts);
                    $("#" + data[i][2] + "_select").val(data[i][0]);
                    $("#" + data[i][3] + "_select").html(value_opts);
                    $("#" + data[i][3] + "_select").val(data[i][5]);
				}
                $('#header_select select').prop('disabled', false);
				clean_error();
				update_series();
			} else {
				show_error("<b>No data returned from server.</b>");
			}
		},
		error:  function(jqXHR, textStatus, errorThrown) {
			show_error("<b>Error occured when fetching data</b>: "+
						errorThrown);
		}});
}

function ref_date_changed() {
	update_chart();
}

function update_chart() {
	//console.log('updating '+state_params());
	update_permalink();
	chart_package.show_chart("json/data/?"+state_params());
}

function download_csv() {
	var url = "csv/?"+state_params();
	window.open(url, '_blank');
}

function update_permalink(event) {
	var href=document.URL.split("?")[0]+"?"+state_params(false);
	$("#chart_permalink").attr({ "href": href });
}
