function ChartPackage(chart_element, error_element, title_element, width, height, language, datepicker_element) {
	this.chart_element = chart_element;
	this.error_element = error_element;
	this.title_element = title_element;
	this.width = width ? width : -1;
	this.height = height ? height : -1;
	this.info_element = this.error_element;
	this.chart = null;
	this.language = language;
	this.datepicker_element = datepicker_element;
}

ChartPackage.prototype.set_chart_element = function(el) {
	this.chart_element = el;
};

ChartPackage.prototype.set_error_element = function(el) {
	this.error_element = el;
};

ChartPackage.prototype.set_chart = function(chart_ref, obj) {
	obj.chart = chart_ref;
};

ChartPackage.prototype.show_info = function (text) {
	if (this.info_element != this.chart_element)
		/* do not use it if it is the same as the chart element */
		$(this.info_element).html(text).show(500).delay(5000).hide(500);
}


ChartPackage.prototype.show_error = function (text) {
	$(this.error_element).html(text);
}

ChartPackage.prototype.clean_error = function (text) {
	$(this.error_element).html("");
}

ChartPackage.prototype.show_chart = function (url) {
	$(this.chart_element).empty().addClass("loading");
	jQuery.ajax({
		dataType: "json",
		url: url,
		success: this.handle_chart_response.bind(this),
		error: this.handle_chart_response_error.bind(this)  
	});
}	


ChartPackage.prototype.handle_chart_response = function(response){
	var el = this.chart_element;
	var data = null;
	eval('data =' + response.data);
	if (response.ok && data != null) {
		this.clean_error();
		/* update datepicker limits */
		if (this.datepicker_element != null) {
    		if (response.available_min_date != null) {
                $(this.datepicker_element).datepicker("option", "minDate", response.available_min_date);
    		}
            if (response.available_max_date != null) {
                $(this.datepicker_element).datepicker("option", "maxDate", response.available_max_date);
            }
		}
		/* notes */
		var note_text = "";
		for (var i=0; i < response.notes.length; i++) {
			note_text += "<div>"+response.notes[i]+"</div>";
		}
		if (note_text != "")
			this.show_info(note_text);
		/* the title */
		var title = response.chart_title;
		if (this.title_element != null)
			$(this.title_element).html(title);
		/* the chart */
		var datatable = new google.visualization.DataTable(data, 0.6);
		if (datatable.getNumberOfRows() == 0) {
			/* short-circuit in case no data was found */
			var info_text = response.chart_text;
			if (info_text == null)
				info_text = "No data was received";
			$(el).removeClass("loading");
			$(el).html("<div class='info'><strong>"+info_text+"</strong></div>");
			return
		}
		if (response.percent == true) {
			var formatter = new google.visualization.NumberFormat({suffix: ' %'});
			for (var i=1; i<datatable.getNumberOfColumns(); i++)
				formatter.format(datatable, i);
		} else {
			var formatter = new google.visualization.NumberFormat({pattern:'#,###'});
			for (var i=1; i<datatable.getNumberOfColumns(); i++)
				if (datatable.getColumnType(i) == "number")
					formatter.format(datatable, i);
		}
		var vaxis_params = {title: response.dimension_y};
		var haxis_params = {title: response.dimension_x};
		var chart_type = response.chart_type;
		var point_size = null;
		var line_width = null;
		/* decide on chart type */
		if (chart_type == "bar_chart") {
			/* BarChart is rotated */
			vaxis_params.title = response.dimension_x;
			haxis_params.title = response.dimension_y;
		}
		
		/* client side scaling */
		if (response.client_side_scaling == "log") {
			vaxis_params.logScale = true;
			vaxis_params.format = "#";
		}
		/* decide on width */
		var width = this.width;
		if (width < 0)
			width = this.calculate_available_width();
        
        width = Math.min(1100, width);
        
        /*
		var height = this.height;
		if (height < 0)
			height = Math.floor(width / 1.618);
        */
        var height = Math.floor(width * 0.9);
		
		/* show the chart */
		$(el).removeClass("loading");
		if (chart_type == "table") {
			// reverse order for time data to put newest data up
            if (response.has_time) {
                datatable.sort({column: 0, desc: true});
            }
			this.insert_simple_table(el, datatable);
		} else {
			var params = {
					width: width,
					height: height,
					title: title,
					isStacked: response.summable_series,
					percent: response.percent,
					vAxis: vaxis_params,
					hAxis: haxis_params
				}
			if ((response.custom_colors != undefined) && (response.custom_colors.length > 0)) {
				params['colors'] = response.custom_colors;
			}
			
			
			// OGRA JS
			// Czech localization
			if (this.language == 'cs') {
				params['lang'] = {
					months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 
						'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
					shortMonths: ['led', 'úno', 'bře', 'dub', 'kvě', 'čer', 
						'čnc', 'srp', 'zář', 'říj', 'lis', 'pro'],
					//numericSymbols: [' tis.', ' mil.', ' mld.'],
					// numericSymbols: [' 000', ' 000 000', ' 000 000 000'],
					numericSymbols: null,
					thousandsSep: ' ',
					decimalPoint: ',',
					downloadJPEG: 'Stáhnout jako JPEG obrázek',
					downloadPDF: 'Stáhnout jako PDF dokument',
					downloadPNG: 'Stáhnout jako PNG obrázek',
					downloadSVG: 'Stáhnout jako SVG vektorovou grafiku',
					loading: 'Načítání...',
					contextButtonTitle: 'Exportovat nebo tisknout graf',
					printChart: 'Vytisknout graf',
					total: 'Celkem'
				}
			} else {
				params['lang'] = {
					// numericSymbols: [' 000', ' 000 000', ' 000 000 000'],
                    numericSymbols: null,
					thousandsSep: ' '
				}
			}

			// credits
			params['credits_text'] = "CZ.NIC - https://stats.nic.cz/"; 
			
			if (typeof(state_params) == 'undefined') {
				params['credits_href'] = ""; 
			} else {
				var my_url = document.URL.split("?")[0]+"?"+state_params(false);
				params['credits_href'] = my_url; 
			}

			var ogra_chart_type = response.chart_type.substr(0, response.chart_type.length-6);
			
			// correction of line stacking
			if (ogra_chart_type == "line") {
                params.isStacked = false;
            }
            
            var used_library = "high";
			
			// getting reference to chart
			if (used_library == "high") {
				params['callback'] = this.set_chart;
				params['callback_args'] = [this];
				params['dateTimeLabelFormats'] = {
                        millisecond: ' ',
                        second: ' ',
                        minute: ' ',
                        hour: ' ',
                        day: '%e. %b',
                        week: '%e. %b',
                        month: '%b \'%y',
                        year: '%Y'
                };
                params['legend'] = {
                        align: 'center',
                        layout: 'horizontal',
                        verticalAlign: 'bottom'
                };
                params['min_column_height'] = 2;
                params['pie_chart_size'] = '50%';
			}
			
			// creating graph by OGRA
			Ogra.graph(el.id, data, ogra_chart_type, used_library, params);
		}
	} else {
		$(el).removeClass("loading");
		if (response.error != null)
			this.show_error("<b>"+response.error+"</b>");
		else
			this.show_error("<b>No data returned from server.</b>");
	}
}

ChartPackage.prototype.handle_chart_response_error = function(jqXHR, textStatus,
		errorThrown) {
	var el = this.chart_element;
	$(el).removeClass("loading");
	this.show_error("<b>Error occured when fetching data</b>: "+
			errorThrown);
}

ChartPackage.prototype.insert_simple_table = function(el, datatable) {
	/* here we paste the whole text together and the commit
	 * it to the table - it is faster that adding individual elements
	 */
	var table = "<table class='result_table'><thead><tr>";
	for (var j=0; j<datatable.getNumberOfColumns(); j++) {
		table += "<th>"+datatable.getColumnLabel(j)+"</th>";
	}
	table += "</tr></thead><tbody>";
	for (var i=0; i<datatable.getNumberOfRows(); i++) {
		table += "<tr>";
		for (var j=0; j<datatable.getNumberOfColumns(); j++) {
			if (datatable.getColumnType(j) != "number")
				table += "<td class='left'>";
			else
				table += "<td>";
			table += datatable.getFormattedValue(i, j)+"</td>";
		}
		table += "</tr>";
	}
	table += "</tbody></table>";
	$(el).append(table);
}

ChartPackage.prototype.calculate_available_width = function() {
	return $(this.chart_element).innerWidth();
}
