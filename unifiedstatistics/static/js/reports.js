function state_params() {
    return $('#lang_dummy_param').serialize();
}

function update_chart(stat, pos) {
    chart_packages[pos].show_chart(stat + "/" + pos + "/json/data/?" + state_params());
}

function download_csv(stat, pos) {
    var url = stat + "/" + pos + "/csv/?" + state_params();
    window.open(url, '_blank');
}

function setup_chart(stat, pos, lang) {
    chart_packages[pos] = new ChartPackage(document.getElementById("chart_div_" + pos),
                                     document.getElementById("chart_div_" + pos),
                                     document.getElementById("chart_title_" + pos),
                                     undefined, undefined, lang, undefined);
    chart_packages[pos].info_element = document.getElementById("json_info_" + pos);
    update_chart(stat, pos);
}
