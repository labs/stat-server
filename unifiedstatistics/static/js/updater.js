// refresh graph with data specified in html form
update = function(elem_id, server_par, args) {
    /*
     * elem_id - place for new graph
     * args - list of ids of html elements
     *      - elements should be html form fields with filled value
     */

    type = "high";
    
    // generating url
    //var data_url = "/generate_data?";
    var data_url = "/stats/generate_data_hosting";
    
    // parsing args
    for (var i = 0; i < args.length ; i++) {
        var e = document.getElementById(args[i]);
        if (e == null) {
            console.log('Refresh Error: Invalid ID. Element "' + args[i] + '" not found.');
            return false;
        } else if (e.value == false) {
            console.log('Refresh Error: Invalid value of element element "' + args[i] + '".');
            return false;
        }
        
        // pulling known parameters
        if (args[i] == "chart_type") {
            chart_type = e.value;
            
            console.log(chart_type);
            
            // exception for table
            if (chart_type == "table") {
                type = "google";
                for (var k in server_par) {
                    if (k == "limit") {
                        //server_par[k] = 9999;
                        delete server_par[k]
                    }
                }
            }
            
        } else if (args[i] == "type") {
            type = e.value;
        } else {
            // generating url
            if (i == 0) {
                data_url += "?";
            } else {
                data_url += "&";
            }
            data_url += args[i] + "=" + e.value;
        }
    }
    
    // parsing parameters
    for (var k in server_par) {
        data_url += "&" + k + "=" + server_par[k];
    }
    
    // get element
    var element = document.getElementById(elem_id);
    if (element == null) {
        console.log('Refresh Error: Invalid ID. Element "' + elem_id + '" not found.');
        return false;
    }
    
    // empty element
    element.innerHTML = "";
   
    console.log(type);
    // create new graph
    //Ogra.graph(elem_id, data_url, chart_type, type);
    Ogra.graph(elem_id, data_url, chart_type, type, {'callback': csv_button, 'callback_args': [elem_id, data_url]});
    
    return true;
}

// adds button to download data in csv file
csv_button = function(elem_id, url) {
    
    //TODO check if element exists? who can call this method?
    
    // create button    
    var b = document.createElement('button');
    b.appendChild(document.createTextNode("Stáhnout zdrojová data jako CSV"));
    
    var element = document.getElementById(elem_id);
    element.insertBefore(b, element.firstChild);
    
    // onclick method
    b.onclick = function() {
        var url_csv = url + "&output=csv";
        
        var iframe = document.getElementById("hiddenDownloader");
        
        if (iframe == null) {
            iframe = document.createElement('iframe');
            iframe.id = "hiddenDownloader";
            iframe.style.visibility = 'hidden';
            document.body.appendChild(iframe);
        }
        iframe.src = url_csv;
    }
}