# django imports
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.core.urlresolvers import reverse

# third party imports
from guardian.admin import GuardedModelAdmin

# local imports
from unifiedstatistics import models


class AccessKeyAdmin(admin.ModelAdmin):
    list_display = ['group', 'secret', 'is_valid', 'remaining_uses', 'valid_until']


class StatisticsAdmin(GuardedModelAdmin):
    pass


def roles(self):
    p = self.groups.values_list('name', flat=True)
    return ', '.join(p)
roles.short_description = u'Groups'


def last(self):
    fmt = "%Y-%m-%d %H:%M:%S"
    return self.last_login.strftime(fmt)
last.allow_tags = True
last.admin_order_field = 'last_login'


def adm(self):
    return self.is_superuser
adm.boolean = True
adm.admin_order_field = 'is_superuser'


def staff(self):
    return self.is_staff
staff.boolean = True
staff.admin_order_field = 'is_staff'


def persons(self):
    return ', '.join(['<a href="%s">%s</a>' % (reverse('admin:auth_user_change', args=(x.id,)), x.username) for x in self.user_set.all().order_by('username')])
persons.allow_tags = True


class UserAdmin(UserAdmin):
    list_display = ['username', 'email', 'first_name', 'last_name', 'is_active', staff, adm, roles, last]
    list_filter = ['groups', 'is_staff', 'is_superuser', 'is_active']


class GroupAdmin(GroupAdmin):
    list_display = ['name', persons]
    list_display_links = ['name']

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)

admin.site.register(models.Report)
admin.site.register(models.Statistics, StatisticsAdmin)
admin.site.register(models.AccessKey, AccessKeyAdmin)
