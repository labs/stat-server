from django.conf.urls import patterns, url
import views


urlpatterns = patterns('',
    url(r'^$', views.dashboard, name='dashboard'),

    url(r'^stats/$', views.all_statistics),
    url(r'^stats/(?P<stat_code_name>\w+)/$', views.statistics_page),
    url(r'^stats/(?P<stat_code_name>\w+)/json/data/$', views.json_data),
    url(r'^stats/(?P<stat_code_name>\w+)/json/slices/$', views.json_slices),
    url(r'^stats/(?P<stat_code_name>\w+)/json/series/$', views.json_series),
    url(r'^stats/(?P<stat_code_name>\w+)/csv/$', views.csv_data),
    
    url(r'^dashchart/(?P<ref_code_name>\w+)/json/data/$', views.dashchart_data),
    
    url(r'^reports/$', views.reports),
    url(r'^reports/(?P<report_code_name>\w+)/$', views.report_page, name='report-page'),
    url(r'^reports/(?P<report_code_name>\w+)/(?P<stat_code_name>\w+)/(?P<position>\d+)/json/data/$', views.json_data),
    
    url(r'^animation_test/$', views.animation_test),
    
    url(r'^logout/$', views.logout_view),
    url(r'^login/$', views.login_view),
    url(r'^access/$', views.set_access_view),

    #url(r'^crash/$', views.crash),
)
