# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StatGroup'
        db.create_table('stats_statgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('info_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('info_en', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('stats', ['StatGroup'])

        # Adding model 'StatGroupToStatistics'
        db.create_table('stats_statgrouptostatistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stat_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.StatGroup'])),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Statistics'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('stats', ['StatGroupToStatistics'])

        # Adding unique constraint on 'StatGroupToStatistics', fields ['stat_group', 'statistics', 'position']
        db.create_unique('stats_statgrouptostatistics', ['stat_group_id', 'statistics_id', 'position'])

        # Adding model 'Statistics'
        db.create_table('stats_statistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.DataType'])),
            ('info_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('info_en', self.gf('django.db.models.fields.TextField')()),
            ('methodology_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('methodology_en', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('stats', ['Statistics'])

        # Adding model 'DataType'
        db.create_table('stats_datatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('amount_dim', self.gf('django.db.models.fields.related.ForeignKey')(related_name='amounttodatatype_set', to=orm['stats.Dimension'])),
        ))
        db.send_create_signal('stats', ['DataType'])

        # Adding model 'Dimension'
        db.create_table('stats_dimension', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('stats', ['Dimension'])

        # Adding model 'DimensionToDataType'
        db.create_table('stats_dimensiontodatatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.DataType'])),
            ('dimension', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Dimension'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('stats', ['DimensionToDataType'])

        # Adding unique constraint on 'DimensionToDataType', fields ['data_type', 'dimension', 'position']
        db.create_unique('stats_dimensiontodatatype', ['data_type_id', 'dimension_id', 'position'])

        # Adding model 'DataPoint'
        db.create_table('stats_datapoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.DataType'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('amount', self.gf('django.db.models.fields.BigIntegerField')()),
            ('value1', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value2', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value3', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal('stats', ['DataPoint'])

        # Adding unique constraint on 'DataPoint', fields ['data_type', 'date', 'value1', 'value2', 'value3']
        db.create_unique('stats_datapoint', ['data_type_id', 'date', 'value1', 'value2', 'value3'])

        # Adding model 'TextToIntMap'
        db.create_table('stats_texttointmap', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dimension', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Dimension'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('text_local_cs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text_local_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('stats', ['TextToIntMap'])

        # Adding unique constraint on 'TextToIntMap', fields ['dimension', 'text']
        db.create_unique('stats_texttointmap', ['dimension_id', 'text'])


    def backwards(self, orm):
        # Removing unique constraint on 'TextToIntMap', fields ['dimension', 'text']
        db.delete_unique('stats_texttointmap', ['dimension_id', 'text'])

        # Removing unique constraint on 'DataPoint', fields ['data_type', 'date', 'value1', 'value2', 'value3']
        db.delete_unique('stats_datapoint', ['data_type_id', 'date', 'value1', 'value2', 'value3'])

        # Removing unique constraint on 'DimensionToDataType', fields ['data_type', 'dimension', 'position']
        db.delete_unique('stats_dimensiontodatatype', ['data_type_id', 'dimension_id', 'position'])

        # Removing unique constraint on 'StatGroupToStatistics', fields ['stat_group', 'statistics', 'position']
        db.delete_unique('stats_statgrouptostatistics', ['stat_group_id', 'statistics_id', 'position'])

        # Deleting model 'StatGroup'
        db.delete_table('stats_statgroup')

        # Deleting model 'StatGroupToStatistics'
        db.delete_table('stats_statgrouptostatistics')

        # Deleting model 'Statistics'
        db.delete_table('stats_statistics')

        # Deleting model 'DataType'
        db.delete_table('stats_datatype')

        # Deleting model 'Dimension'
        db.delete_table('stats_dimension')

        # Deleting model 'DimensionToDataType'
        db.delete_table('stats_dimensiontodatatype')

        # Deleting model 'DataPoint'
        db.delete_table('stats_datapoint')

        # Deleting model 'TextToIntMap'
        db.delete_table('stats_texttointmap')


    models = {
        'stats.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'stats.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': "orm['stats.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Dimension']", 'through': "orm['stats.DimensionToDataType']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'stats.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Statistics']", 'through': "orm['stats.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        'stats.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Statistics']"})
        },
        'stats.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        'stats.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['stats']