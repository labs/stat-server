# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ChartReference'
        db.delete_table('stats_chartreference')

        # Adding model 'ChartParamSet'
        db.create_table('stats_chartparamset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('ref_date', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('date_range', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('time_processing', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('time_sampling', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('data_scaling', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('include_other', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('series_limit', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('one_series', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal('stats', ['ChartParamSet'])

        # Adding model 'DashboardChart'
        db.create_table('stats_dashboardchart', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Statistics'])),
            ('position', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('params', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['stats.ChartParamSet'], unique=True)),
        ))
        db.send_create_signal('stats', ['DashboardChart'])


    def backwards(self, orm):
        # Adding model 'ChartReference'
        db.create_table('stats_chartreference', (
            ('include_other', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('data_scaling', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date_range', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(max_length=50, unique=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('time_processing', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('one_series', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.Statistics'])),
            ('time_sampling', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('ref_date', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('series_limit', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('stats', ['ChartReference'])

        # Deleting model 'ChartParamSet'
        db.delete_table('stats_chartparamset')

        # Deleting model 'DashboardChart'
        db.delete_table('stats_dashboardchart')


    models = {
        'stats.chartparamset': {
            'Meta': {'object_name': 'ChartParamSet'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date_range': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_other': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_series': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'ref_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'series_limit': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_processing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_sampling': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'})
        },
        'stats.dashboardchart': {
            'Meta': {'object_name': 'DashboardChart'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'params': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['stats.ChartParamSet']", 'unique': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Statistics']"})
        },
        'stats.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'stats.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': "orm['stats.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Dimension']", 'through': "orm['stats.DimensionToDataType']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'recurrent': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'stats.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partitioning': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Statistics']", 'through': "orm['stats.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        'stats.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Statistics']"})
        },
        'stats.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'stat_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'stat_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'stats.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'custom_color': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'custom_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['stats']