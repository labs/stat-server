# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DataPointAggreg'
        db.create_table('stats_datapointaggreg', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stats.DataType'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('time_period', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('amount', self.gf('django.db.models.fields.BigIntegerField')()),
            ('value1', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value2', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value3', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('amount_min', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_max', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_avg', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_first', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_last', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_sum', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_median', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_middle', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
        ))
        db.send_create_signal('stats', ['DataPointAggreg'])

        # Adding unique constraint on 'DataPointAggreg', fields ['data_type', 'date', 'time_period', 'value1', 'value2', 'value3']
        db.create_unique('stats_datapointaggreg', ['data_type_id', 'date', 'time_period', 'value1', 'value2', 'value3'])


    def backwards(self, orm):
        # Removing unique constraint on 'DataPointAggreg', fields ['data_type', 'date', 'time_period', 'value1', 'value2', 'value3']
        db.delete_unique('stats_datapointaggreg', ['data_type_id', 'date', 'time_period', 'value1', 'value2', 'value3'])

        # Deleting model 'DataPointAggreg'
        db.delete_table('stats_datapointaggreg')


    models = {
        'stats.chartparamset': {
            'Meta': {'object_name': 'ChartParamSet'},
            'client_data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date_range': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_other': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_series': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'ref_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'series_limit': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slicing_dim': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'time_processing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_sampling': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'})
        },
        'stats.dashboardchart': {
            'Meta': {'object_name': 'DashboardChart'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'params': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['stats.ChartParamSet']", 'unique': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Statistics']"})
        },
        'stats.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'stats.datapointaggreg': {
            'Meta': {'unique_together': "(('data_type', 'date', 'time_period', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPointAggreg'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'amount_avg': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_first': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_last': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_max': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_median': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_middle': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_min': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_sum': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time_period': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'stats.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': "orm['stats.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Dimension']", 'through': "orm['stats.DimensionToDataType']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'recurrent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'summable_periods': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'stats.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partitioning': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'stats.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['stats.Statistics']", 'through': "orm['stats.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        'stats.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Statistics']"})
        },
        'stats.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.DataType']", 'null': 'True', 'blank': 'True'}),
            'default_params': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.ChartParamSet']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'stat_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'stat_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'stats.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'custom_color': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'custom_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stats.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['stats']