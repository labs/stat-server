# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Report'
        db.create_table('unifiedstatistics_report', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('report_file_cs', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('report_file_en', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['Report'])

        # Adding model 'DashboardChart'
        db.create_table('unifiedstatistics_dashboardchart', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Statistics'])),
            ('position', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('params', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['unifiedstatistics.ChartParamSet'], unique=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['DashboardChart'])

        # Adding model 'ChartParamSet'
        db.create_table('unifiedstatistics_chartparamset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('ref_date', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('date_range', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('time_processing', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('time_sampling', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('data_scaling', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('client_data_scaling', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('include_other', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('series_limit', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('one_series', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('slicing_dim', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('slicing_value', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('display_as', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['ChartParamSet'])

        # Adding model 'StatGroup'
        db.create_table('unifiedstatistics_statgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('info_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('info_en', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['StatGroup'])

        # Adding model 'ReportGroup'
        db.create_table('unifiedstatistics_reportgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('info', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['ReportGroup'])

        # Adding model 'ReportGroupToReport'
        db.create_table('unifiedstatistics_reportgrouptoreport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('report_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.ReportGroup'])),
            ('report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Report'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['ReportGroupToReport'])

        # Adding unique constraint on 'ReportGroupToReport', fields ['report_group', 'report', 'position']
        db.create_unique('unifiedstatistics_reportgrouptoreport', ['report_group_id', 'report_id', 'position'])

        # Adding model 'StatGroupToStatistics'
        db.create_table('unifiedstatistics_statgrouptostatistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stat_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.StatGroup'])),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Statistics'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['StatGroupToStatistics'])

        # Adding unique constraint on 'StatGroupToStatistics', fields ['stat_group', 'statistics', 'position']
        db.create_unique('unifiedstatistics_statgrouptostatistics', ['stat_group_id', 'statistics_id', 'position'])

        # Adding model 'ReportSection'
        db.create_table('unifiedstatistics_reportsection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Report'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['ReportSection'])

        # Adding unique constraint on 'ReportSection', fields ['report', 'position']
        db.create_unique('unifiedstatistics_reportsection', ['report_id', 'position'])

        # Adding model 'ReportToStatistics'
        db.create_table('unifiedstatistics_reporttostatistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('report', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Report'])),
            ('report_section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.ReportSection'], null=True)),
            ('statistics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Statistics'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('params', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.ChartParamSet'], null=True)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('info_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('info_en', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('highlight_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('highlight_en', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['ReportToStatistics'])

        # Adding unique constraint on 'ReportToStatistics', fields ['report', 'position']
        db.create_unique('unifiedstatistics_reporttostatistics', ['report_id', 'position'])

        # Adding model 'Statistics'
        db.create_table('unifiedstatistics_statistics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.DataType'], null=True, blank=True)),
            ('stat_file_cs', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('stat_file_en', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('info_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('info_en', self.gf('django.db.models.fields.TextField')()),
            ('methodology_cs', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('methodology_en', self.gf('django.db.models.fields.TextField')()),
            ('default_params', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.ChartParamSet'], null=True, blank=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['Statistics'])

        # Adding model 'DataType'
        db.create_table('unifiedstatistics_datatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('amount_dim', self.gf('django.db.models.fields.related.ForeignKey')(related_name='amounttodatatype_set', to=orm['unifiedstatistics.Dimension'])),
            ('recurrent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('summable_periods', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['DataType'])

        # Adding model 'Dimension'
        db.create_table('unifiedstatistics_dimension', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code_name', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('name_cs', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('type', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('unifiedstatistics', ['Dimension'])

        # Adding model 'DimensionToDataType'
        db.create_table('unifiedstatistics_dimensiontodatatype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.DataType'])),
            ('dimension', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Dimension'])),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('partitioning', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['DimensionToDataType'])

        # Adding unique constraint on 'DimensionToDataType', fields ['data_type', 'dimension', 'position']
        db.create_unique('unifiedstatistics_dimensiontodatatype', ['data_type_id', 'dimension_id', 'position'])

        # Adding model 'DataPoint'
        db.create_table('unifiedstatistics_datapoint', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.DataType'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('amount', self.gf('django.db.models.fields.BigIntegerField')()),
            ('value1', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value2', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value3', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['DataPoint'])

        # Adding unique constraint on 'DataPoint', fields ['data_type', 'date', 'value1', 'value2', 'value3']
        db.create_unique('unifiedstatistics_datapoint', ['data_type_id', 'date', 'value1', 'value2', 'value3'])

        # Adding model 'DataPointAggreg'
        db.create_table('unifiedstatistics_datapointaggreg', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.DataType'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('time_period', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('amount', self.gf('django.db.models.fields.BigIntegerField')()),
            ('value1', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value2', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('value3', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('amount_min', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_max', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_avg', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_first', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_last', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_sum', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_median', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
            ('amount_middle', self.gf('django.db.models.fields.BigIntegerField')(null=True)),
        ))
        db.send_create_signal('unifiedstatistics', ['DataPointAggreg'])

        # Adding unique constraint on 'DataPointAggreg', fields ['data_type', 'date', 'time_period', 'value1', 'value2', 'value3']
        db.create_unique('unifiedstatistics_datapointaggreg', ['data_type_id', 'date', 'time_period', 'value1', 'value2', 'value3'])

        # Adding model 'TextToIntMap'
        db.create_table('unifiedstatistics_texttointmap', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dimension', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['unifiedstatistics.Dimension'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('text_local_cs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('text_local_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('custom_color', self.gf('django.db.models.fields.CharField')(default='', max_length=16)),
            ('custom_order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('unifiedstatistics', ['TextToIntMap'])

        # Adding unique constraint on 'TextToIntMap', fields ['dimension', 'text']
        db.create_unique('unifiedstatistics_texttointmap', ['dimension_id', 'text'])


    def backwards(self, orm):
        # Removing unique constraint on 'TextToIntMap', fields ['dimension', 'text']
        db.delete_unique('unifiedstatistics_texttointmap', ['dimension_id', 'text'])

        # Removing unique constraint on 'DataPointAggreg', fields ['data_type', 'date', 'time_period', 'value1', 'value2', 'value3']
        db.delete_unique('unifiedstatistics_datapointaggreg', ['data_type_id', 'date', 'time_period', 'value1', 'value2', 'value3'])

        # Removing unique constraint on 'DataPoint', fields ['data_type', 'date', 'value1', 'value2', 'value3']
        db.delete_unique('unifiedstatistics_datapoint', ['data_type_id', 'date', 'value1', 'value2', 'value3'])

        # Removing unique constraint on 'DimensionToDataType', fields ['data_type', 'dimension', 'position']
        db.delete_unique('unifiedstatistics_dimensiontodatatype', ['data_type_id', 'dimension_id', 'position'])

        # Removing unique constraint on 'ReportToStatistics', fields ['report', 'position']
        db.delete_unique('unifiedstatistics_reporttostatistics', ['report_id', 'position'])

        # Removing unique constraint on 'ReportSection', fields ['report', 'position']
        db.delete_unique('unifiedstatistics_reportsection', ['report_id', 'position'])

        # Removing unique constraint on 'StatGroupToStatistics', fields ['stat_group', 'statistics', 'position']
        db.delete_unique('unifiedstatistics_statgrouptostatistics', ['stat_group_id', 'statistics_id', 'position'])

        # Removing unique constraint on 'ReportGroupToReport', fields ['report_group', 'report', 'position']
        db.delete_unique('unifiedstatistics_reportgrouptoreport', ['report_group_id', 'report_id', 'position'])

        # Deleting model 'Report'
        db.delete_table('unifiedstatistics_report')

        # Deleting model 'DashboardChart'
        db.delete_table('unifiedstatistics_dashboardchart')

        # Deleting model 'ChartParamSet'
        db.delete_table('unifiedstatistics_chartparamset')

        # Deleting model 'StatGroup'
        db.delete_table('unifiedstatistics_statgroup')

        # Deleting model 'ReportGroup'
        db.delete_table('unifiedstatistics_reportgroup')

        # Deleting model 'ReportGroupToReport'
        db.delete_table('unifiedstatistics_reportgrouptoreport')

        # Deleting model 'StatGroupToStatistics'
        db.delete_table('unifiedstatistics_statgrouptostatistics')

        # Deleting model 'ReportSection'
        db.delete_table('unifiedstatistics_reportsection')

        # Deleting model 'ReportToStatistics'
        db.delete_table('unifiedstatistics_reporttostatistics')

        # Deleting model 'Statistics'
        db.delete_table('unifiedstatistics_statistics')

        # Deleting model 'DataType'
        db.delete_table('unifiedstatistics_datatype')

        # Deleting model 'Dimension'
        db.delete_table('unifiedstatistics_dimension')

        # Deleting model 'DimensionToDataType'
        db.delete_table('unifiedstatistics_dimensiontodatatype')

        # Deleting model 'DataPoint'
        db.delete_table('unifiedstatistics_datapoint')

        # Deleting model 'DataPointAggreg'
        db.delete_table('unifiedstatistics_datapointaggreg')

        # Deleting model 'TextToIntMap'
        db.delete_table('unifiedstatistics_texttointmap')


    models = {
        'unifiedstatistics.chartparamset': {
            'Meta': {'object_name': 'ChartParamSet'},
            'client_data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date_range': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'display_as': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_other': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_series': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'ref_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'series_limit': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slicing_dim': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'time_processing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_sampling': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'})
        },
        'unifiedstatistics.dashboardchart': {
            'Meta': {'object_name': 'DashboardChart'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'params': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'unique': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'unifiedstatistics.datapointaggreg': {
            'Meta': {'unique_together': "(('data_type', 'date', 'time_period', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPointAggreg'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'amount_avg': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_first': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_last': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_max': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_median': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_middle': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_min': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_sum': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time_period': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'unifiedstatistics.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': "orm['unifiedstatistics.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Dimension']", 'through': "orm['unifiedstatistics.DimensionToDataType']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'recurrent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'summable_periods': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'unifiedstatistics.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'unifiedstatistics.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partitioning': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'unifiedstatistics.report': {
            'Meta': {'object_name': 'Report'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'report_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'report_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'unifiedstatistics.reportgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'ReportGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Report']", 'through': "orm['unifiedstatistics.ReportGroupToReport']", 'symmetrical': 'False'})
        },
        'unifiedstatistics.reportgrouptoreport': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report_group', 'report', 'position'),)", 'object_name': 'ReportGroupToReport'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"}),
            'report_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ReportGroup']"})
        },
        'unifiedstatistics.reportsection': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"})
        },
        'unifiedstatistics.reporttostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportToStatistics'},
            'highlight_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'highlight_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'params': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"}),
            'report_section': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ReportSection']", 'null': 'True'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Statistics']", 'through': "orm['unifiedstatistics.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        'unifiedstatistics.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']", 'null': 'True', 'blank': 'True'}),
            'default_params': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'stat_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'stat_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'unifiedstatistics.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'custom_color': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'custom_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['unifiedstatistics']