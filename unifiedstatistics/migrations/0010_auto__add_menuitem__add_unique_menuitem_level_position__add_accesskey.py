# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MenuItem'
        db.create_table(u'unifiedstatistics_menuitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label_cs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('label_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('href_cs', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('href_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'unifiedstatistics', ['MenuItem'])

        # Adding unique constraint on 'MenuItem', fields ['level', 'position']
        db.create_unique(u'unifiedstatistics_menuitem', ['level', 'position'])

        # Adding model 'AccessKey'
        db.create_table(u'unifiedstatistics_accesskey', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('secret', self.gf('django.db.models.fields.CharField')(default=u'pIYiVt8dTbT6hrsy', unique=True, max_length=16, blank=True)),
            ('valid_until', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('remaining_uses', self.gf('django.db.models.fields.PositiveIntegerField')(default=1, null=True, blank=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'])),
        ))
        db.send_create_signal(u'unifiedstatistics', ['AccessKey'])


    def backwards(self, orm):
        # Removing unique constraint on 'MenuItem', fields ['level', 'position']
        db.delete_unique(u'unifiedstatistics_menuitem', ['level', 'position'])

        # Deleting model 'MenuItem'
        db.delete_table(u'unifiedstatistics_menuitem')

        # Deleting model 'AccessKey'
        db.delete_table(u'unifiedstatistics_accesskey')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'unifiedstatistics.accesskey': {
            'Meta': {'object_name': 'AccessKey'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'remaining_uses': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'secret': ('django.db.models.fields.CharField', [], {'default': "u'izJtxDRMxv3NDJeU'", 'unique': 'True', 'max_length': '16', 'blank': 'True'}),
            'valid_until': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'unifiedstatistics.chartparamset': {
            'Meta': {'object_name': 'ChartParamSet'},
            'client_data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date_range': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'display_as': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_other': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_series': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'ref_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'relative_scale_base': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'series_limit': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slicing_dim': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_dim_2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_dim_3': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'slicing_value_2': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'slicing_value_3': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'time_processing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_sampling': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'})
        },
        u'unifiedstatistics.dashboardchart': {
            'Meta': {'object_name': 'DashboardChart'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'params': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['unifiedstatistics.ChartParamSet']", 'unique': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Statistics']"})
        },
        u'unifiedstatistics.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3', 'value4'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value4': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        u'unifiedstatistics.datapointaggreg': {
            'Meta': {'unique_together': "(('data_type', 'date', 'time_period', 'value1', 'value2', 'value3', 'value4'),)", 'object_name': 'DataPointAggreg'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'amount_avg': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_first': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_last': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_max': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_median': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_middle': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_min': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_sum': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time_period': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value4': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        u'unifiedstatistics.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': u"orm['unifiedstatistics.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['unifiedstatistics.Dimension']", 'through': u"orm['unifiedstatistics.DimensionToDataType']", 'symmetrical': 'False'}),
            'fault_tolerance': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'recurrent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'summable_periods': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'update_interval': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        u'unifiedstatistics.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'unifiedstatistics.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Dimension']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partitioning': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'sorting': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': 'None', 'null': 'True'})
        },
        u'unifiedstatistics.menuitem': {
            'Meta': {'ordering': "('level', 'position')", 'unique_together': "(('level', 'position'),)", 'object_name': 'MenuItem'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'href_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'href_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'label_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'unifiedstatistics.report': {
            'Meta': {'object_name': 'Report'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'report_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'report_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'unifiedstatistics.reportgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'ReportGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['unifiedstatistics.Report']", 'through': u"orm['unifiedstatistics.ReportGroupToReport']", 'symmetrical': 'False'})
        },
        u'unifiedstatistics.reportgrouptoreport': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report_group', 'report', 'position'),)", 'object_name': 'ReportGroupToReport'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Report']"}),
            'report_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.ReportGroup']"})
        },
        u'unifiedstatistics.reportsection': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportSection'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Report']"})
        },
        u'unifiedstatistics.reporttostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportToStatistics'},
            'highlight_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'highlight_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'params': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.ChartParamSet']", 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Report']"}),
            'report_section': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.ReportSection']", 'null': 'True'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Statistics']"})
        },
        u'unifiedstatistics.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['unifiedstatistics.Statistics']", 'through': u"orm['unifiedstatistics.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        u'unifiedstatistics.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Statistics']"})
        },
        u'unifiedstatistics.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.DataType']", 'null': 'True', 'blank': 'True'}),
            'default_params': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.ChartParamSet']", 'null': 'True', 'blank': 'True'}),
            'enforced_params': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['unifiedstatistics.ChartParamSet']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'stat_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'stat_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'unifiedstatistics.statisticsgroupobjectpermission': {
            'Meta': {'unique_together': "([u'group', u'permission', u'content_object'],)", 'object_name': 'StatisticsGroupObjectPermission'},
            'content_object': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Statistics']"}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Permission']"})
        },
        u'unifiedstatistics.statisticsuserobjectpermission': {
            'Meta': {'unique_together': "([u'user', u'permission', u'content_object'],)", 'object_name': 'StatisticsUserObjectPermission'},
            'content_object': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Statistics']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'permission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Permission']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'unifiedstatistics.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'custom_color': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'custom_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['unifiedstatistics.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['unifiedstatistics']