# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'DataType.update_interval'
        db.add_column('unifiedstatistics_datatype', 'update_interval',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'DataType.fault_tolerance'
        db.add_column('unifiedstatistics_datatype', 'fault_tolerance',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'DataType.update_interval'
        db.delete_column('unifiedstatistics_datatype', 'update_interval')

        # Deleting field 'DataType.fault_tolerance'
        db.delete_column('unifiedstatistics_datatype', 'fault_tolerance')


    models = {
        'unifiedstatistics.chartparamset': {
            'Meta': {'object_name': 'ChartParamSet'},
            'client_data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_scaling': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date_range': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'display_as': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'include_other': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_series': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'ref_date': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'series_limit': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slicing_dim': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'slicing_value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'time_processing': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time_sampling': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'})
        },
        'unifiedstatistics.dashboardchart': {
            'Meta': {'object_name': 'DashboardChart'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'params': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'unique': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.datapoint': {
            'Meta': {'unique_together': "(('data_type', 'date', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPoint'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'unifiedstatistics.datapointaggreg': {
            'Meta': {'unique_together': "(('data_type', 'date', 'time_period', 'value1', 'value2', 'value3'),)", 'object_name': 'DataPointAggreg'},
            'amount': ('django.db.models.fields.BigIntegerField', [], {}),
            'amount_avg': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_first': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_last': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_max': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_median': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_middle': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_min': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'amount_sum': ('django.db.models.fields.BigIntegerField', [], {'null': 'True'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'time_period': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'value1': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value2': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'value3': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'unifiedstatistics.datatype': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'DataType'},
            'amount_dim': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'amounttodatatype_set'", 'to': "orm['unifiedstatistics.Dimension']"}),
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'dimensions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Dimension']", 'through': "orm['unifiedstatistics.DimensionToDataType']", 'symmetrical': 'False'}),
            'fault_tolerance': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'recurrent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'summable_periods': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'update_interval': ('django.db.models.fields.IntegerField', [], {'null': 'True'})
        },
        'unifiedstatistics.dimension': {
            'Meta': {'ordering': "('code_name',)", 'object_name': 'Dimension'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'unifiedstatistics.dimensiontodatatype': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('data_type', 'dimension', 'position'),)", 'object_name': 'DimensionToDataType'},
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']"}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'partitioning': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'unifiedstatistics.report': {
            'Meta': {'object_name': 'Report'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'report_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'report_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'unifiedstatistics.reportgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'ReportGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Report']", 'through': "orm['unifiedstatistics.ReportGroupToReport']", 'symmetrical': 'False'})
        },
        'unifiedstatistics.reportgrouptoreport': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report_group', 'report', 'position'),)", 'object_name': 'ReportGroupToReport'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"}),
            'report_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ReportGroup']"})
        },
        'unifiedstatistics.reportsection': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"})
        },
        'unifiedstatistics.reporttostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('report', 'position'),)", 'object_name': 'ReportToStatistics'},
            'highlight_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'highlight_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'params': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'null': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'report': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Report']"}),
            'report_section': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ReportSection']", 'null': 'True'}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.statgroup': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StatGroup'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'statistics': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['unifiedstatistics.Statistics']", 'through': "orm['unifiedstatistics.StatGroupToStatistics']", 'symmetrical': 'False'})
        },
        'unifiedstatistics.statgrouptostatistics': {
            'Meta': {'ordering': "('position',)", 'unique_together': "(('stat_group', 'statistics', 'position'),)", 'object_name': 'StatGroupToStatistics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'stat_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.StatGroup']"}),
            'statistics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Statistics']"})
        },
        'unifiedstatistics.statistics': {
            'Meta': {'object_name': 'Statistics'},
            'code_name': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'data_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.DataType']", 'null': 'True', 'blank': 'True'}),
            'default_params': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.ChartParamSet']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'info_en': ('django.db.models.fields.TextField', [], {}),
            'methodology_cs': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'methodology_en': ('django.db.models.fields.TextField', [], {}),
            'name_cs': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'stat_file_cs': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'stat_file_en': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'unifiedstatistics.texttointmap': {
            'Meta': {'unique_together': "(('dimension', 'text'),)", 'object_name': 'TextToIntMap'},
            'custom_color': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '16'}),
            'custom_order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dimension': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['unifiedstatistics.Dimension']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'text_local_cs': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'text_local_en': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['unifiedstatistics']