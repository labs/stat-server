# Django imports
from django.middleware.locale import LocaleMiddleware
from django.utils.translation import get_language
from django.utils import translation
from django.utils.cache import patch_vary_headers

# local imports
from unifiedstatistics.common import get_language_from_request

class StatsLocaleMiddleware(LocaleMiddleware):
    """ Rewritten Django's LocaleMiddleware which uses our rewritten
        function get_language_from_request which don't look for language
        into session nor browse accept-language settings, instead it's using
        only Cookies and default LANGUAGE_CODE from settings.
    """
    def process_request(self, request):
        language = get_language_from_request(request) # our rewritten function
        translation.activate(language)
        request.LANGUAGE_CODE = get_language()

    def process_response(self, request, response):
        patch_vary_headers(response, ('Cookie',))
        return super(StatsLocaleMiddleware, self).process_response(request, response)

