# encoding: utf-8

# built-in modules
import datetime
import json
import logging

# Django imports
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.cache import cache
from django.utils.translation import get_language, check_for_language
from django.conf import settings
from django.db import connection
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied
from django_mojeid.signals import trigger_error
from django.dispatch import receiver
from django.contrib import messages

# third party imports
from guardian.shortcuts import get_objects_for_user, get_objects_for_group

# local imports
from unifiedstatistics import models
from data_mining import DataRetreivalException
from data_request import DataRequest
from common import create_cache_key, querydict_to_dict, get_public_group

logger = logging.getLogger("unifiedstatistics.views")


def dashboard(request):
    chart_refs = models.DashboardChart.objects.all().order_by("position")
    data = dict(chart_refs=chart_refs)
    return render(request, 'dashboard.html', data)


def all_statistics(request):
    stat_groups = _get_all_stat_groups(request)
    data = dict(stat_groups=stat_groups)
    return render(request, 'all_statistics.html', data)


def reports(request):
    code_name = _get_last_report_code_name()
    if code_name:
        return redirect('report-page', report_code_name=code_name)
    else:
        raise Http404


def statistics_page(request, stat_code_name):
    try:
        stat = models.Statistics.objects.select_related('default_params')\
                                                .get(code_name=stat_code_name)
        # Statistics must be public or user must be authenticated and has permission to view statistics - otherwise raise HTTP 403
        if (not stat.get_public()) and (not request.user.is_staff) and ((not request.user.is_authenticated()) or (not request.user.has_perm('view_statistics', stat))):
            raise PermissionDenied
    except models.Statistics.DoesNotExist:
        raise Http404
    stat_groups = _get_all_stat_groups(request)
    # let's see if we have an online or offline statistics
    if stat.data_type is None:
        if stat.stat_file is None:
            # there must be a file if there is no data_type, otherwise this
            # stat is not active
            raise Http404
        data = dict(statistics=stat, stat_groups=stat_groups)
        return render(request, "offline_statistics_page.html", data)
    else:
        drq = _get_data_request(request.GET, stat.code_name, user=request.user)
        data = dict(data_request=drq, stat_groups=stat_groups)
        data.update(drq.available_dates)
        return render(request, "statistics_page.html", data)


def report_page(request, report_code_name):
    report = _get_report(report_code_name)
    if not isinstance(report, models.Report):
        raise Http404
    #try:
    #    report = models.Report.objects.get(code_name=report_code_name)
    #except models.Report.DoesNotExist:
    #    raise Http404
    report_groups = _get_all_report_groups()
    if report.is_offline():
        data = dict(report_groups=report_groups, report=report)
        return render(request, "offline_report_page.html", data)
    else:
        data = dict(report_groups=report_groups, report=report)
        return render(request, "report_page.html", data)


def set_language(request):
    """
    Redirect to a given url while setting the chosen language in the
    cookie. The url and the language code need to be specified in the
    GET parameters. Setting language to session was removed and only
    cookie is used.
    """
    lang_code = request.GET.get('language', None)
    next = request.GET.get('next', None)
    if not next:
        next = request.META.get('HTTP_REFERER', None)
    if not next:
        next = '/'
    response = HttpResponseRedirect(next)
    if lang_code and check_for_language(lang_code):
        set_language_cookie(request, response, lang_code)
    return response

def logout_view(request):
    return logout(request, next_page=reverse(all_statistics))


def login_view(request):
    data = dict(stat_groups=_get_all_stat_groups(request),
                redirect=reverse(set_access_view))
    return render(request, "login.html", data)


@receiver(trigger_error, dispatch_uid='trigger_error')
def redirect_to_login(**kwargs):
    request = kwargs['request']
    error = kwargs['error']
    messages.add_message(request, messages.INFO, error.text)
    return HttpResponseRedirect(reverse(login_view))


@login_required
def set_access_view(request):
    """
    Page with small form allowing to enter access key for private statistics.
    """
    data = dict(submitted=False)
    if request.method == 'POST':
        data['submitted'] = True
        secret = request.POST['key']
    elif request.method == 'GET' and 'key' in request.GET:
        data['submitted'] = True
        secret = request.GET['key']
    if data['submitted']:
        data['new_stats'] = []
        data['key_valid'] = False
        try:
            key = models.AccessKey.objects.get(secret=secret)
            group, added = key.apply_key(request.user)
            data['new_stats'] = map(lambda x: x.content_object.name,
                                    group.statisticsgroupobjectpermission_set.all())
            data['key_valid'] = True
            data['added'] = added
            if added:
                # delete cached menu, because it probably changed
                cache_key = create_cache_key("main", "stat_groups", request.user.id)
                cache.delete(cache_key)
        except (models.AccessKey.DoesNotExist, AttributeError):
            pass
    data['stat_groups'] = _get_all_stat_groups(request)
    return render(request, "access.html", data)


def animation_test(request):
    return render(request, "animation_test.html")


def error500(request):
    return render(request, "500.html")

## ---------- JSON FOLLOWS ----------------


def html_decorator(func):
    """wrap view inside html to enable optimizing of AJAX requests in django-debug-toolbar
    """
    def _decorated(*args, ** kwargs):
        response = func(*args, **kwargs)

        wrapped = ("<html><body>",
                   response.content,
                   "</body></html>")

        return HttpResponse(wrapped)

    return _decorated


def json_data(request, stat_code_name, **kwargs):
    params = request.GET
    cache_key = create_cache_key("json_data", stat_code_name, params, kwargs, request.user.id)
    ret = cache.get(cache_key)
    if not ret:
        logger.debug("cache miss for %s", cache_key)
        report_code_name = kwargs['report_code_name'] if 'report_code_name' in kwargs else None
        position = kwargs['position'] if 'position' in kwargs else None
        drq = _get_data_request(params, stat_code_name, report_code_name, position, user=request.user)
        ret = drq.get_json_data()
        cache.set(cache_key, ret)
    else:
        logger.debug("cache hit for %s", cache_key)
    return HttpResponse(json.dumps(ret, default=_json_encode),
                        content_type="application/json")


#@cache_page(60*10)
def dashchart_data(request, ref_code_name):
    lang = get_language()
    cache_key = create_cache_key("json_data_dash", ref_code_name, lang, request.user.id)
    ret = cache.get(cache_key)
    if not ret:
        logger.debug("cache miss for %s", cache_key)
        try:
            dash_chart = models.DashboardChart.objects.select_related().\
                                                    get(code_name=ref_code_name)
        except models.DashboardChart.DoesNotExist:
            raise Http404
        params = dash_chart.params.get_data_request_params()
        drq = _get_data_request(params, dash_chart.statistics, user=request.user)
        ret = drq.get_json_data()
        dashchart_title = dash_chart.name
        if not ret['has_time']:
            # append original title to the dashchart name when chart does not
            # has time dimension - otherwise it won't display the day it is
            # taken from
            dashchart_title += " (" + ret['chart_title'] + ")"
        ret['chart_title'] = dashchart_title
        cache.set(cache_key, ret)
    else:
        logger.debug("cache hit for %s", cache_key)
    response = HttpResponse(json.dumps(ret, default=_json_encode),
                            content_type="application/json")
    if hasattr(settings, "ACCESS_CONTROL_ALLOW_ORIGIN") and \
       settings.ACCESS_CONTROL_ALLOW_ORIGIN:
        response['Access-Control-Allow-Origin'] = \
                                          settings.ACCESS_CONTROL_ALLOW_ORIGIN
    return response 


def csv_data(request, stat_code_name, **kwargs):
    report_code_name = kwargs['report_code_name'] if 'report_code_name' in kwargs else None
    drq = _get_data_request(request.GET, stat_code_name, report_code_name, user=request.user)
    # fetch the data from the db
    try:
        package = drq.get_data()
    except DataRetreivalException:
        raise Http404
    return_data = package.data_table.ToCsv(separator="|",
                                           columns_order=package.column_order,
                                           order_by=package.order_data_by)
    res = HttpResponse(return_data, content_type="text/csv")
    res["Content-Disposition"] = "attachment; filename=%s.csv" % stat_code_name
    return res


def json_slices(request, stat_code_name, **kwargs):
    report_code_name = kwargs['report_code_name'] if 'report_code_name' in kwargs else None
    drq = _get_data_request(request.GET, stat_code_name, report_code_name, user=request.user)
    slicing = drq.get_slicing()
    non_slicing = map(lambda x: (x.code_name, x.name), drq.get_non_slicing_dims())
    ret = {"ok": True, "data": slicing, "non_slicing": non_slicing}
    return HttpResponse(json.dumps(ret), content_type="application/json")


def json_series(request, stat_code_name, **kwargs):
    report_code_name = kwargs['report_code_name'] if 'report_code_name' in kwargs else None
    drq = _get_data_request(request.GET, stat_code_name, report_code_name, user=request.user)
    series = []
    for key, value, sel in drq.get_possible_series():
        series.append((key, value, sel))
    data = dict(series=series, summable=drq.summable_series,
                scales=drq.get_valid_scaling_options())
    ret = {"ok": True, "data": data}
    return HttpResponse(json.dumps(ret), content_type="application/json")

# HELPER FUNCTIONS


def _json_encode(value):
    if isinstance(value, datetime.date):
        return unicode(value)
    elif isinstance(value, datetime.datetime):
        return unicode(value)
    raise TypeError("Cannot serialize %s" % value)


def get_and_check_cookie_domain(request, cookie_domain):
    """
    Set cookie_domain to None if requested domain does not end with it
    (you can't set cookie for other domain than your (sub)domain)
    """
    if cookie_domain:
        requested_domain = request.get_host().split(':')[0] # get domain(host) without port
        if cookie_domain[0] == '.':
            cookie_domain = cookie_domain[1:] # remove leading dot so we can set it exactly for the that domain without leading dot
        if not requested_domain.endswith(cookie_domain):
            cookie_domain = None
    return cookie_domain


def set_language_cookie(request, response, lang_code):
    response.set_cookie(
        settings.LANGUAGE_COOKIE_NAME,
        lang_code,
        max_age=settings.LANGUAGE_COOKIE_MAX_AGE,
        domain=get_and_check_cookie_domain(request, settings.LANGUAGE_COOKIE_DOMAIN)
    )

# caching stuff


def _get_all_stat_groups(request):
    cache_key = create_cache_key("main", "stat_groups", request.user.id)
    data = cache.get(cache_key)
    if not data:
        data = []
        cursor = connection.cursor()
        # optimizing distinct query on indexed column, which can be otherwise written as:
        # non_empty_data_type_ids = models.DataPoint.objects.distinct().values_list('data_type', flat=True)
        # see http://zogovic.com/post/44856908222/optimizing-postgresql-query-for-distinct-values
        cursor.execute("WITH RECURSIVE t(n) AS ("
                       "    SELECT MIN(data_type_id) FROM unifiedstatistics_datapoint"
                       "  UNION"
                       "    SELECT (SELECT data_type_id FROM unifiedstatistics_datapoint WHERE data_type_id > n ORDER BY data_type_id LIMIT 1)"
                       "    FROM t WHERE n IS NOT NULL"
                       ")"
                       "SELECT n FROM t")
        res = cursor.fetchall()
        # flattening query result
        non_empty_data_type_ids = filter(lambda x: x != None, [item for sublist in res for item in sublist])
        
        for stat_group in models.StatGroup.objects.prefetch_related('statistics__data_type').all():
            non_empty_stats = filter(lambda stat: (stat.data_type is None) or (stat.data_type.id in non_empty_data_type_ids),
                                     stat_group.get_statistics())
            if request.user.is_staff:
                allowed_stats = non_empty_stats
            else:
                authorized_stats = set(get_objects_for_group(get_public_group(), 'view_statistics', models.Statistics))
                if request.user.is_authenticated():
                    authorized_stats.update(get_objects_for_user(request.user, 'view_statistics', models.Statistics))
                allowed_stats = filter(lambda stat: stat in authorized_stats, non_empty_stats)
            if allowed_stats:
                data.append(dict(group=stat_group,
                                 stats=allowed_stats))
        cache.set(cache_key, data)
    return data


def _get_all_report_groups():
    cache_key = create_cache_key("main", "report_groups")
    data = cache.get(cache_key)
    if not data:
        data = []
        for report_group in models.ReportGroup.objects.select_related().all():
            data.append(dict(group=report_group,
                             reports=report_group.reports.order_by('reportgrouptoreport__position')))
        cache.set(cache_key, data)
    return data


def _get_data_request(params, stat_code_name, report=None, position=None, user=None):
    params = querydict_to_dict(params)
    if "lang" not in params:
        params["lang"] = get_language()
    cache_key = create_cache_key("data", stat_code_name, report, position, params, user.id)
    logger.debug("cache search for %s", cache_key)
    drq = cache.get(cache_key)
    if not drq:
        logger.debug("cache miss for %s", cache_key)
        drq = DataRequest(params, stat_code_name, report, position, user)
        cache.set(cache_key, drq)
    else:
        logger.debug("cache hit for %s", cache_key)
    return drq


def _get_last_report_code_name():
    cache_key = create_cache_key("main", "last_report_code_name")
    data = cache.get(cache_key)
    if not data:
        try:
            data = models.ReportGroup.objects.order_by('position')[0].reports.\
                order_by('reportgrouptoreport__position')[0].code_name
        except IndexError:
            data = None
        cache.set(cache_key, data)
    return data


def _get_report(code_name):
    cache_key = create_cache_key("main", "report", code_name)
    data = cache.get(cache_key)
    if not data:
        try:
            data = models.Report.objects.filter(code_name=code_name).\
                prefetch_related('reportsection_set__reporttostatistics_set__statistics__data_type').\
                prefetch_related('reporttostatistics_set__statistics__data_type').\
                get(code_name=code_name)
            cache.set(cache_key, data)
        except models.Report.DoesNotExist:
            raise Http404
    return data


def crash(request):
    return XXX
