# build-in modules
import os
import logging
import csv
from optparse import make_option

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db import transaction
from django.db.backends import BaseDatabaseWrapper
from django.db.backends.util import CursorWrapper

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.rewrite_remaps")

# this is here to prevent storage of run queries in connection.queries
# in DEBUG mode
if settings.DEBUG:
    BaseDatabaseWrapper.make_debug_cursor = lambda self, cursor:\
                                            CursorWrapper(cursor, self)

class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('-a', '--action',
            action='store_true',
            dest='action',
            default=False,
            help='This must be explicitly set in order for the change to '
                 'really take place data type to dump'),
        make_option('-i', '--inverse',
            action='store_true',
            dest='inverse',
            default=False,
            help='Inverse the remap - useful for undoing previous remaps.'),
        )
    
    def handle(self, *args, **options):
        self.really_update = options.get('action')
        self.inverse = options.get('inverse')
        if len(args) < 2:
            raise CommandError('Give me a dimension code_name and filename '
                               'with the remap')
        dim_name = args[0]
        infilename = args[1]
        try:
            dimension = models.Dimension.objects.get(code_name=dim_name)
        except models.Dimension.DoesNotExist:
            raise CommandError("Dimension with code_name '%s' does not exist" %\
                               dim_name)
        if not os.path.isfile(infilename):
            raise CommandError("File not found '%s'" % infilename) 
        remap = {}
        with file(infilename, 'r') as infile:
            reader = csv.reader(infile, delimiter="|")
            for i, row in enumerate(reader):
                if len(row) != 2:
                    raise ValueError("Each row in remap file must have exactly "
                                     "two values - line %d has %d" % \
                                     (i, len(row)))
                orig = row[0].decode('utf-8')
                new = row[1].decode('utf-8')
                if self.inverse:
                    new, orig = orig, new
                if orig in remap:
                    raise ValueError("Original value '%s' on line %d already "
                                     "remapped to '%s'!" % (remap[orig], i, new))
                remap[orig] = new
        self.do_rewrite(dimension, remap)
        if not self.really_update:
            logger.warn("No change was performed to the database - use the -a "
                        "switch to really do the rewrite.")
        
        
    @transaction.commit_on_success()    
    def do_rewrite(self, dimension, remap):
        ttis = dimension.texttointmap_set.all()
        used_remaps = set()
        for tti in ttis:
            if tti.text in remap:
                logger.debug("Remapping %s => %s for id %d", tti.text,
                             remap[tti.text], tti.id)
                used_remaps.add(tti.text)
                if self.really_update:
                    tti.text = remap[tti.text]
                    tti.save()
            else:
                logger.warn("No remap for %s (id %d)", tti.text, tti.id)
        for orig in remap:
            if orig not in used_remaps:
                logger.warn("Unused remap %s => %s", orig, remap[orig])
