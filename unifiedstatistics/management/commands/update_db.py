# build-in modules
import datetime
import os
import logging
import time
import csv

# Django imports
from django.core.management.base import NoArgsCommand
from django.conf import settings
from django.db import transaction
from django.db.backends import BaseDatabaseWrapper
from django.db.backends.util import CursorWrapper

# local imports
from unifiedstatistics import models
from unifiedstatistics.aggregation import Aggregator 

# basic setup
logger = logging.getLogger("unifiedstatistics.update_db")

# this is here to prevent storage of run queries in connection.queries
# in DEBUG mode
if settings.DEBUG:
    BaseDatabaseWrapper.make_debug_cursor = lambda self, cursor:\
                                            CursorWrapper(cursor, self)

class Command(NoArgsCommand):
    def handle(self, **options):
        files = os.listdir(settings.INCOMING_DATA_DIR)
        files.sort()
        files.reverse()
        self._remaps = {}
        self._min_date = None
        self._max_date = None
        # read some data from the db in advance to speed up larger updates
        self._data_types = dict([(dt.code_name, dt)
                                 for dt in models.DataType.objects.all()])
        self._dt_to_dims = {}
        for dt_code, dt in self._data_types.iteritems():
            self._dt_to_dims[dt_code] = [d2dt.dimension 
                                for d2dt in dt.dimensiontodatatype_set.all()\
                                                        .order_by("position")]
        # main loop
        for fname in files:
            start_time = time.time()
            fullname = os.path.join(settings.INCOMING_DATA_DIR, fname)
            if not os.path.isfile(fullname):
                # skip directories and such
                continue
            logger.info("Processing file '%s'", fullname)
            basename, _ext = os.path.splitext(fname)
            f_split = basename.split("-")
            # check the number of parts
            if len(f_split) != 2:
                logger.warn("Incompatible file name '%s'", fname)
                continue
            f_date = f_split[0]
            f_type = f_split[1]

            data_type = self._data_types.get(f_type)
            if data_type is None:
                logger.error("Unknown datatype '%s' for file '%s'",
                             f_type, fullname)
                continue
            else:
                # parse date
                try:
                    year = int(f_date[:4])
                    month = int(f_date[4:6])
                    day = int(f_date[6:])
                except ValueError:
                    logger.error("Cannot parse date '%s' in filename '%s'",
                                 f_date, fullname)
                    continue
                else:
                    date = datetime.datetime(year, month, day, 0, 0)
                    with file(fullname, 'r') as data_file:
                        self._read_csv_data_file(data_type, date, data_file)
                    if not self._min_date or self._min_date > date:
                        self._min_date = date
                    if not self._max_date or self._max_date < date:
                        self._max_date = date

            # move file to 'done' directory
            done_dir = os.path.join(settings.INCOMING_DATA_DIR, "done")
            if not os.path.isdir(done_dir):
                try:
                    os.mkdir(done_dir)
                except Exception as exc:
                    logger.critical("Cannot create directory for processed "
                                    "files: '%s' (error: %s)", done_dir, exc)
                    break
            try:
                pass
                os.rename(fullname, os.path.join(done_dir, fname))
            except Exception as exc:
                logger.critical("Cannot move file '%s' to directory '%s' "
                                "(error: %s)", fullname, done_dir, exc)
                continue
            logger.debug("Done in %.2f s", time.time()-start_time)
            
        # update aggregations
        if self._min_date and self._max_date:
            logger.info("Creating aggregations")
            Aggregator.create_aggregs(time_period="1m", 
                                      start_date=self._min_date,
                                      end_date=self._max_date)


    @transaction.commit_on_success()
    def _read_csv_data_file(self, data_type, date, data_file):
        """create new DataPoint of given Datatype"""
        def store_data_point(amount, *values):
            new_vals = []
            for i, value in enumerate(values):
                dim = dim_objs[i]
                if dim.type == dim.TYPE_TEXT:
                    # we need to remap this
                    remap = self._get_remap(dim)
                    # do we have a stored remap?
                    new_val = remap.get(value)
                    if new_val is None:
                        new_map = models.TextToIntMap(dimension=dim,
                                                      text=value)
                        new_map.save()
                        new_val = new_map.id
                        remap[value] = new_val # update the remap
                else:
                    new_val = value
                new_vals.append(new_val)
            while len(new_vals) < 4:
                new_vals.append(None)
            if amount in ("", "-"):
                return # no amount - missing or 0 data
            amount = int(amount)
            kwargs = dict(data_type=data_type,
                          date=date,
                          value1=new_vals[0],
                          value2=new_vals[1],
                          value3=new_vals[2],
                          value4=new_vals[3])
            try:
                point = models.DataPoint.objects.get(**kwargs)
            except models.DataPoint.DoesNotExist:
                point = models.DataPoint(amount=amount, **kwargs)
                point.save()
            else:
                if point.amount != amount:
                    logger.info("Updating amount in existing value %s, %s",
                                 point.amount, amount)
                    point.amount = amount
                    point.save()
                else:
                    pass
                    #logger.debug("Skipping existing value")
                    

        # dimensions for given DataType
        dim_objs = self._dt_to_dims[data_type.code_name]
        reader = csv.reader(data_file, delimiter="|")
        # read data into a list to make them easier to parse
        records = [record for record in reader]
        if not records:
            logger.warn("Empty file")
            return
        dim_count = len(dim_objs)
        for record in records:
            if len(record) != dim_count+1:
                logger.error("Only %d value(s) for %dD data.", len(record),
                             dim_count+1)
                raise ValueError("Only %d value(s) for %dD data." % \
                                 (len(record), dim_count+1))
            amount = record[-1]
            if amount:
                # do not store empty values
                values = [unicode(val, "utf-8") for val in record[:-1]]
                store_data_point(record[-1], *values)
        return True

        
    def _get_remap(self, dim_obj):
        remap = self._remaps.get(dim_obj.code_name)
        if remap is None:
            remap = dim_obj.get_str_to_int_remap()
            self._remaps[dim_obj.code_name] = remap
        return remap
