# built-in modules
from optparse import make_option
from datetime import date

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db.backends import BaseDatabaseWrapper
from django.db.backends.util import CursorWrapper

# local imports
from unifiedstatistics.aggregation import Aggregator
from unifiedstatistics import models

# this is here to prevent storage of run queries in connection.queries
# in DEBUG mode
if settings.DEBUG:
    BaseDatabaseWrapper.make_debug_cursor = lambda self, cursor:\
                                            CursorWrapper(cursor, self)


class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('-s', '--start-date',
            action='store',
            dest='start_date',
            default=None,
            help='Start date for created aggregations'),
        make_option('-e', '--end-date',
            action='store',
            dest='end_date',
            default=None,
            help='End date for created aggregations'),                                     
        make_option('-t', '--data-type',
            action='store',
            dest='dt_name',
            default=None,
            help='Code name of data type for created aggregations'),                                     
        )
    
    def handle(self, **options):
        start_date = options.get('start_date')
        end_date = options.get('end_date')
        dt_name = options.get('dt_name')
        if start_date:
            year, month, day = map(int, start_date.split("-"))
            start_date = date(year=year, month=month, day=day)
        if end_date:
            year, month, day = map(int, end_date.split("-"))
            end_date = date(year=year, month=month, day=day)
        if dt_name:
            try:
                data_type = models.DataType.objects.get(code_name=dt_name)
            except models.DataType.DoesNotExist:
                raise CommandError("Data type with code_name '%s' does not exist" % dt_name)                
        else:
            data_type = None

        Aggregator.create_aggregs(time_period="1m", 
                                  start_date=start_date, end_date=end_date,
                                  data_type=data_type)
