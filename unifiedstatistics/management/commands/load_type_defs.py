# build-in modules
import os
import logging
import json
from optparse import make_option

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.load_type_defs")

class Command(BaseCommand):
    
    args = "JSON file"
    
    option_list = BaseCommand.option_list + (
        make_option('-f', '--force-update',
            action='store_true',
            dest='force_update',
            default=False,
            help='Force update on instances different from db'),
        )
    
    def handle(self, *args, **options):
        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        elif verbosity in ("2", "3"):
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        if not args:
            raise CommandError('Give me a JSON file to load from')
        force_update = options.get('force_update', False)
        for arg in args:
            if not os.path.isfile(arg):
                raise CommandError('File "%s" does not exist' % arg)
            try:
                with file(arg, 'r') as infile:
                    data = json.load(infile)
            except Exception as exc:
                raise CommandError('Cannot read file "%s" - error "%s"' % \
                                   (arg, exc))
            # dimensions go before data types
            dimensions = data.get('dimensions', {})
            for code_name, definition in dimensions.iteritems():
                self._create_dimension(code_name, definition,
                                       force_update=force_update)
            # data types
            data_types = data.get("data_types", {})
            for code_name, definition in data_types.iteritems():
                self._create_data_type(code_name, definition,
                                       force_update=force_update)
                
        
    @transaction.commit_on_success()
    def _create_dimension(self, code_name, definition, force_update=False):
        attrs = {}
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        attrs['type'] = models.Dimension.get_type_by_name(
                                definition.get('type', 'string'))
        try:
            dim = models.Dimension.objects.get(code_name=code_name)
        except models.Dimension.DoesNotExist:
            logger.info("Creating Dimension '%s'", code_name)
            dim = models.Dimension(code_name=code_name, **attrs)
            dim.save()
        else:
            logger.debug("Dimension '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    if getattr(dim, name) != value:
                        updated.append(name)
                        setattr(dim, name, value)
                if updated:
                    dim.save()
                    logger.info("Updated '%s' of dimension '%s'",
                                 ",".join(updated), code_name)
        

    @transaction.commit_on_success()
    def _create_data_type(self, code_name, definition, force_update=False):
        # the data type itself
        attrs = {}
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        # amount dim
        amount_dim_code = definition.get("amount_dim")
        if not amount_dim_code:
            logger.error("Missing amount_dim name")
            raise CommandError("Error in input file.")
        try:
            amount_dim = models.Dimension.objects.get(code_name=amount_dim_code)
        except models.Dimension.DoesNotExist:
            logger.error("Amount dimension '%s' referenced by DataType '%s' "
                         "does not exist", amount_dim_code, code_name)
            raise CommandError("Error in input file.")
        attrs["amount_dim"] = amount_dim
        attrs["recurrent"] = definition.get("recurrent", False)
        attrs["summable_periods"] = definition.get("summable_periods", True)
        attrs["update_interval"] = definition.get("update_interval", None)
        attrs["fault_tolerance"] = definition.get("fault_tolerance", 0)
        
        # save the datatype
        try:
            dtype = models.DataType.objects.get(code_name=code_name)
        except models.DataType.DoesNotExist:
            logger.info("Creating DataType '%s'", code_name)
            dtype = models.DataType(code_name=code_name, **attrs)
            dtype.save()
        else:
            logger.debug("DataType '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    if getattr(dtype, name) != value:
                        updated.append(name)
                        setattr(dtype, name, value)
                if updated:
                    dtype.save()
                    logger.info("Updated '%s' of data type '%s'",
                                 ",".join(updated), code_name)
        # connect it with dimensions
        for pos, dim_ref in enumerate(definition.get('dimensions', [])):
            if type(dim_ref) is dict:
                dim_code = dim_ref['name']
                partitioning = dim_ref.get('partitioning', True)
                sorting = models.DimensionToDataType.\
                    get_sorting_by_name(dim_ref.get('sorting', None))
            else:
                dim_code = dim_ref
                partitioning = True
                sorting = None
            try:
                dim = models.Dimension.objects.get(code_name=dim_code)
            except models.Dimension.DoesNotExist:
                logger.error("Dimension '%s' reference in DataType '%s' does "
                             "not exist", dim_code, code_name)
                raise CommandError("Error in input file.")
            else:
                # connect Dimensions with Datatype
                try:
                    dtt = models.DimensionToDataType.objects.get(data_type=dtype,
                                                                 dimension=dim)
                except models.DimensionToDataType.DoesNotExist:
                    logger.info("Connecting DataType '%s' with Dimension '%s'",
                                code_name, dim_code)
                    dtt = models.DimensionToDataType(data_type=dtype,
                                                     dimension=dim,
                                                     position=pos,
                                                     partitioning=partitioning,
                                                     sorting=sorting)
                    dtt.save()
                else:
                    logger.debug("DataType '%s' already connected to Dimension "
                                 "'%s'", code_name, dim_code)
                    save = False
                    if dtt.position != pos:
                        dtt.position = pos
                        logger.info("Updating 'position' to '%s' ", pos)
                        save = True
                    if dtt.partitioning != partitioning:
                        dtt.partitioning = partitioning
                        logger.info("Updating 'partitioning' to '%s' ",
                                     partitioning)
                        save = True
                    if dtt.sorting != sorting:
                        dtt.sorting = sorting
                        logger.info("Updating 'sorting' to '%s' for connection %s with %s",
                                    dtt.get_sorting_string(),
                                    dtype.code_name, dim.code_name)
                        save = True
                    if save:
                        dtt.save()
