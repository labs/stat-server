# build-in modules
import logging
import urllib2
import json
from urlparse import urljoin

# Django-related imports
from django.core.management.base import NoArgsCommand
from django.conf import settings

# local imports
from unifiedstatistics.models import MenuItem

# basic setup
logger = logging.getLogger("unifiedstatistics.update_menu")

class Command(NoArgsCommand):

    help = "Updates static menu items from main CZ.NIC website"

    MENU_DEFINITION_URL = 'http://www.nic.cz/sitemap/json'

    # links to follow in menu structure in order from top to down level
    TRAVERSE_ITEMS = ['/page/313/', 'https://stats.nic.cz/']

    def handle(self, **options):
        for lang, _ in settings.LANGUAGES:
            opener = urllib2.build_opener()
            opener.addheaders.append(('Cookie', 'django_language=' + lang))
            try:
                f = opener.open(self.MENU_DEFINITION_URL)
                menu = json.load(f)
                level = 0
                while menu:
                    self.update_single_level(lang, level, menu)
                    if level < len(self.TRAVERSE_ITEMS):
                        menu = dict(map(lambda x: (x[1], x[2]), menu)).get(self.TRAVERSE_ITEMS[level])
                    else:
                        menu = None
                    level += 1
                to_delete = MenuItem.objects.filter(level__gte=level)
                if to_delete.count():
                    logger.info("Deleting old menu item on level %d and higher", level)
                    to_delete.delete()
            except urllib2.HTTPError:
                logger.warn("Failed to fetch menu definitions from %s", self.MENU_DEFINITION_URL)
            except ValueError:
                logger.warn("Failed to parse JSON menu definitions from %s", self.MENU_DEFINITION_URL)

    def update_single_level(self, lang, level, data):
        for pos, item in enumerate(data):
            try:
                mi = MenuItem.objects.get(level=level, position=pos)
            except MenuItem.DoesNotExist:
                mi = MenuItem(level=level, position=pos)
                logger.info("Creating new menu item on level %d, position %d", mi.level, mi.position)
                mi.save()
            save = False
            old_value = getattr(mi, 'label_' + lang)
            if old_value != item[0]:
                setattr(mi, 'label_' + lang, item[0])
                save = True
            old_value = getattr(mi, 'href_' + lang)
            new_value = urljoin('http://www.nic.cz/', item[1])
            if old_value != new_value:
                setattr(mi, 'href_' + lang, new_value)
                save = True
            is_active = item[1] in self.TRAVERSE_ITEMS
            if mi.active != is_active:
                mi.active = is_active
                save = True
            if save:
                logger.info("Updating menu item on level %d, position %d", mi.level, mi.position)
                mi.save()
        to_delete = MenuItem.objects.filter(level=level).filter(position__gt=pos)
        if to_delete.count():
            logger.info("Deleting old menu item on level %d", level)
            to_delete.delete()
