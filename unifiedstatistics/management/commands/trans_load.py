# build-in modules
import os
import logging
import csv
from optparse import make_option

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.trans_load")

class Command(BaseCommand):
    
    args = "JSON file"
    
    option_list = BaseCommand.option_list + (
        make_option('-f', '--force-update',
            action='store_true',
            dest='force_update',
            default=False,
            help='Force update on instances different from db'),
        )
    
    def handle(self, *args, **options):
        if not args:
            raise CommandError('Give me a CSV file to load from')
        force_update = options.get('force_update', False)

        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        else:
            logger.setLevel(logging.INFO)

        for arg in args:
            if not os.path.isfile(arg):
                raise CommandError('File "%s" does not exist' % arg)
            self._load_csv_file(arg, force_update=force_update)
                
        
    @transaction.commit_on_success()
    def _load_csv_file(self, filename, force_update=False):
        with file(filename, "r") as infile:
            reader = csv.DictReader(infile, delimiter="|")
            for row in reader:
                dim_code_name = row['dimension__code_name']
                text = row['text']
                try:
                    t2i = models.TextToIntMap.objects.get(
                                            dimension__code_name=dim_code_name,
                                            text=text)
                except models.TextToIntMap.DoesNotExist:
                    try:
                        dim = models.Dimension.objects.get(code_name=dim_code_name)
                        if dim.type != models.Dimension.TYPE_TEXT:
                            logger.warn("Dimension %s is not text type, "
                                        "skipping", dim_code_name)
                            continue
                        t2i = models.TextToIntMap(dimension=dim,
                                                  text=text)
                        logger.info("Creating %s::%s in advance",
                                    dim_code_name, text)
                        t2i.save()
                    except models.Dimension.DoesNotExist:
                        logger.warn("Dimension %s does not exits, "
                                    "skipping", dim_code_name)
                        continue

                save = False
                for key, value in row.iteritems():
                    value = value.decode('utf-8')
                    if key == "custom_order":
                        value = int(value)
                    if key not in ("text", "dimension__code_name"):
                        old_value = getattr(t2i, key)
                        if old_value == value:
                            # nothing to do
                            pass
                        elif not old_value:
                            setattr(t2i, key, value)
                            save = True
                        elif force_update:
                            logger.info("Forcing update of value '%s' "
                                        "of %s::%s",
                                        key, dim_code_name, text)
                            setattr(t2i, key, value)
                            save = True
                        else:
                            logger.warn("Not updating existing value '%s' "
                                        "of %s::%s",
                                        key, dim_code_name, text)
                if save:
                    logger.info("Updating %s::%s", dim_code_name, text)
                    t2i.save()
