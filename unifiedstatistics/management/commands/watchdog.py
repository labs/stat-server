# build-in modules
import logging
import datetime
import sys

# Django imports
from django.core.management.base import NoArgsCommand

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.watchdog")

class Command(NoArgsCommand):
    
    # watchdog return codes:
    STATE_OK = 0
    STATE_WARNING = 1
    STATE_CRITICAL = 2
    STATE_UNKNOWN = 3
    STATE_DEPENDENT = 4
    
    def handle(self, **options):
        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        elif verbosity in ("2", "3"):
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)
            
        not_checked = models.DataType.objects.filter(update_interval=None)
        if len(not_checked) > 0:
            logger.info("Following data types are not checked because of missing "
                         "update interval definition:\n- %s",
                         "\n- ".join(map(lambda x: x.code_name, not_checked))) 
        dts_to_check = models.DataType.objects.filter(update_interval__gt=0)
        now = datetime.datetime.now()
        return_code = self.STATE_OK
        for dt in dts_to_check:
            logger.debug("Checking %s...", dt.code_name)
            dps = dt.datapoint_set.order_by('-date')[:1]
            if (len(dps) > 0):
                logger.debug("Last data point: %s", dps[0].date.date().isoformat())
                delta_since_last = now - dps[0].date
                tolerance_limit = datetime.timedelta(days=(dt.fault_tolerance + dt.update_interval))
                escalation_limit = datetime.timedelta(days=(2*dt.fault_tolerance + dt.update_interval))
                missing_datetime = dps[0].date + datetime.timedelta(days=dt.update_interval)
                missing_delta = now - missing_datetime
                if delta_since_last > escalation_limit:
                    return_code = max(return_code, self.STATE_CRITICAL)
                    logger.error("Missing values for data type %s since %s (%d days ago)",
                                   dt.name_en,
                                   missing_datetime.date().isoformat(),
                                   missing_delta.days)
                elif delta_since_last > tolerance_limit:
                    return_code = max(return_code, self.STATE_WARNING)
                    logger.warning("Missing values for data type %s since %s (%d days ago)",
                                   dt.name_en,
                                   missing_datetime.date().isoformat(),
                                   missing_delta.days)
                    
            else:
                return_code = max(return_code, self.STATE_WARNING)
                logger.warning("No values for data type %s yet",
                               dt.code_name)
        sys.exit(return_code)
