# build-in modules
import os
import logging
import json
from optparse import make_option
import hashlib
import yaml

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.conf import settings
from django.core.files import File

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.load_reports_defs")

class Command(BaseCommand):
    
    args = "JSON or YAML file"
    
    option_list = BaseCommand.option_list + (
        make_option('-f', '--force-update',
            action='store_true',
            dest='force_update',
            default=False,
            help='Force update on instances different from db'),
        )
    
    def handle(self, *args, **options):
        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        elif verbosity in ("2", "3"):
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        if not args:
            raise CommandError('Give me a JSON or YAML file to load from')
        force_update = options.get('force_update', False)
        for arg in args:
            if not os.path.isfile(arg):
                raise CommandError('File "%s" does not exist' % arg)
            if arg.endswith(".json"):
                loader = json
            else:
                loader = yaml
            try:
                with file(arg, 'r') as infile:
                    data = loader.load(infile)
            except Exception as exc:
                raise CommandError('Cannot read file "%s" - error "%s"' % \
                                   (arg, exc))

            # reports
            valid_code_names = []
            reports = data.get('reports', {})
            for code_name, definition in reports.iteritems():
                valid_code_names.append(code_name)
                self._create_report(code_name, definition,
                                        force_update=force_update)

            # deleting old reports
            old_reports = models.Report.objects.exclude(code_name__in=valid_code_names)
            if old_reports.count() > 0 and force_update:
                logger.info("Deleting old %r", old_reports)
                old_reports.delete()
                
            # report groups
            valid_code_names = []
            report_groups = data.get("report_groups", {})
            for code_name, definition in report_groups.iteritems():
                valid_code_names.append(code_name)
                self._create_report_group(code_name, definition,
                                        force_update=force_update)

            # deleting old report groups
            old_report_groups = models.ReportGroup.objects.exclude(code_name__in=valid_code_names)
            if old_report_groups.count() > 0 and force_update:
                logger.info("Deleting old %r", old_report_groups)
                old_report_groups.delete()
                    
    @transaction.commit_on_success()
    def _create_report(self, code_name, definition, force_update=False):
        # the report itself
        attrs = {}
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        for lang, name in definition.get('info', {}).iteritems():
            attrs["info_"+lang] = name
        for lang, name in definition.get('report_file', {}).iteritems():
            attrs["report_file_"+lang] = File(file(name, 'r'))
        
        try:
            report = models.Report.objects.get(code_name=code_name)
        except models.Report.DoesNotExist:
            logger.info("Creating Report '%s'", code_name)
            report = models.Report(code_name=code_name, **attrs)
            report.save()
        else:
            logger.debug("Report '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    old_value = getattr(report, name)
                    if hasattr(old_value, 'size'):
                        # this is a file?
                        if not self._compare_files(old_value, value):
                            updated.append(name)
                            setattr(report, name, value)
                    else:
                        if old_value != value:
                            updated.append(name)
                            setattr(report, name, value)
                if updated:
                    report.save()
                    logger.info("Updated '%s' of Report '%s'",
                                 ",".join(updated), code_name)
        # connect it with report sections and with statistics
        valid_sections = []
        pos = 0
        for section_pos, section_def in enumerate(definition.get('sections', [None])):
            # valid section handling
            if section_def:
                # attributes
                attrs = {}
                for lang, name in section_def.get('name', {}).iteritems():
                    attrs["name_"+lang] = name
                try:
                    report_section = models.ReportSection.objects.get(position=section_pos, report=report)
                except models.ReportSection.DoesNotExist:
                    logger.info("Creating Report section %d for Report '%s'", section_pos, code_name)
                    report_section = models.ReportSection(report=report, position=section_pos, **attrs)
                    report_section.save()
                else:
                    logger.debug("Report section %d for Report '%s' already exists", section_pos, code_name)
                    if force_update:
                        updated = []
                        for name, value in attrs.iteritems():
                            old_value = getattr(report_section, name)
                            if old_value != value:
                                updated.append(name)
                                setattr(report_section, name, value)
                        if updated:
                            report_section.save()
                            logger.info("Updated '%s' of Report section %d for Report '%s'",
                                         ",".join(updated), section_pos, code_name)
                valid_sections.append(report_section)
            else:
                # report without sections - statistics are defined directly under report definition
                report_section = None
                section_def = definition
            
            # connect it with statistics
            for stat_def in section_def.get('statistics', []):
                stat_code = stat_def.get('code_name')
                if not stat_code:
                    raise CommandError('code_name missing for statistics on position ' + pos)
                
                # getting referenced statistics object
                try:
                    stat = models.Statistics.objects.get(code_name=stat_code)
                except models.Statistics.DoesNotExist:
                    logger.error("Statistics '%s' referenced in Report '%s' does "
                                 "not exist", stat_code, code_name)
                    raise CommandError("Error in input file.")

                # default params
                default_params = stat_def.get("default_params")
                param_set = None
                if default_params:
                    ps_code = default_params.get('code_name')
                    if not ps_code:
                        raise CommandError('code_name missing from "default_params"')
                    try:
                        param_set = models.ChartParamSet.objects.get(code_name=ps_code)
                    except models.ChartParamSet.DoesNotExist:
                        logger.info("Creating ChartParamSet '%s'", ps_code)
                        param_set = models.ChartParamSet(**default_params)
                        param_set.save()
                    else:
                        logger.debug("ChartParamSet '%s' already exists", ps_code)
                        if force_update:
                            updated = []
                            # Check if stored values of unfilled parameters use default values
                            for field in param_set._meta.fields:
                                if (not field.name in default_params) and (not field.auto_created):
                                    old_value = getattr(param_set, field.name)
                                    default_value = field.get_default()
                                    if old_value != default_value:
                                        updated.append(field.name)
                                        setattr(param_set, field.name, default_value)
                            # Check values of filled parameters
                            for name, value in default_params.iteritems():
                                old_value = getattr(param_set, name)
                                value = param_set._meta.get_field_by_name(name)[0].get_prep_value(value) 
                                if old_value != value:
                                    updated.append(name)
                                    setattr(param_set, name, value)
                            if updated:
                                param_set.save()
                                logger.info("Updated '%s' of param_set '%s'",
                                             ",".join(updated), ps_code)
    
                # attributes
                attrs = {}
                attrs['statistics'] = stat
                attrs['params'] = param_set
                attrs['report_section'] = report_section
                
                for lang, _dummy in settings.LANGUAGES: 
                    attrs['name_' + lang] = ''
                    attrs['info_' + lang] = ''
                    attrs['highlight_' + lang] = ''
                for lang, name in stat_def.get('name', {}).iteritems():
                    attrs["name_" + lang] = name
                for lang, name in stat_def.get('info', {}).iteritems():
                    attrs["info_" + lang] = name
                for lang, name in stat_def.get('highlight', {}).iteritems():
                    attrs["highlight_" + lang] = name
                
                try:
                    rts = models.ReportToStatistics.objects.get(report=report,
                                                                position=pos)
                except models.ReportToStatistics.DoesNotExist:
                    logger.info("Connecting Report '%s' with Statistics '%s'",
                                code_name, stat_code)
                    rts = models.ReportToStatistics(report=report,
                                                    position=pos,
                                                    **attrs)
                    rts.save()
                else:
                    logger.debug("Report '%s' already connected with statistics on position %d", code_name, pos)
                    if force_update:
                        updated = []
                        for name, value in attrs.iteritems():
                            if getattr(rts, name) != value:
                                updated.append(name)
                                setattr(rts, name, value)
                        if updated:
                            rts.save()
                            logger.info("Updated '%s' of %r",
                                         ",".join(updated), rts)

                # increment counter of statistics in report
                pos += 1

        # delete old connections between statistics and processed report section
        old_r2s = models.ReportToStatistics.objects.filter(report=report,
                                                           position__gte=pos)
        if old_r2s.count() > 0 and force_update:
            logger.info("Deleting old connections %r", old_r2s)
            old_r2s.delete()
        
        # delete old report sections
        valid_sections_ids = map(lambda x: x.id, valid_sections)
        old_sections = models.ReportSection.objects.filter(report=report).exclude(id__in=valid_sections_ids)
        if old_sections.count() > 0 and force_update:
            logger.info("Deleting unused sections %r", old_sections)
            old_sections.delete()
            
    @transaction.commit_on_success()
    def _create_report_group(self, code_name, definition, force_update=False):
        # the data type itself
        attrs = {}
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        attrs['position'] = definition.get("position", 100)
        try:
            rgroup = models.ReportGroup.objects.get(code_name=code_name)
        except models.ReportGroup.DoesNotExist:
            logger.info("Creating ReportGroup '%s'", code_name)
            rgroup = models.ReportGroup(code_name=code_name, **attrs)
            rgroup.save()
        else:
            logger.debug("ReportGroup '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    if getattr(rgroup, name) != value:
                        updated.append(name)
                        setattr(rgroup, name, value)
                if updated:
                    rgroup.save()
                    logger.info("Updated '%s' of ReportGroup '%s'",
                                 ",".join(updated), code_name)
        # connect it with reports
        valid_report_codes = []
        for pos, report_code in enumerate(definition.get('reports', [])):
            try:
                report = models.Report.objects.get(code_name=report_code)
            except models.Report.DoesNotExist:
                logger.error("Report '%s' referenced in ReportGroup '%s' does "
                             "not exist", report_code, code_name)
                raise CommandError("Error in input file.")
            else:
                valid_report_codes.append(report_code)
                # connect Report with ReportGroup
                try:
                    rgtr = models.ReportGroupToReport.objects.get(
                                            report_group=rgroup, report=report)
                except models.ReportGroupToReport.DoesNotExist:
                    logger.info("Connecting ReportGroup '%s' with Report '%s'",
                                code_name, report_code)
                    rgtr = models.ReportGroupToReport(report_group=rgroup,
                                                        report=report,
                                                        position=pos)
                    rgtr.save()
                else:
                    logger.debug("ReportGroup '%s' already connected to Report "
                                 "'%s'", code_name, report_code)
                    if rgtr.position != pos:
                        logger.info("Updating position of '%s'", report_code)
                        rgtr.position = pos
                        rgtr.save()

        # deleting old connections
        old_connections = rgroup.reportgrouptoreport_set.exclude(report__code_name__in=valid_report_codes)
        if old_connections.count() > 0 and force_update:
            logger.info("Deleting old %r", old_connections)
            old_connections.delete()

    def _compare_files(self, f1, f2):
        def do_hash(fobj):
            hasher = hashlib.sha1()
            hasher.update(fobj.read())
            return hasher.hexdigest()
        return do_hash(f1) == do_hash(f2)
