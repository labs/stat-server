# build-in modules
import logging
from optparse import make_option

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Min, Max, Sum

# local imports
from unifiedstatistics import models
from unifiedstatistics.aggregation import Aggregator

# basic setup
logger = logging.getLogger("unifiedstatistics.modify_series")

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('-x', '--delete',
            action='store_true',
            dest='delete',
            default=None,
            help='Delete given series from given dimension'),
        make_option('-m', '--merge',
            action='store',
            dest='merge',
            default=None,
            help='Destination series to which series specified by option -s should be merged'),
        make_option('-s', '--series',
            action='store',
            dest='series',
            default=None,
            help='Series from given dimension to be modified'),
        make_option('-d', '--dimension',
            action='store',
            dest='dim_name',
            default=None,
            help='Code name of selected dimension to be modified'),
        make_option('-t', '--datatype',
            action='store',
            dest='dt_name',
            default=None,
            help='Code name of selected data type to be modified'),
        )
   
    def handle(self, *args, **options):
        delete = options.get('delete')
        merge = options.get('merge')
        dim_name = options.get('dim_name')
        dt_name = options.get('dt_name')
        series = options.get('series')
        
        if not dt_name:
            raise CommandError("Option -t is required")
        if not dim_name:
            raise CommandError("Option -d is required")
        if not series:
            raise CommandError("Option -s is required")
        
        try:
            data_type = models.DataType.objects.get(code_name=dt_name)
        except models.DataType.DoesNotExist:
            raise CommandError("Data type with code_name '%s' does not exist" % dt_name)
        
        try:
            dimension = models.Dimension.objects.get(code_name=dim_name)
        except models.Dimension.DoesNotExist:
            raise CommandError("Dimension with code_name '%s' does not exist" % dim_name)
        
        dt_dims = data_type.get_dimensions()
        if not dimension in dt_dims:
            raise CommandError("Dimension '%s' is not connected with data type '%s'" % (dim_name, dt_name))
        dim_column = "value%d" % (dt_dims.index(dimension) + 1)
        series_int = dimension.value_to_int(series)
        if not series_int:
            raise CommandError("Invalid series value '%s'" % series)

        if delete and merge:
            raise CommandError("Options -x and -m are mutually exclusive")
        if not (delete or merge):
            raise CommandError("One from the options -x and -m is required")
        if delete:
            self.delete_series(data_type, dim_column, series_int)
        elif merge:
            merge_int = dimension.value_to_int(merge)
            if not merge_int:
                raise CommandError("Invalid series value '%s'" % merge)
            self.merge_series(data_type, dim_column, series_int, merge_int)

    def delete_series(self, data_type, dim_column, series):
        """This method deletes data points of given series and data_type. Note
        that possible TextToIntMap mapping is not deleted as it can be used
        with another data types.
        
        :param data_type: DataType instance with series to delete
        :type data_type: :py:class:`unifiedstatistics.models.DataType`
        :param dim_column: name of field in DataPoint with given series (e.g. value1, value2, etc.)
        :type dim_column: str
        :param series: value of series to deleted
        :type series: int
        """
        filter_values = {}
        filter_values[dim_column] = series
        filter_values['data_type'] =  data_type
        date_range = models.DataPoint.objects.filter(**filter_values).aggregate(Min('date'), Max('date'))
        start_date = date_range['date__min']
        end_date = date_range['date__max']
        if start_date and end_date:
            models.DataPoint.objects.filter(**filter_values).delete()
            models.DataPointAggreg.objects.filter(**filter_values).delete()
            logger.info("Data points successfully deleted")
            logger.info("Updating date point aggregations")
            Aggregator.create_aggregs(time_period="1m",
                                      start_date=start_date, end_date=end_date,
                                      data_type=data_type)        
        else:
            logger.info("No data points to delete")

    def merge_series(self, data_type, dim_column, origin_series, dest_series):
        """This method adds amount from origin_series data points to appropriate
        data points of dest_series and delete origin_series data points.
        
        :param data_type: DataType instance with series to merge
        :type data_type: :py:class:`unifiedstatistics.models.DataType`
        :param dim_column: name of field in DataPoint with given series (e.g. value1, value2, etc.)
        :type dim_column: str
        :param origin_series: value of series to merge (and delete)
        :type origin_series: int
        :param dest_series: value of series to be merged with
        :type dest_series: int
        """
        filter_values = {}
        filter_values[dim_column + '__in'] = [origin_series, dest_series]
        filter_values['data_type'] =  data_type
        group_columns = ['date']
        value_columns = filter(lambda x: x.startswith('value'), models.DataPoint._meta.get_all_field_names())
        for col in value_columns:
            if col != dim_column:
                group_columns.append(col)
        datapoint_sums = models.DataPoint.objects.filter(**filter_values).values(*group_columns).annotate(Sum('amount')).order_by(*group_columns)
        for record in datapoint_sums:
            amount = record.pop('amount__sum')
            record[dim_column] = dest_series
            try:
                # update destination series with sum 
                point = models.DataPoint.objects.get(data_type=data_type, **record)
                if point.amount != amount:
                    logger.debug("Updating amount in data point from %r to value %d", point.date, amount)
                    point.amount = amount
                    point.save()
            except models.DataPoint.DoesNotExist:
                # destination series does not exist with given field values, create it
                point = models.DataPoint(amount=amount, **record)
                logger.debug("Creating new data point for %r with value %d", point.date, amount)
                point.save()
        logger.info("Data points successfully merged")
        # finally we need to delete the old series
        self.delete_series(data_type, dim_column, origin_series)
