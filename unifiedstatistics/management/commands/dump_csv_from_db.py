# build-in modules
import datetime
import os
import logging
import time
import csv
from optparse import make_option
from datetime import date

# Django imports
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction
from django.db.backends import BaseDatabaseWrapper
from django.db.backends.util import CursorWrapper

# local imports
from unifiedstatistics import models
from unifiedstatistics.aggregation import Aggregator 

# basic setup
logger = logging.getLogger("unifiedstatistics.update_db")

# this is here to prevent storage of run queries in connection.queries
# in DEBUG mode
if settings.DEBUG:
    BaseDatabaseWrapper.make_debug_cursor = lambda self, cursor:\
                                            CursorWrapper(cursor, self)


class Command(BaseCommand):
    
    option_list = BaseCommand.option_list + (
        make_option('-t', '--data-type',
            action='store',
            dest='data_type',
            default="ALL",
            help='code_name of data type to dump'),
        make_option('-o', '--outdir',
            action='store',
            dest='outdir',
            default="./",
            help='Where to put CSV data'),   
        make_option('-s', '--start-date',
            action='store',
            dest='start_date',
            default=None,
            help='Start date for created aggregations'),
        make_option('-e', '--end-date',
            action='store',
            dest='end_date',
            default=None,
            help='End date for created aggregations'),                                
        )

    def handle(self, **options):
        query = models.DataPoint.objects.all()
        start_date = options.get('start_date')
        end_date = options.get('end_date')
        data_type_name = options.get('data_type')
        self.outdir = options.get('outdir')
        if start_date:
            year, month, day = map(int, start_date.split("-"))
            start_date = date(year=year, month=month, day=day)
            query = query.filter(date__gte=start_date)
        if end_date:
            year, month, day = map(int, end_date.split("-"))
            end_date = date(year=year, month=month, day=day)
            query = query.filter(date__lte=end_date)
        if data_type_name != 'ALL':
            query = query.filter(data_type__code_name=data_type_name)
        query = query.order_by("date", "data_type","value1","value2","value3").\
                    values_list("date", "data_type__code_name", "value1",
                                "value2", "value3", "amount")
        self.prepare_remaps()
        current_data = []
        current_key = None
        for row in query.iterator():
            key = (row[0], row[1])
            if key != current_key:
                if current_data:
                    self.dump_data(current_key, current_data)
                current_data = []
                current_key = key
            current_data.append(row[2:])
        if current_data:
            self.dump_data(current_key, current_data)


    def prepare_remaps(self):
        """create dictionaries useful for remapping int values to strings"""
        data_type_to_dims = {}
        for data_type in models.DataType.objects.all():
            data_type_to_dims[data_type.code_name] = [dim.code_name for dim in
                                                      data_type.get_dimensions()]
        self.data_type_to_dims = data_type_to_dims
        dim_int_to_text = {}
        for tti in models.TextToIntMap.objects.all():
            d_name = tti.dimension.code_name
            if d_name not in dim_int_to_text:
                dim_int_to_text[d_name] = {}
            dim_int_to_text[d_name][tti.id] = tti.text
        self.dim_int_to_text = dim_int_to_text

    def dump_data(self, key, data):
        #logger.debug("Dumping %s", key)
        date, data_type = key
        filename = "%04d%02d%02d-%s.csv" % (date.year, date.month, date.day,
                                            data_type)
        dims = self.data_type_to_dims[data_type]
        remaps = [self.dim_int_to_text[dim] for dim in dims]
        new_data = []
        for row in data:
            new_row = []
            for i, remap in enumerate(remaps):
                new_row.append(remap[row[i]].encode('utf-8'))
            new_row.append(row[-1])
            new_data.append(new_row)
        with file(os.path.join(self.outdir, filename), "w") as outfile:
            writer = csv.writer(outfile, delimiter="|")
            writer.writerows(new_data)
