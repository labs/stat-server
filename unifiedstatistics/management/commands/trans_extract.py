# build-in modules
import logging
import csv
from cStringIO import StringIO
import sys

# Django imports
from django.core.management.base import NoArgsCommand
from django.conf import settings

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.extract_trans")


class Command(NoArgsCommand):
    def handle(self, **options):
        cols = ["dimension__code_name", "text"]
        for lang_code, _lang in settings.LANGUAGES:
            cols.append("text_local_"+lang_code)
        cols += ["custom_color","custom_order"]
        out = StringIO()
        writer = csv.writer(out, delimiter="|")
        writer.writerow(cols)
        for row in models.TextToIntMap.objects.all().\
                    order_by("dimension__code_name", "text").values_list(*cols):
            new_row = []
            for val in row:
                if type(val) is int:
                    new_row.append(val)
                elif val is None:
                    new_row.append("")
                else:
                    new_row.append(val.encode('utf-8'))
            writer.writerow(new_row)
        sys.stdout.write(out.getvalue())
        
