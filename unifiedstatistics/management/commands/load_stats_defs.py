# build-in modules
import os
import logging
import json
from optparse import make_option
import hashlib
import yaml

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.core.files import File
from django.db.models.fields import FieldDoesNotExist, NOT_PROVIDED

# local imports
from unifiedstatistics import models

# basic setup
logger = logging.getLogger("unifiedstatistics.load_stats_defs")

class Command(BaseCommand):
    
    args = "JSON or YAML file"
    
    option_list = BaseCommand.option_list + (
        make_option('-f', '--force-update',
            action='store_true',
            dest='force_update',
            default=False,
            help='Force update on instances different from db'),
        )
    
    def handle(self, *args, **options):
        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        elif verbosity in ("2", "3"):
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        if not args:
            raise CommandError('Give me a JSON or YAML file to load from')
        force_update = options.get('force_update', False)
        for arg in args:
            if not os.path.isfile(arg):
                raise CommandError('File "%s" does not exist' % arg)
            if arg.endswith(".json"):
                loader = json
            else:
                loader = yaml
            try:
                with file(arg, 'r') as infile:
                    data = loader.load(infile)
            except Exception as exc:
                raise CommandError('Cannot read file "%s" - error "%s"' % \
                                   (arg, exc))
            # statistics go before statistics groups
            statistics = data.get('statistics', {})
            valid_code_names = []
            for code_name, definition in statistics.iteritems():
                valid_code_names.append(code_name)
                self._create_statistics(code_name, definition,
                                        force_update=force_update)

            # deleting old statistics
            old_statistics = models.Statistics.objects.exclude(code_name__in=valid_code_names)
            if old_statistics.count() > 0 and force_update:
                logger.info("Deleting old %r", old_statistics)
                old_statistics.delete()

            # statistics groups
            stat_groups = data.get("stat_groups", {})
            valid_code_names = []
            for code_name, definition in stat_groups.iteritems():
                valid_code_names.append(code_name)
                self._create_stat_group(code_name, definition,
                                        force_update=force_update)
            # deleting old statistics groups
            old_stat_groups = models.StatGroup.objects.exclude(code_name__in=valid_code_names)
            if old_stat_groups.count() > 0 and force_update:
                logger.info("Deleting old %r", old_stat_groups)
                old_stat_groups.delete()
            

        
    @transaction.commit_on_success()
    def _create_statistics(self, code_name, definition, force_update=False):
        
        def _get_params(params_id):
            params_definition = definition.get(params_id)
            param_set = None
            if params_definition:
                ps_code = params_definition.get('code_name')
                if not ps_code:
                    raise CommandError("code_name missing from '%s'" % params_id)
                try:
                    param_set = models.ChartParamSet.objects.get(code_name=ps_code)
                except models.ChartParamSet.DoesNotExist:
                    logger.info("Creating ChartParamSet '%s'", ps_code)
                    param_set = models.ChartParamSet(**params_definition)
                    param_set.save()
                else:
                    logger.debug("ChartParamSet '%s' already exists", ps_code)
                    if force_update:
                        updated = []
                        for name, value in params_definition.iteritems():
                            try:
                                field_type = models.ChartParamSet._meta.get_field(name).get_internal_type()
                            except FieldDoesNotExist:
                                logger.warning("ChartParamSet '%s' has no field named '%s'. Ignoring.", ps_code, name)
                            else:
                                if type(value) == int and field_type == 'CharField':
                                    value = str(value)
                                old_value = getattr(param_set, name)
                                if old_value != value:
                                    updated.append(name)
                                    setattr(param_set, name, value)
                        # null unused fields or set them to default value
                        unused_fields = set(map(lambda f: f.name, param_set._meta.fields)) - set(params_definition) - set(['code_name'])
                        for field_name in unused_fields:
                            field = param_set._meta.get_field_by_name(field_name)[0]
                            old_value = getattr(param_set, field_name)
                            if (field.default == NOT_PROVIDED) and field.null and (old_value != None):
                                updated.append(field_name)
                                setattr(param_set, field_name, None)
                            elif (field.default != NOT_PROVIDED) and (old_value != field.default):
                                updated.append(field_name)
                                setattr(param_set, field_name, field.default)
                        if updated:
                            param_set.save()
                            logger.info("Updated '%s' of param_set '%s'",
                                         ",".join(updated), ps_code)
            return param_set
                        
        default_params = _get_params("default_params")
        enforced_params = _get_params("enforced_params")
        
        # the statistics itself
        attrs = dict(default_params=default_params, enforced_params=enforced_params)
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        for lang, name in definition.get('info', {}).iteritems():
            attrs["info_"+lang] = name
        for lang, name in definition.get('methodology', {}).iteritems():
            attrs["methodology_"+lang] = name
        for lang, name in definition.get('stat_file', {}).iteritems():
            attrs["stat_file_"+lang] = File(file(name, 'r'))
        dt_code = definition.get("data_type")
        if dt_code is None:
            data_type = None
        else:
            try:
                data_type = models.DataType.objects.get(code_name=dt_code)
            except models.DataType.DoesNotExist:
                logger.error("DataType '%s' referenced from Statistics '%s' "
                             "does not exits", dt_code, code_name) 
                raise CommandError('Inconsistency in JSON data')
        attrs['data_type'] = data_type
        try:
            stat = models.Statistics.objects.get(code_name=code_name)
        except models.Statistics.DoesNotExist:
            logger.info("Creating Statistics '%s'", code_name)
            stat = models.Statistics(code_name=code_name, **attrs)
            stat.save()
        else:
            logger.debug("Statistics '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    old_value = getattr(stat, name)
                    if hasattr(old_value, 'size'):
                        # this is a file?
                        if not self._compare_files(old_value, value):
                            updated.append(name)
                            setattr(stat, name, value)
                    else:
                        if old_value != value:
                            updated.append(name)
                            setattr(stat, name, value)
                if updated:
                    stat.save()
                    logger.info("Updated '%s' of statistics '%s'",
                                 ",".join(updated), code_name)
        

    @transaction.commit_on_success()
    def _create_stat_group(self, code_name, definition, force_update=False):
        # the statistics group itself
        attrs = {}
        for lang, name in definition.get('name', {}).iteritems():
            attrs["name_"+lang] = name
        for lang, name in definition.get('info', {}).iteritems():
            attrs["info_"+lang] = name
        attrs['position'] = definition.get("position", 100)
        try:
            sgroup = models.StatGroup.objects.get(code_name=code_name)
        except models.StatGroup.DoesNotExist:
            logger.info("Creating StatGroup '%s'", code_name)
            sgroup = models.StatGroup(code_name=code_name, **attrs)
            sgroup.save()
        else:
            logger.debug("StatGroup '%s' already exists", code_name)
            if force_update:
                updated = []
                for name, value in attrs.iteritems():
                    if getattr(sgroup, name) != value:
                        updated.append(name)
                        setattr(sgroup, name, value)
                if updated:
                    sgroup.save()
                    logger.info("Updated '%s' of StatGroup '%s'",
                                 ",".join(updated), code_name)
        # connect it with statistics
        valid_stats = []
        for pos, stat_code in enumerate(definition.get('statistics', [])):
            try:
                stat = models.Statistics.objects.get(code_name=stat_code)
            except models.Statistics.DoesNotExist:
                logger.error("Statistics '%s' referenced in StatGroup '%s' does "
                             "not exist", stat_code, code_name)
                raise CommandError("Error in input file.")
            else:
                # connect StatGroup with Statistics
                valid_stats.append(stat)
                try:
                    sgts = models.StatGroupToStatistics.objects.get(
                                            stat_group=sgroup, statistics=stat)
                except models.StatGroupToStatistics.DoesNotExist:
                    logger.info("Connecting StatGroup '%s' with Statistics '%s'",
                                code_name, stat_code)
                    sgts = models.StatGroupToStatistics(stat_group=sgroup,
                                                        statistics=stat,
                                                        position=pos)
                    sgts.save()
                else:
                    logger.debug("StatGroup '%s' already connected to Statistics "
                                 "'%s'", code_name, stat_code)
                    if sgts.position != pos:
                        logger.debug("Updating position of '%s'", stat_code)
                        sgts.position = pos
                        sgts.save()
        old_sgts = models.StatGroupToStatistics.objects.filter(stat_group=sgroup).exclude(statistics__in=valid_stats)
        if old_sgts.count() > 0 and force_update:
            logger.info("Deleting old %r", old_sgts)
            old_sgts.delete()

                        
    def _compare_files(self, f1, f2):
        def do_hash(fobj):
            hasher = hashlib.sha1()
            hasher.update(fobj.read())
            return hasher.hexdigest()
        return do_hash(f1) == do_hash(f2)
