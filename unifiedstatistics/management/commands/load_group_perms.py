import os
import logging
import json
import yaml
from optparse import make_option

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Group
from guardian.shortcuts import get_perms, assign_perm, remove_perm, get_objects_for_group
from unifiedstatistics.models import Statistics

# basic setup
logger = logging.getLogger("unifiedstatistics.load_group_perms")

class Command(BaseCommand):
    
    args = "JSON or YAML file"
    
    option_list = BaseCommand.option_list + (
        make_option('-f', '--force-update',
            action='store_true',
            dest='force_update',
            default=False,
            help='Force update on instances different from db'),
        )
    
    def handle(self, *args, **options):
        verbosity = options.get("verbosity", "1")
        if verbosity == "0":
            logger.setLevel(logging.WARN)
        elif verbosity in ("2", "3"):
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)

        if not args:
            raise CommandError('Give me a JSON or YAML file to load from')
        force_update = options.get('force_update', False)
        for arg in args:
            if not os.path.isfile(arg):
                raise CommandError('File "%s" does not exist' % arg)
            if arg.endswith(".json"):
                loader = json
            else:
                loader = yaml
            try:
                with file(arg, 'r') as infile:
                    data = loader.load(infile)
            except Exception as exc:
                raise CommandError('Cannot read file "%s" - error "%s"' % \
                                   (arg, exc))
            for group_name, stats in data.iteritems():
                group, created = Group.objects.get_or_create(name=group_name)
                if created:
                    logger.info("Creating group %s", group_name)
                for stat_name in stats:
                    try:
                        stat = Statistics.objects.get(code_name=stat_name)
                    except Statistics.DoesNotExist:
                        logger.error("Statistics '%s' referenced in group '%s' "
                                     "does not exits", stat_name, group_name)
                    else:
                        if not 'view_statistics' in get_perms(group, stat):
                            logger.info("Statistics '%s' set available for group '%s'",
                                        stat_name, group_name)
                            assign_perm('view_statistics', group, stat)
                        if force_update:
                            # Remove all permissions for statistics not listed for processed group
                            old_authorization = get_objects_for_group(group, 'view_statistics', Statistics).exclude(code_name__in=stats)
                            if old_authorization:
                                logger.info("Deleting permission for group %r to view %r", group, old_authorization)
                                for old_stat in old_authorization:
                                    remove_perm('view_statistics', group, old_stat)
            if force_update:
                # Remove all permissions for groups not listed in the configuration
                for group in Group.objects.exclude(name__in=data.keys()):
                    old_authorization = get_objects_for_group(group, 'view_statistics', Statistics)
                    if old_authorization:
                        logger.info("Deleting permission for group %r to view %r", group, old_authorization)
                        for old_stat in old_authorization:
                            remove_perm('view_statistics', group, old_stat)
