# -*- coding: utf-8 -*-

# build-in modules
import shutil
import json
import cPickle
import urlparse
import yaml
import logging
from os import path, listdir
from tempfile import mkdtemp
from datetime import datetime
from uuid import uuid4 as randstring

# to be changed: create settings file for test
logging.disable(logging.INFO)
# end of 'to be changed'

logger = logging.getLogger('unifiedstatistics.unittest')

# try to import selenium; if it fails, set Firefox to None
try:
    from selenium.webdriver import Firefox
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.action_chains import ActionChains
    from selenium.webdriver.support.ui import Select
    from selenium.common.exceptions import NoSuchElementException, TimeoutException
except ImportError:
    Firefox = None
else:
    import time

# Django imports
from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.utils.unittest import skip
from django.utils.html import escape
from django.core.management import call_command

# local imports
from unifiedstatistics import models

# define a singleton and make Firefox be it
def singleton(cls):
    instances = {}
    def getinstance():
        if cls.__name__ not in instances:
            instances[cls.__name__] = cls()
        return instances[cls.__name__]
    return getinstance

if Firefox:
    Firefox = singleton(Firefox)
    TABLE_EL = (By.CSS_SELECTOR, "div#chart_div table.result_table")
    GRAPH_EL = (By.TAG_NAME, 'svg')
    SERIE_SELECTOR_XPATH = "//div[@id='ddcl-series_select-ddw']/div/div[contains(label/text(), '%s')]"
    SEL_WAIT_TIME = 10 # selenium waitFor timeout in seconds

TESTDATADIR = path.join(path.split(__file__)[0],  "testdata") # directory where the files with test data are
WHITESPACE_MAP = { ord('\t'): None, ord('\n'): None, ord('\r'): None, ord(' '): None}


class ManageCommandsTest(TestCase):
    """test of management commands load_type_defs and load_stats_defs"""
    def test_call_commands(self):
        """checks manage.py commands load_type_defs and checks if saved in database correctly"""
        errmsg = "error in loading data to database by manage command load_type_defs\n"
        
        call_command("load_type_defs",  path.join(TESTDATADIR, "data_types.json"))
        
        dt = models.DataType.objects.all()
        dim = models.Dimension.objects.all()
        dimtodt = models.DimensionToDataType.objects.all()
        
        self.assertEqual(len(dt), 18, errmsg + "not all objects models.DataType were correctly stored ")
        self.assertEqual(len(dim), 22, errmsg + "not all objects models.Dimension were correctly stored")
        self.assertEqual(len(dimtodt), 21, errmsg + "not all objects models.DimensionToDataType were correctly stored")
        # 0-D datatype
        reldims = dt.get(code_name = 'number_of_domains').dimensions.all()
        self.assertEqual(len(reldims), 0, errmsg + "number_of_domains has wrong number of dimensions (expected 0, got " + str(len(reldims)) + ")")
        # 1-D datatype
        reldims = dt.get(code_name = 'registry_operations').dimensions.all()
        self.assertEqual(len(reldims), 1, errmsg + "registry_operations has wrong number of dimensions (expected 1, got " + str(len(reldims)) + ")")
        self.assertEqual(reldims[0].code_name, 'operation', errmsg + "registry_operations not correctly connected to dimension")
        # 2-D datatype
        reldims = dt.get(code_name = 'dnssec_by_registrator').dimensions.all()
        self.assertEqual(len(reldims), 2, errmsg + "dnssec_by_registrator has wrong number of dimensions (expected 2, got " + str(len(reldims)) + ")")
        self.assertEqual(set(map(lambda x: getattr(x, 'code_name'), reldims)), set(('dnssec_status', 'registrator_name')), errmsg + "dnssec_by_registrator not correctly connected to dimensions")
        
        errmsg = "error in loading data to database by manage command load_stats_defs\n"
        
        call_command("load_stats_defs", path.join(TESTDATADIR, "statistics.yaml"))
        sg = models.StatGroup.objects.all()
        stat = models.Statistics.objects.all()
        sgtostat = models.StatGroupToStatistics.objects.all()
        chpset = models.ChartParamSet.objects.all()
        
        self.assertEqual(len(sg), 7, errmsg + "not all objects models.StatGroup were correctly stored (expected 7 objects, got " + str(len(sg)) + ")")
        self.assertEqual(len(stat), 21, errmsg + "not all objects models.Statistics were correctly stored (expected 21 objects, got " + str(len(stat)) + ")")
        self.assertEqual(len(sgtostat), 22, errmsg + "not all objects models.StatGroupToStatistics were correctly stored (expected 22 objects, got " + str(len(sgtostat)) + ")")
        self.assertEqual(len(chpset), 6, errmsg + "not all objects models.ChartParamSet were correctly stored (expected 6 objects, got " + str(len(chpset)) + ")")
        # check many-to-many relation
        relstats = stat.get(code_name = 'domains_by_dnssec').statgroup_set.all()
        self.assertEqual(len(relstats), 2)
        self.assertEqual(set(map(lambda x: getattr(x, 'code_name'), relstats)), set(('domains', 'dnssec')))


class ReportDefinitionLoadingTest(TestCase):
    fixtures = ["datadef.json"]
    maxDiff = None
    
    def verifyStatistics(self, yaml_stats, db_stats, msg_prefix = u""):
        self.assertEqual(len(yaml_stats), len(db_stats), msg_prefix +
                          u"Not all statistics saved to database (expected '%d' statistics, got '%d')" %
                          (len(yaml_stats), len(db_stats)))
        db_stats = db_stats.order_by('position')
        
        for i in xrange(len(yaml_stats)):
            db_stat = db_stats[i]
            yaml_stat = yaml_stats[i]
            self.assertEqual(yaml_stat['code_name'], db_stat.statistics.code_name, msg_prefix +
                             u"Wrong statistics linked to statistic (position = '%s') (expected: '%s', got '%s')" %
                             (db_stat.position, yaml_stat['code_name'], db_stat.statistics.code_name))
            names = yaml_stat.get('name', {})
            for lang, text in names.iteritems():
                self.assertEqual(getattr(db_stat, 'name_' + lang), text, "Wrong ReportToStatistics title in the language '%s'" % lang)
            infos = yaml_stat.get('info', {})
            for lang, text in infos.iteritems():
                self.assertEqual(getattr(db_stat, 'info_' + lang), text, msg_prefix +
                                 u"Wrong ReportToStatistics info in the language '%s'" % lang)
            highlights = yaml_stat.get('highlight', {})
            for lang, text in highlights.iteritems():
                self.assertEqual(getattr(db_stat, 'highlight_' + lang), text, msg_prefix +
                                 u"Wrong ReportToStatistics highlight in the language '%s'" % lang)
            # db_stat.params == yaml_stat.get('default_params')# TODO default_params
            for paramname, paramdef in yaml_stat.get('default_params', {}).iteritems():
                db_value = getattr(db_stat.params, paramname)
                self.assertEqual(unicode(paramdef), unicode(db_value), msg_prefix +
                                 u"Wrong chart parameter '%s' for statistics '%s' (expected '%s', got '%s')." %
                                 (paramname, db_stat.statistics.code_name, unicode(paramdef), unicode(db_value)))
    
    def verifyReportDef(self, yaml_file, msg_prefix = u""):
        # load yaml definitons file
        with open(yaml_file, "rb") as fh:
            yaml_raw_data = yaml.load(fh.read())
        
        # test reports
        yaml_reports = yaml_raw_data.get('reports', {})
        yaml_repgroups = yaml_raw_data.get('report_groups', {})
        db_reports = models.Report.objects.all()
        self.assertEqual(len(yaml_reports), len(db_reports), msg_prefix +
                         u"Reports were not correctly saved to database (expected %d objects, got %d)" %
                         (len(yaml_reports), len(db_reports)))
        for code_name, definition in yaml_reports.iteritems():
            try:
                report = db_reports.get(code_name = code_name)
            except models.Report.DoesNotExist:
                self.fail("Report with code_name %s was not correctly saved in the database" % code_name)
            
            localized_names = definition.get('name', {})
            for lang, text in localized_names.iteritems():
                self.assertEqual(getattr(report, 'name_' + lang), text, msg_prefix +
                                 u"Wrong Report title in the language '%s'" % lang)
            
            localized_files = definition.get('report_file', {})
            for lang, text in localized_files.iteritems():
                file_path = getattr(report, 'report_file_' + lang).path
                file_name = path.splitext(path.split(text)[1])[0]
                self.assertTrue(file_name in path.split(file_path)[1], msg_prefix +
                                u"Wrong Report file in the language '%s'." % (lang, ))
                self.assertTrue(path.isfile(file_path), msg_prefix +
                                "File '%s' for Report '%s' does not exist." %
                                (file_path, code_name))
            #haslocfile = report.has_localized_file()
            #self.assertEqual(haslocfile, bool(localized_files),
            #                 "Report.has_localized_file gave wrong response (expected '%s' got '%s')." % (str(bool(localized_files)), str(haslocfile)))
            
            yaml_sections = definition.get('sections', ())
            # stats_not_in_sections = definition.get('statistics', ())
            num_stats_in_yaml = 0 # len(stats_not_in_sections)
            for section in yaml_sections:
                try:
                    db_section = report.reportsection_set.get(name_en = section['name']['en'])
                except models.ReportSection.DoesNotExist:
                    self.fail("ReportSection with english name '%s' was not correctly saved in the database" % section['name']['en'])
                stats = section.get('statistics', ())
                num_stats_in_yaml += len(stats)
                db_stats = db_section.reporttostatistics_set.all()
                self.assertEqual(len(stats), len(db_stats), msg_prefix +
                                 u"Not all statistics were saved for Report '%s', section (EN) '%s'" %
                                 (report.code_name, section['name']['en']))
                self.verifyStatistics(stats, db_stats)
            self.assertEqual(num_stats_in_yaml, len(report.reporttostatistics_set.all()), msg_prefix +
                             "Not all statistics defined in yaml were loaded to the database")
            
            # check connection reportgroup -> report
            for rg in report.reportgroup_set.all():
                yaml_rg_reports = map(str, yaml_repgroups.get(rg.code_name, {}).get('reports', ()))
                try:
                    self.assertIn(report.code_name, yaml_rg_reports, msg_prefix +
                                  "Connection between report group '%s' and report '%s' exists in database but not in config file." %
                                  (rg.code_name, report.code_name))
                except AttributeError: # old version of python
                    pass
            
            # check connection report -> reportgroup
            for rg_name, rg_data in yaml_repgroups.iteritems():
                if (report.code_name in rg_data.get('reports', ()) and
                    rg_name not in map(lambda x: x.code_name, report.reportgroup_set.all())):
                    self.fail("Connection between report group '%s' and report '%s' is missing (but defined in config file.)" %
                              (rg_name, report.code_name))
    

    def testCommand(self):
        """test if manage load_reports_defs command works correctly and the reports page shows information provided by the loaded config file"""
        
        # check loading of reports' definitions
        call_command("load_reports_defs", path.join(TESTDATADIR, "reports.yaml"))
        
        self.verifyReportDef(path.join(TESTDATADIR, "reports.yaml"))
        
        client = Client()
        db_reports = models.Report.objects.all()
        db_reports_names = map(lambda x: x.name_en, db_reports)
        for report in db_reports:
            report_page = client.get("/reports/%s/" % report.code_name)
            for report_name in db_reports_names:
                try:
                    self.assertContains(report_page, report_name)
                except AssertionError:
                    logger.warn("On the page '/reports/%s/' there is no menu entry for report '%s'." %
                                (report.code_name, report_name))
            # check if all statistics are shown on the webpage
            for statname in map(lambda x: x.name_en or x.statistics.name_en, report.reporttostatistics_set.all()):
                try:
                    self.assertContains(report_page, escape(statname))
                except AssertionError:
                    logger.warn("On the page '/reports/%s/' the statistic '%s' is missing." %
                                (report.code_name, statname))
        
        # load other definition and have a look if no stale definitions left
        call_command("load_reports_defs", path.join(TESTDATADIR, "reports2.yaml"), force_update = True)
        self.verifyReportDef(path.join(TESTDATADIR, "reports2.yaml"),
                              "Running load_reports_defs, adding/overwriting data, loading 'reports2.yaml' file.")
        
        call_command("load_reports_defs", path.join(TESTDATADIR, "reports3.yaml"), force_update = True)
        self.verifyReportDef(path.join(TESTDATADIR, "reports3.yaml"),
                             "Running load_reports_defs, adding/overwriting data, loading 'reports3.yaml' file.")
        
        if models.ReportSection.objects.filter(name_en = 'Domains'):
            logger.warn("Stale object ReportSection name_en='Domains' still exists in the database.")
        
        call_command("load_reports_defs", path.join(TESTDATADIR, "reports4.yaml"), force_update = True)
        self.verifyReportDef(path.join(TESTDATADIR, "reports4.yaml"),
                             "Running load_reports_defs, adding/overwriting data, loading 'reports4.yaml' file.")
        
        if models.ChartParamSet.objects.filter(code_name = 'test_region_2011_dr'):
            logger.warn("Stale object ChartParamSet code_name='test_region_2011_dr' still exists in the database.")
        
        call_command("load_reports_defs", path.join(TESTDATADIR, "reports5.yaml"), force_update = True)
        self.verifyReportDef(path.join(TESTDATADIR, "reports5.yaml"),
                             "Running load_reports_defs, adding/overwriting data, loading 'reports5.yaml' file.")
        
        if models.ReportGroup.objects.filter(name_en = 'Offline statistics'):
            logger.warn("Stale object ReportGroup name_en='Offline statistics' still exists in the database.")


class LoadDataTest(TestCase):
    """test loading csv data files"""
    fixtures = ["datadef.json"]
    
    def setUp(self):
        self.tmp_csv_dir = mkdtemp(prefix = 'djangotest')
    
    def tearDown(self):
        shutil.rmtree(self.tmp_csv_dir)
    
    def testLoad1dCSV(self):
        """Test loading of 1-dimensional CSV file"""
        with self.settings(INCOMING_DATA_DIR = self.tmp_csv_dir):
            shutil.copy(path.join(TESTDATADIR, "csv/20121204-number_of_domains.csv"), self.tmp_csv_dir)
            call_command('update_db')
            points = models.DataPoint.objects.all()
            aggpoints = models.DataPointAggreg.objects.all()
            self.assertEqual(len(points), 1)
            self.assertEqual(points[0].date, datetime(2012, 12, 04))
            self.assertEqual(points[0].amount, 1005655)
            self.assertEqual(len(aggpoints), 1)
            self.assertEqual(aggpoints[0].amount, 1005655)
            
            shutil.copy(path.join(TESTDATADIR, "csv/20121205-number_of_domains.csv"), self.tmp_csv_dir)
            shutil.copy(path.join(TESTDATADIR, "csv/20121206-number_of_domains.csv"), self.tmp_csv_dir)
            shutil.copy(path.join(TESTDATADIR, "csv/20130101-number_of_domains.csv"), self.tmp_csv_dir)
            shutil.copy(path.join(TESTDATADIR, "csv/20130102-number_of_domains.csv"), self.tmp_csv_dir)
            call_command('update_db')
            
            lastpoint = models.DataPoint.objects.get(date = datetime(2013, 01, 02))
            aggpt = models.DataPointAggreg.objects.get(date = datetime(2012, 12, 01))
            
            self.assertEqual(aggpt.amount_min, 1005655)
            self.assertEqual(aggpt.amount_max, 1006429)
            self.assertEqual(aggpt.amount_avg, 1006051)
            self.assertEqual(aggpt.amount_first, 1005655)
            self.assertEqual(aggpt.amount_last, 1006429)
            if hasattr(self, 'assertIsNone'):
                self.assertIsNone(aggpt.amount_sum) # assertIsNone added in python 2.7.
            
            self.assertEqual(aggpt.amount_median,1006071)
            self.assertEqual(aggpt.amount_middle, 1006071)
            
            # test if wrong date raises ValueError
            shutil.copy(path.join(TESTDATADIR, "csv/bad/20130229-number_of_domains.csv"), self.tmp_csv_dir)
            self.assertRaises(ValueError, call_command, 'update_db')
        
    
    def test_load_123D_csv(self):
        """test loading higher dimensional CSV files"""
        csvfiles = [x for x in listdir(path.join(TESTDATADIR, "csv")) if x[-3:] == 'csv']
        for fn in csvfiles:
            shutil.copy(path.join(TESTDATADIR, "csv", fn), self.tmp_csv_dir)
        
        with self.settings(INCOMING_DATA_DIR = self.tmp_csv_dir):
            call_command('update_db')
        
        has_data = {
            'dns_health': False,
            'dnssec_by_registrator': True,
            'domain_number_changes': True,
            'domains_by_dnssec': False,
            'domains_by_registrator': False,
            'domains_by_status': False,
            'hosting': False,
            'ipv6_as': False,
            'ipv6_as_active': False,
            'ipv6_by_hosting': False,
            'ipv6_domains': False,
            'ipv6_queries': False,
            'ipv6_resolvers': False,
            'ipv6_services': False,
            'number_of_domains': True,
            'number_of_enum_domains': False,
            'registry_operations': False,
            'registry_other_objects': False
        }
        for name, hasdata in has_data.iteritems():
            data = models.DataType.objects.get(code_name = name)
            self.assertEqual(data.has_data(), hasdata, "error in models.DataType.has_data method for code_name " + name)


class XhrClient(Client):
    def __init__(self):
        Client.__init__(self, HTTP_X_REQUESTED_WITH='XMLHttpRequest')


class JsonTests(TestCase):
    """tests json output"""
    fixtures = ["datadef.json", "jsontest.json"]
    client_class = XhrClient
    
    def test_higher_dimensions(self):
        # if a graph has more than one dimension, it must be possible to choose among them
        # django.core.urlresolvers.reverse('unifiedstatistics.views.statistics_page', None, ['dnssec_by_registrator'])
        response = self.client.get('/stats/dnssec_by_registrator/')
        self.assertContains(response, "Chart for", msg_prefix = "No 'Chart for' select present on the page where I expect it.")
        # django.core.urlresolvers.reverse('unifiedstatistics.views.statistics_page', None, ['number_of_domains'])
        response = self.client.get('/stats/number_of_domains/')
        self.assertNotContains(response, "Chart for", msg_prefix = "'Chart for' select present on the page where I don't expect it. (for 1-dimensional graph)")
        # django.core.urlresolvers.reverse('unifiedstatistics.views.statistics_page', None, ['domain_number_changes'])
        response = self.client.get('/stats/domain_number_changes/')
        self.assertNotContains(response, "Chart for", msg_prefix = "'Chart for' select present on the page where I don't expect it. (for 2-dimensional graph)")
    
    def compare_jsons(self, expected, got, exact = True):
        try:
            expected = json.loads(expected)
        except ValueError:
            return False
        
        try:
            got = json.loads(got)
        except ValueError:
            return False
        
        if not expected.has_key('data') or not got.has_key('data'):
            return False
        
        expecteddata = json.loads(expected['data'])
        gotdata = json.loads(got['data'])
        
        del expected['data']
        del got['data']
        
        if exact:
            return expecteddata == gotdata and expected == got
        else:
            return all(k in got and got[k] == v for k, v in expected.iteritems()) and expecteddata == gotdata
    
    def test_json(self):
        # this test expects that structure of json for ogra library won't change
        # if this happens, you must generate new jsonoutput.dat file 
        # following lines of code can be helpful in that case
        #
        # from json import loads as l
        # from json import dumps as d
        # from cPickle import Pickler as P
        # from cPickle import Unpickler as U
        #
        # with open('testdata/jsonoutput.dat', "rb") as f:
        #     i = U(f).load()
        # n = [(t[0], d({k: v for k, v in l(t[1]).iteritems()})) for t in i] # this simply copies the structure, change it
        # with open('testdata/jsonoutput.dat-new', "wb") as f:
        #     cPickle.Pickler(f).dump(n)
        
        with open(path.join(TESTDATADIR, 'jsonoutput.dat'), "rb") as fh:
            testoutput = cPickle.Unpickler(fh).load() # in jsonoutput.dat there are data which are expected for given fixture jsontest.json
        for case in testoutput:
            response = self.client.get(case[0])
            self.assertEqual(response['Content-Type'], 'application/json')
            self.assertTrue(self.compare_jsons(case[1], response.content, False), "error in json output (url: " + case[0] + ")\nexpected: " + case[1] + "\ngot: " + response.content)


class SvgInfo(object):
    """ SvgInfo([element]) -> Create an object from selenium.webdriver.remote.webelement.WebElement
    element (must be a svg element). This object can be used for getting Highcart graph properties
    and values as names of axes, values in stacked column graphs, and so on. If no element is given,
    take the first one on the current page.
"""
    def __init__(self, element = None):
        super(SvgInfo, self).__init__()
        self.webdriver = Firefox()
        self.svg = element or self.webdriver.find_element(By.TAG_NAME, 'svg')
        
        self.__legend = None
        self.action_chain = ActionChains(self.webdriver)
    
    def getChartTitle(self):
        """ returns chart title
"""
        try:
            title = self.svg.find_elements_by_class_name('highcharts-title')[0]
            title = title.find_element_by_tag_name('tspan')
            # we can not get text property since it may return empty string in case
            # the element is hidden (css display: none) we have to run javascript in the browser
            title = self.webdriver.execute_script("return arguments[0].firstChild.data", title)
        except IndexError:
            raise NoSuchElementException("no such data in graph")
        return title
    
    def __getAxes(self):
        return map(lambda el: el.find_elements_by_tag_name('text')[0], self.svg.find_elements_by_class_name('highcharts-axis'))
    
    def getGraphType(self):
        """ Returns graph type, only these three types can be identified:
    column_chart, line_chart, pie_chart
    other types will give undefined results (one of the three types above)
"""
        series_group = self.svg.find_element_by_class_name("highcharts-series-group")
        if series_group.find_elements_by_tag_name("rect"):
            return "column_chart"
        elif series_group.find_elements_by_xpath("./*"):
            return "line_chart"
        else:
            return "pie_chart"
    
    def getYAxisName(self):
        for axistext in self.__getAxes():
            transf = axistext.get_attribute('transform')
            if transf and transf.find("rotate") != -1:
                return axistext.find_elements_by_tag_name('tspan')[0].text
    
    def getXAxisName(self):
        for axistext in self.__getAxes():
            transf = axistext.get_attribute('transform')
            if not transf or transf.find("rotate") == -1:
                return axistext.find_elements_by_tag_name('tspan')[0].text
    
    def __getTooltipText(self):
        return map(lambda el: getattr(el, 'text'),
                   self.svg.find_elements_by_class_name('highcharts-tooltip')[0].find_elements_by_tag_name('tspan'))
    
    def getLegendItems(self):
        if self.__legend is not None:
            return self.__legend
        
        legitems = self.svg.find_elements_by_class_name('highcharts-legend-item')
        if legitems:
            self.__legend = map(lambda el: el.find_elements_by_tag_name('tspan')[0].text, legitems)
        else:
            self.__legend = ['']
        return self.__legend
    
    def getSeriesPosition(name):
        try:
            pos = self.getLegendItems().index(name)
        except IndexError:
            raise NoSuchElementException("no such data in graph")
        return pos
    
    def __runIntoGraph(self, col, serie = 0):
        try:
            series_g = self.svg.find_elements(By.CSS_SELECTOR, '.highcharts-tracker > g')[serie]
            rects = series_g.find_elements(By.TAG_NAME, 'rect')
            if rects:
                self.action_chain.move_to_element(rect[col]).perform()
            else:
                path = series_g.find_elements(By.TAG_NAME, 'path')[col]
                raise NotImplemented("can't read this type of graph")
        except IndexError:
            raise NoSuchElementException("no such data in graph")
        
    
    def getChartSerieValues(self, serie = 0):
        values = []
        try:
            series_g = self.svg.find_elements(By.CSS_SELECTOR, '.highcharts-tracker > g')[serie]
            rects = series_g.find_elements(By.TAG_NAME, 'rect')
            if rects:
                
                self.action_chain.move_to_element(rect[col]).perform()
                return self.__getTooltipText()
            else:
                path = series_g.find_elements(By.TAG_NAME, 'path')[col]
                raise NotImplemented("can't do this for the moment, unittest.SvgInfo.__runIntoGraph method not fully implemented")
        except IndexError:
            raise NoSuchElementException("no such data in graph")
        try:
            self.__runIntoGraph(col, row)
        except IndexError:
            raise NoSuchElementException("no such data in graph")
        return self.__getTooltipText()
    
    def getYAxisRange(self):
        axes = map(lambda x: x.find_elements(By.TAG_NAME, "text"), self.svg.find_elements(By.CLASS_NAME, "highcharts-axis-labels"))
        if len(axes) != 2:
            RuntimeError("strange svg format (maybe recently has changed), can't localize axes")
        if len(axes[0]) > 1:
            if axes[0][0].get_attribute("x") == axes[0][1].get_attribute("x"):
                y_axis = axes[0]
            else:
                y_axis = axes[1]
        elif len(axes[1]) > 1:
            if axes[1][0].get_attribute("x") == axes[1][1].get_attribute("x"):
                y_axis = axes[1]
            else:
                y_axis = axes[0]
        else:
            RuntimeError("can't distinguish between the axes (neither of them has more than one label)")
        
        y_axis = map(lambda x: x.text, y_axis)
        return (y_axis[0], y_axis[-1])

class ElementWatchdog(object):
    """ElementWatchdog(how, ident [,deleted]) -> returns context manager which prepares everyting for running ajax action and than waits for the specified element described by how and ident to change or to be deleted
    how (enum from selenium.webdriver.common.by.By) - method of getting object as css selector, xpath,...
    ident (string) - identification of the element on the page by the method 'how'
    deleted (bool) - if set to True end waiting also in case the element was deleted, if set to False, wait for the element to be recreated (default is False), 
"""
    def __init__(self, how, ident, deleted = False):
        super(ElementWatchdog, self).__init__()
        self.driver = Firefox()
        self.how = how
        self.ident = ident
        self.deleted = deleted
        self.uuid = str(randstring())
        element = Firefox().find_element(how, ident)
    
    def __enter__(self):
        element = self.driver.find_element(self.how, self.ident)
        self.driver.execute_script("arguments[0].waitforchange = arguments[1]", element, self.uuid)
        return element
    
    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type:
            # if any exception was raised, do not wait, but propagate the exception
            return False
        
        for i in range(SEL_WAIT_TIME):
            try:
                element = self.driver.find_element(self.how, self.ident)
            except NoSuchElementException:
                if self.deleted:
                    break
            else:
                if self.driver.execute_script("return (arguments[0].waitforchange)", element) != self.uuid:
                    break
            time.sleep(1)
        else:
            raise TimeoutException("time out limit reached, expected element " + self.ident + " hasn't been  loaded")


def skipIfNoSelenium():
    if Firefox:
        return lambda f: f
    else:
        return skip("selenium is not installed, skipping selenium-based tests")


@skipIfNoSelenium()
class SeleniumTests(LiveServerTestCase):
    fixtures = ["livetest.json"]
    maxDiff = None
    
    @classmethod
    def setUpClass(cls):
        super(SeleniumTests, cls).setUpClass()
        cls.driver = Firefox()
        cls.driver.implicitly_wait(SEL_WAIT_TIME)
        cls.testdata = []
        for filename in cls.fixtures:
            with open(path.join(TESTDATADIR, "../fixtures/", filename)) as jsondata:
                cls.testdata.extend(json.loads(jsondata.read()))
    
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super(SeleniumTests, cls).tearDownClass()
    
    def setUp(self):
        self.driver.delete_all_cookies()
        self.verificationErrors = []
        
    def is_element_present(self, how, what):
        try:
            element = self.driver.find_element(by = how, value = what)
        except NoSuchElementException:
            return False
        return element
    
    def waitForElement(self, how, ident):
        for i in range(SEL_WAIT_TIME):
            element = self.is_element_present(how, ident)
            if element:
                return element
            time.sleep(1)
        else:
            self.fail("time out limit reached, expected element " + ident + " hasn't been  loaded")
    
    def verify(self, assertion, *args):
        assert_command = getattr(self, assertion)
        try:
            getattr(self, assertion)(*args)
        except AssertionError, e:
            self.verificationErrors.append(unicode(e))
    
    def verifyTable(self, table_element, table_file):
        with open(path.join(TESTDATADIR, table_file)) as ref_tab:
            self.verify('assertEqual', table_element.text.translate(WHITESPACE_MAP), unicode(ref_tab.read(), 'utf-8').translate(WHITESPACE_MAP))
    
    def verifyLink(self, link):
        anchor = self.driver.find_element(By.ID, 'chart_permalink').get_attribute('href')
        parsedanchor = urlparse.urlparse(anchor)
        parsedlink = urlparse.urlparse(link)
        cururl = urlparse.urlparse(self.driver.current_url)
        anchorquery = urlparse.parse_qs(parsedanchor.query)
        expectedquery = urlparse.parse_qs(parsedlink.query)
        try:
            ss_select = self.driver.find_element(By.ID, 'slice_value_select')
        except NoSuchElementException:
            pass
        else:
            expectedquery['sv'] = [Select(ss_select).first_selected_option.get_attribute('value')]
        
        try:
            sv_inputs = self.driver.find_element(By.ID, 'ddcl-series_select-ddw')
        except NoSuchElementException:
            pass
        else:
            expectedquery['ss'] = [x.get_attribute('value') for x in sv_inputs.find_elements(By.TAG_NAME, "input") if self.driver.execute_script("return arguments[0].checked", x)]
        
        
        self.verify('assertEqual', cururl.path, parsedanchor.path, "wrong link to graph (wrong path),\nexpected: " + link + "\ngot: " + anchor)
        self.verify('assertEqual', expectedquery, anchorquery, "wrong link to graph (wrong query (GET parameters ),\nexpected: " + str(expectedquery) + "\ngot: " + str(anchorquery))
    
    def testDashboard(self):
        """check the graphs on dashboard"""
        db_graphs = { "chartref-" + x['fields']['code_name']: (x['fields']['name_en'], x['fields']['name_cs']) for x in self.testdata if x['model'] == "unifiedstatistics.dashboardchart"}
        
        driver = self.driver
        driver.get(self.live_server_url + "/")
        
        # assert presence of six divs which contain graphs
        map(lambda chref: self.assertTrue(self.is_element_present(By.ID, chref)), db_graphs.keys())
        
        # wait for the graphs to load
        for chref in db_graphs.iterkeys():
            self.waitForElement(By.CSS_SELECTOR, "#" + chref + " *")
        
        # check the names and axes labels in the graphs
        for chref, (name_en, name_cs) in db_graphs.iteritems():
            div = driver.find_element(By.ID, chref)
            if chref == "chartref-ipv6_domains":
                self.verify('assertRegexpMatches', div.text, "Error occured when fetching data", "For graph with no data I haven't got a message 'Error occured when fetching data'.")
            else:
                svg = SvgInfo(div.find_element(By.TAG_NAME, "svg"))
                gtitle = svg.getChartTitle()
                self.verify('assertNotEqual', gtitle.find(name_en), -1, u"Wrong graph title:\nexpected: " + name_en + "\ngot: " + gtitle)
        
        # change language to cs
        Select(driver.find_element_by_id("id_select_language")).select_by_visible_text(u"Česky (cs)")
        
        # assert presence of six divs which contain graphs
        map(lambda chref: self.assertTrue(self.is_element_present(By.ID, chref)), db_graphs.keys())
        
        # wait for the graphs to load
        for chref in db_graphs.iterkeys():
            self.waitForElement(By.CSS_SELECTOR, "#" + chref + " *")
        
        # check the names of graphs
        for chref, (name_en, name_cs) in db_graphs.iteritems():
            div = driver.find_element(By.ID, chref)
            if chref == "chartref-ipv6_domains":
                self.verify('assertRegexpMatches', div.text, "Error occured when fetching data", "For graph with no data I haven't got a message 'Error occured when fetching data'.")
            else:
                svg = SvgInfo(div.find_element(*GRAPH_EL))
                gtitle = svg.getChartTitle()
                self.verify('assertNotEqual', gtitle.find(name_cs), -1, u"Wrong graph title:\nexpected: " + name_cs + "\ngot: " + gtitle)
        
        # check if any verification errors
        self.assertEqual([], self.verificationErrors)
    
    def testStatisticsOneByOne(self):
        """test 'STATISTICS' page"""
        driver = self.driver
        driver.get(self.live_server_url + "/stats/")
        left_menu = driver.find_element(By.ID, 'accordion')
        left_menu_items = (u"Domains", u"Number of .cz domains", u"Domains by registrators", u"Domains by DNSSEC",
        u"DNSSEC", u"Domains by DNSSEC", u"DNSSEC by registrators", u"Registry", u"Registry operations")
        items_with_no_data = [x['fields']['name_en'] for x in self.testdata if x['model'] == "unifiedstatistics.statistics" and x['fields']['name_en'] not in left_menu_items]
        
        for x in left_menu_items:
            self.assertRegexpMatches(left_menu.text, x, u"Left menu item '" + x + u"' was supposed to be present on the webpage but I did not find it.")
        for x in items_with_no_data:
            try:
                self.assertNotRegexpMatches(left_menu.text, x, u"Left menu item '" + x + u"' was supposed to be hidden (since it has no data) but I found it on webpage.")
            except AssertionError, e:
                self.verificationErrors.append(unicode(e))
        
        # test 1D graph "Number of .cz domains"
        driver.find_element_by_link_text("Number of .cz domains").click()
        self.waitForElement(*GRAPH_EL)
        
        self.verifyLink("/stats/number_of_domains/?rd=2013-01-06&dr=all&tp=i-1m&ds=normal&da=chart")
        with ElementWatchdog(*GRAPH_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("individual days")
        
        svg = SvgInfo()
        self.verify('assertEqual', svg.getChartTitle(), "From 2012-12-04 to 2013-01-06", u"Wrong svg graph title")
        self.verify('assertEqual', svg.getXAxisName(), "Time", "Wrong x-axis label")
        self.verify('assertEqual', svg.getYAxisName(), "Number of domains", "Wrong y-axis label")
        self.verify('assertEqual', svg.getLegendItems(), [''])
        
        Select(driver.find_element_by_id("display_as_select")).select_by_visible_text("table")
        table = self.waitForElement(*TABLE_EL)
        self.verifyTable(table, "numberdomains_days_tab.txt")
        
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("relative difference")
        
        self.verify('assertTrue', driver.find_element(By.ID, 'scale_select').get_attribute('disabled'))
        self.verifyLink("/stats/number_of_domains/?rd=2013-01-06&dr=all&tp=reldiff&da=table")
        
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("individual months")
        
        table = self.waitForElement(*TABLE_EL)
        self.verifyTable(table, "numberdomains_months_tab.txt")
        
        # test 2D graph "Domains by registrators"
        driver.find_element_by_link_text("Domains by registrators").click()
        self.waitForElement(*GRAPH_EL)
        
        with ElementWatchdog(*GRAPH_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("individual days")
        
        self.verifyLink("/stats/domains_by_registrator/?rd=2013-01-06&dr=all&tp=i-1d&ss=0&ds=normal&da=chart")
        svg = SvgInfo()
        self.verify('assertEqual', svg.getChartTitle(), "From 2012-12-04 to 2013-01-06")
        self.verify('assertEqual', svg.getXAxisName(), "Time")
        self.verify('assertEqual', svg.getYAxisName(), "Number of domains")
        top15 = [u"1X s.r.o.", u"ACTIVE 24, s.r.o.", u"GENERAL REGISTRY, s.r.o.", u"Gransy s.r.o.", u"IGNUM, s.r.o.",
        u"INTERNET CZ, a.s.", u"Media4Web s.r.o", u"ONEsolution s.r.o.", u"Seonet Multimedia s.r.o.",
        u"Stable.cz s.r.o.", u"TELE3 s.r.o.", u"Web4U s.r.o.", u"WEDOS Internet, a.s.", u"ZONER software, a. s."]
        self.verify('assertEqual', set(svg.getLegendItems()), set(top15 + [u"Other"]))
        series = [driver.execute_script("return arguments[0].firstChild.data", x) for x in driver.find_element(By.ID, 'series_select').find_elements(By.TAG_NAME, 'option')]
        
        with open(path.join(TESTDATADIR, 'domains_by_registrators_series.txt')) as ref_tab:
            expected = set(unicode(x, 'utf-8').strip() for x in ref_tab.readlines() if x.strip())
        
        self.assertEqual(set(series), expected)
        
        with ElementWatchdog(*GRAPH_EL):
            driver.find_element_by_css_selector("span.ui-dropdownchecklist-text").click()
            driver.find_element_by_xpath(SERIE_SELECTOR_XPATH % "IGNUM").click()
            driver.find_element_by_xpath(SERIE_SELECTOR_XPATH % "INTERNET CZ").click()
            driver.find_element_by_id("chart_params").click()
        
        svg = SvgInfo()
        self.verify('assertEqual', svg.getLegendItems(), [u'IGNUM, s.r.o.', u'INTERNET CZ, a.s.'])
        self.verifyLink("/stats/domains_by_registrator/?rd=2013-01-06&dr=all&tp=i-1d&ss=38&ss=14&ds=normal&da=chart")
        
        Select(driver.find_element_by_id("display_as_select")).select_by_visible_text("table")
        table = self.waitForElement(*TABLE_EL)
        self.verifyTable(table, "ignum_internet_domains.txt")
        
        # test 3D graph DNSSEC by registrators
        driver.find_element_by_link_text("DNSSEC by registrators").click()
        self.waitForElement(*GRAPH_EL)
        
        # choose chart for "dnssec protection -> dnssec"
        dimsel = Select(driver.find_element(By.ID, "slice_dim_select"))
        if dimsel.first_selected_option.text != u"DNSSEC protection":
            with ElementWatchdog(*GRAPH_EL):
                dimsel.select_by_visible_text("DNSSEC protection")
        valsel = Select(driver.find_element(By.ID, "slice_value_select"))
        if valsel.first_selected_option.text != u"dnssec":
            with ElementWatchdog(*GRAPH_EL):
                valsel.select_by_visible_text("dnssec")
        
        # select date range all, scale percent and show individual months
        select = Select(driver.find_element(By.ID, "date_range"))
        if select.first_selected_option.text != u"maximum":
            with ElementWatchdog(*GRAPH_EL):
                select.select_by_visible_text("maximum")
        select = Select(driver.find_element(By.ID, "scale_select"))
        if select.first_selected_option.text != u"relative in %":
            with ElementWatchdog(*GRAPH_EL):
                select.select_by_visible_text("relative in %")
        select = Select(driver.find_element(By.ID, "time_processing"))
        if select.first_selected_option.text != u"individual months":
            with ElementWatchdog(*GRAPH_EL):
                select.select_by_visible_text("individual months")
        
        self.verifyLink("/stats/dnssec_by_registrator/?sd=dnssec_status&sv=4&rd=2013-01-06&dr=all&tp=i-1m&ss=0&ds=percent&da=chart")
        
        svg = SvgInfo()
        self.verify('assertEqual', svg.getChartTitle(), "dnssec, from 2012-12 to 2013-01")
        self.verify('assertEqual', svg.getXAxisName(), "Month")
        self.verify('assertEqual', svg.getYAxisName(), "Number of domains - Relative share [%]")
        top15 = [u'ACTIVE 24, s.r.o.', u'AERO Trip PRO s.r.o.', u'banan s.r.o.', u'\u010cesk\xfd server .cz s.r.o.',
        u'GENERAL REGISTRY, s.r.o.', u'Gransy s.r.o.', u'IGNUM, s.r.o.', u'INTERNET CZ, a.s.', u'ONE.CZ s.r.o.',
        u'ONEsolution s.r.o.', u'TELE3 s.r.o.', u'TERMS a.s.', u'Web4U s.r.o.', u'WEDOS Internet, a.s.', u'ZONER software, a. s.']
        self.verify('assertEqual', set(svg.getLegendItems()), set(top15))
        # verify if y axis ranges between 0 - 100% in case of all series and scale relative in %
        if svg.getGraphType() == "column_chart":
            lower, upper = svg.getYAxisRange()
            self.verify("assertTrue", int(lower) <= 0 and int(upper) >= 100, "The graph of Relative share of all series for must have range between 0 - 100% (Statistics > DNSSEC by registrators).")
        
        series = [driver.execute_script("return arguments[0].firstChild.data", x) for x in driver.find_element(By.ID, 'series_select').find_elements(By.TAG_NAME, 'option')]
        with open(path.join(TESTDATADIR, 'domains_by_registrators_series.txt')) as ref_tab:
            expected = set(unicode(x, 'utf-8').strip() for x in ref_tab.readlines() if x.strip())
        
        self.assertEqual(set(series), expected)
        
        with ElementWatchdog(*GRAPH_EL):
            driver.find_element_by_css_selector("span.ui-dropdownchecklist-text").click()
            driver.find_element_by_xpath(SERIE_SELECTOR_XPATH % "IGNUM").click()
            driver.find_element_by_xpath(SERIE_SELECTOR_XPATH % "INTERNET CZ").click()
            driver.find_element_by_id("chart_params").click()
        
        Select(driver.find_element_by_id("display_as_select")).select_by_visible_text("table")
        table = self.waitForElement(*TABLE_EL)
        self.verifyTable(table, "ignum_internet_dnssec.txt")
        
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element(By.ID, "slice_value_select")).select_by_visible_text("no_dnssec")
        
        self.verifyLink("/stats/dnssec_by_registrator/?sd=dnssec_status&sv=6&rd=2013-01-06&dr=all&tp=i-1m&ss=38&ss=14&ds=percent&da=table")
        
        select = Select(driver.find_element(By.ID, "scale_select"))
        if select.first_selected_option.text != u"normal":
            with ElementWatchdog(*TABLE_EL):
                select.select_by_visible_text("normal")
        
        table = driver.find_element(*TABLE_EL)
        self.verifyTable(table, "ignum_internet_nodnssec.txt")
        
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element(By.ID, "slice_dim_select")).select_by_visible_text("Registrator name")
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element(By.ID, "slice_value_select")).select_by_visible_text("INTERNET CZ, a.s.")
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("individual days")
        
        self.verifyLink("/stats/dnssec_by_registrator/?sd=registrator_name&sv=14&rd=2013-01-06&dr=all&tp=i-1d&ss=4&ds=normal&da=table")
        
        with ElementWatchdog(*TABLE_EL):
            driver.find_element_by_css_selector("span.ui-dropdownchecklist-text").click()
            driver.find_element_by_id("ddcl-series_select-i1").click()
            driver.find_element_by_id("chart_params").click()
        
        table = driver.find_element(*TABLE_EL)
        self.verifyTable(table, "internetas_dnssec.txt")
        
        with ElementWatchdog(*TABLE_EL):
            driver.find_element_by_css_selector("span.ui-dropdownchecklist-text").click()
            driver.find_element_by_id("ddcl-series_select-i0").click()
            driver.find_element_by_id("chart_params").click()
        
        table = driver.find_element(*TABLE_EL)
        self.verifyTable(table, "internetas_allseries.txt")
        
        with ElementWatchdog(*TABLE_EL):
            Select(driver.find_element_by_id("time_processing")).select_by_visible_text("individual months")
        
        table = driver.find_element(*TABLE_EL)
        self.verifyTable(table, "internetas_months.txt")
        
        # check if any verification errors
        self.assertEqual([], self.verificationErrors)
