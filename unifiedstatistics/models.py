# build-in modules
from datetime import datetime

# django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.cache import cache
from django.db.models.query import QuerySet
from django.utils.crypto import get_random_string

# third party imports
from transmeta import TransMeta
from guardian.models import UserObjectPermissionBase
from guardian.models import GroupObjectPermissionBase
from guardian.shortcuts import get_perms, assign_perm, remove_perm 

# local imports
from common import Consts


class Report(models.Model):
    """
    Represents a report to be shown at the report page
    """
    __metaclass__ = TransMeta

    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=64)
    report_file = models.FileField(verbose_name=_("File"),
                                 upload_to="offline_reports",
                                 null=True, blank=True)
    
    class Meta:
        translate = ('name', 'report_file')
    
    def __unicode__(self):
        if self.name:
            return self.name
        return self.code_name

    def is_offline(self):
        if hasattr(self.report_file, 'size'):
            return True
        return False
    
    def has_localized_file(self):
        if self.report_file:
            name = None
            for lang_code, _lang_name in settings.LANGUAGES:
                loc_file = getattr(self, "report_file_"+lang_code)
                if name and loc_file.name != name:
                    return True
                name = loc_file.name
            return False
        return None
    
    def get_localized_files(self):
        files = []
        for lang_code, _lang_name in settings.LANGUAGES:
            loc_file = getattr(self, "report_file_"+lang_code)
            files.append((lang_code, loc_file))
        return files


class DashboardChart(models.Model):
    """
    Represents a chart to be shown at the dashboard
    """
    __metaclass__ = TransMeta

    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=64)
    statistics = models.ForeignKey("Statistics")
    position = models.IntegerField(default=0)
    params = models.OneToOneField("ChartParamSet")

    class Meta:
        verbose_name = _("Dashboard chart")
        verbose_name_plural = _("Dashboard chart")
        translate = ('name',)


class ChartParamSetManager(models.Manager):
    def get_by_natural_key(self, code_name):
        return self.get(code_name=code_name)


class ChartParamSet(models.Model):
    """
    Represents a reference to specific params for a chart
    """
    __metaclass__ = TransMeta
    objects = ChartParamSetManager()
        
    TIME_PROC_CHOICES = (
        (Consts.TIME_AGGREG_NONE, _("Normal")),
        (Consts.TIME_AGGREG_DIFF, _("Difference")),
        (Consts.TIME_AGGREG_DIFF_REL, _("Relative difference")),
        )
    
    TIME_AGGREG_CHOICES = (
        (),
        )
    
    DATA_SCALING_CHOICES = (
        (Consts.SCALE_NORMAL, _("Normal")),
        (Consts.SCALE_RELATIVE_100, _("Relative in %")),
        )
    
    CLIENT_DATA_SCALING_CHOICES = (
        (Consts.CLIENT_SCALE_NORMAL, _("Normal")),
        (Consts.CLIENT_SCALE_LOG, _("Logarithmic")),
        )
        
    code_name = models.SlugField(unique=True)
    ref_date = models.CharField(max_length=16, default="")
    date_range = models.CharField(max_length=16)
    time_processing = models.IntegerField(choices=TIME_PROC_CHOICES,
                                          default=Consts.TIME_AGGREG_NONE)
    time_sampling = models.CharField(max_length=16, default="")
    data_scaling = models.IntegerField(choices=DATA_SCALING_CHOICES,
                                       default=Consts.SCALE_NORMAL)
    client_data_scaling = models.IntegerField(choices=CLIENT_DATA_SCALING_CHOICES,
                                              default=Consts.CLIENT_SCALE_NORMAL)
    include_other = models.BooleanField(default=False,
                        help_text=_("Should 'other' be created when limiting "
                                    "the number of series"))
    series_limit = models.IntegerField(default=0)
    one_series = models.CharField(max_length=255, null=True)
    slicing_dim = models.IntegerField(null=True)
    slicing_value = models.CharField(max_length=255, null=True)
    slicing_dim_2 = models.IntegerField(null=True)
    slicing_value_2 = models.CharField(max_length=255, null=True)
    slicing_dim_3 = models.IntegerField(null=True)
    slicing_value_3 = models.CharField(max_length=255, null=True)
    display_as = models.CharField(max_length=16, null=True)
    relative_scale_base = models.IntegerField(null=True,
                                              help_text=_("When using relative scale, this determines "
                                                          "the dimension creating the base. Defaults to "
                                                          "null using as base current series dimension."))
    
    def natural_key(self):
        return self.code_name
        
    def get_data_request_params(self):
        # we import here to avoid circular import because data_request
        # imports models
        from data_request import DataRequest
        if self.time_processing == Consts.TIME_AGGREG_NONE:
            if self.time_sampling:
                time_processing = "i-" + self.time_sampling
            else:
                time_processing = DataRequest.DEFAULT_TIME_PROCESSING
        else:
            time_processing = self.time_processing
        # scaling is synthesized from two values
        if self.data_scaling == Consts.SCALE_NORMAL and \
           self.client_data_scaling == Consts.CLIENT_SCALE_LOG:
            data_scaling = "log"
        else:
            data_scaling = self.data_scaling
        display_as = self.display_as if self.display_as else DataRequest.DEFAULT_DISPLAY_AS
        
        params = {DataRequest.KEY_REF_DATE: self.ref_date,
                  DataRequest.KEY_DATE_RANGE: self.date_range,
                  DataRequest.KEY_TIME_PROCESSING: time_processing,
                  DataRequest.KEY_DATA_SCALING: data_scaling,
                  DataRequest.KEY_SERIES_SET: self.one_series,
                  DataRequest.KEY_SERIES_LIMIT: self.series_limit,
                  DataRequest.KEY_INCLUDE_OTHER: self.include_other,
                  DataRequest.KEY_SLICING_DIM: self.slicing_dim,
                  DataRequest.KEY_SLICING_VALUE: self.slicing_value,
                  DataRequest.KEY_SLICING_DIM_2: self.slicing_dim_2,
                  DataRequest.KEY_SLICING_VALUE_2: self.slicing_value_2,
                  DataRequest.KEY_SLICING_DIM_3: self.slicing_dim_3,
                  DataRequest.KEY_SLICING_VALUE_3: self.slicing_value_3,
                  DataRequest.KEY_DISPLAY_AS: display_as,
                  DataRequest.KEY_RELATIVE_SCALE_BASE: self.relative_scale_base
                  }
        return params
    

class StatGroup(models.Model):
    """
    Represents a group of statistics
    """
    __metaclass__ = TransMeta
        
    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=64)
    statistics = models.ManyToManyField("Statistics",
                                        through="StatGroupToStatistics")
    position = models.PositiveIntegerField()
    info = models.TextField()
    
    class Meta:
        verbose_name = _("Statistics group")
        verbose_name_plural = _("Statistics group")
        translate = ('name', 'info')
        ordering = ("position",)
        
    def get_statistics(self):
        return self.statistics.order_by('statgrouptostatistics__position')
                

class ReportGroup(models.Model):
    """
    Represents a group of reports
    """
    __metaclass__ = TransMeta
        
    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=64)
    reports = models.ManyToManyField("Report",
                                        through="ReportGroupToReport")
    position = models.PositiveIntegerField()
    info = models.TextField()
    
    class Meta:
        verbose_name = _("Report group")
        verbose_name_plural = _("Report group")
        translate = ('name',)
        ordering = ("position",)
        
    def get_reports(self):
        return self.reports.order_by('reportgrouptoreport__position')


class ReportGroupToReport(models.Model):
    """
    Mapping between ReportGroup and Report providing an extra position
    attribute
    """
    report_group = models.ForeignKey(ReportGroup)
    report = models.ForeignKey('Report')
    position = models.PositiveSmallIntegerField()
    
    class Meta:
        unique_together = (("report_group", "report", "position"),)
        verbose_name = _("ReportGroup to Report mapping")
        verbose_name_plural = _("ReportGroup to Report mappings")
        ordering = ("position",)


class StatGroupToStatistics(models.Model):
    """
    Mapping between StatGroup and Statistics providing an extra position
    attribute
    """
    stat_group = models.ForeignKey(StatGroup)
    statistics = models.ForeignKey('Statistics')
    position = models.PositiveSmallIntegerField()
    
    class Meta:
        unique_together = (("stat_group", "statistics", "position"),)
        verbose_name = _("StatGroup to Statistics mapping")
        verbose_name_plural = _("StatGroup to Statistics mappings")
        ordering = ("position",)


class ReportSection(models.Model):
    """
    Represents a section of report
    """
    __metaclass__ = TransMeta
        
    name = models.CharField(max_length=64)
    report = models.ForeignKey(Report)
    position = models.PositiveSmallIntegerField()
    
    class Meta:
        unique_together = (("report", "position"),)
        verbose_name = _("Report section")
        verbose_name_plural = _("Report sections")
        translate = ('name',)
        ordering = ("position",)


class ReportToStatistics(models.Model):
    """
    Mapping between Report and Statistics providing an extra position
    attribute and parameters for statistics
    """
    __metaclass__ = TransMeta
    
    report = models.ForeignKey(Report)
    report_section = models.ForeignKey(ReportSection, null=True)
    statistics = models.ForeignKey('Statistics')
    position = models.PositiveSmallIntegerField()
    params = models.ForeignKey('ChartParamSet', null=True)
    name = models.CharField(max_length=128, blank=True, help_text=_("Title of the chart which "
                                                                    "overwrites related data type "
                                                                    "name if set."))
    info = models.TextField(blank=True, help_text=_("Textual description of chart "))
    highlight = models.TextField(blank=True, help_text=_("Highlighted information connected "
                                                         "with given statistics"))
    
    class Meta:
        unique_together = (("report", "position"),)
        verbose_name = _("Report to Statistics mapping")
        verbose_name_plural = _("Report to Statistics mappings")
        ordering = ("position",)
        translate = ("name", "info", "highlight")
        
    def __unicode__(self):
        if self.name:
            return u"{0} ({1}, {2}, {3})".format(self.name, self.statistics, self.report, self.position)
        return u"({0}, {1}, {2})".format(self.statistics, self.report, self.position)
    
    def get_data_request(self):
        # we import here to avoid circular import because data_request
        from views import _get_data_request
        drq_params = self.params.get_data_request_params() if self.params else {}
        return _get_data_request(drq_params, self.statistics.code_name, self.report)
    
    def _get_real_name(self):
        "Returns real name of statistics for given report"
        if self.name:
            return self.name
        elif self.statistics.data_type:
            return self.statistics.data_type.name
        return ""
    
    real_name = property(_get_real_name)


class StatisticsManager(models.Manager):
    def get_by_natural_key(self, code_name):
        return self.get(code_name=code_name)


class Statistics(models.Model):
    """Represents one statistics such as it is displayed on the web"""
    __metaclass__ = TransMeta
    objects = StatisticsManager()
    
    code_name = models.SlugField(unique=True)
    name = models.CharField(verbose_name=_("Name"), max_length=128)
    data_type = models.ForeignKey("DataType", verbose_name=_("Data type"),
                                  null=True, blank=True)
    stat_file = models.FileField(verbose_name=_("File"),
                                 upload_to="offline_stats",
                                 null=True, blank=True)
    info = models.TextField(verbose_name=_("Info"), blank=True)
    methodology = models.TextField(verbose_name=_("Methodology"), blank=True)
    default_params = models.ForeignKey(ChartParamSet, null=True, blank=True)
    enforced_params =  models.ForeignKey(ChartParamSet, null=True, blank=True, related_name='+')
    
    class Meta:
        verbose_name = _("Statistics")
        verbose_name_plural = _("Statistics")
        translate = ('name', 'info', 'methodology', 'stat_file')
        permissions = (("view_statistics", "Can see given statistics"),)
        
    def natural_key(self):
        return self.code_name
    
    def __unicode__(self):
        if self.name:
            return self.name
        return self.code_name
    
    def is_offline(self):
        if self.data_type is None:
            return True
        return False
    
    def has_localized_file(self):
        if self.stat_file:
            name = None
            for lang_code, _lang_name in settings.LANGUAGES:
                loc_file = getattr(self, "stat_file_"+lang_code)
                if name and loc_file.name != name:
                    return True
                name = loc_file.name
            return False
        return None
    
    def get_localized_files(self):
        files = []
        for lang_code, _lang_name in settings.LANGUAGES:
            loc_file = getattr(self, "stat_file_"+lang_code)
            files.append((lang_code, loc_file))
        return files

    def get_public(self):
        public_group, _unused = Group.objects.get_or_create(name='public')
        return 'view_statistics' in get_perms(public_group, self)
    
    def set_public(self, is_public):
        public_group, _unused = Group.objects.get_or_create(name='public')
        if is_public:
            assign_perm('view_statistics', public_group, self)
        else:
            remove_perm('view_statistics', public_group, self)
            
        
class StatisticsUserObjectPermission(UserObjectPermissionBase):
    """Per statistics user permissions"""
    content_object = models.ForeignKey(Statistics)


class StatisticsGroupObjectPermission(GroupObjectPermissionBase):
    """Per statistics group permissions"""
    content_object = models.ForeignKey(Statistics)


class DataType(models.Model):
    """Represents one type of data, that is a relationship between dimensions"""
    __metaclass__ = TransMeta
    
    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=128)
    amount_dim = models.ForeignKey('Dimension',
                                   related_name="amounttodatatype_set")
    recurrent = models.BooleanField(default=False,
                                    help_text="Is the amount counted from 0 in "
                                              "each time period?")
    summable_periods = models.BooleanField(default=True,
                        help_text="When recurrent, is it possible to sum "
                                  "individual values to get value for longer "
                                  "period?")
    dimensions = models.ManyToManyField("Dimension",
                                        through="DimensionToDataType")
    update_interval = models.IntegerField(null=True,
                                          help_text="Update interval measured "
                                                    "in days")
    fault_tolerance = models.IntegerField(default=0,
                                          help_text="Number of days tolerated "
                                                    "when missing new update")

    class Meta:
        verbose_name = _("Data type")
        verbose_name_plural = _("Data types")
        translate = ('name',)
        ordering = ("code_name",)
        
    def __unicode__(self):
        return self.name
        
    def get_dimensions(self):
        result = [d2dt.dimension
                  for d2dt in self.dimensiontodatatype_set.all()\
                            .select_related("dimension").order_by("position")]
        return result
    
    def time_sampling_compatible(self):
        if self.recurrent and not self.summable_periods:
            return False
        return True
    
    def has_data(self):
        return self.datapoint_set.all().exists()


class Dimension(models.Model):
    """One dimension of a certain type"""
    __metaclass__ = TransMeta
    
    TYPE_INT = 1
    TYPE_TEXT = 2
    
    DIMENSION_TYPE_CHOICES = ((TYPE_INT, _('integer')),
                              (TYPE_TEXT, _('text')),
                              )
    
    code_name = models.SlugField(unique=True)
    name = models.CharField(max_length=64)
    type = models.PositiveSmallIntegerField(choices=DIMENSION_TYPE_CHOICES)
    
    class Meta:
        verbose_name = _("Dimension")
        verbose_name_plural = _("Dimensions")
        translate = ('name',)
        ordering = ("code_name",)
        
    def __unicode__(self):
        return u"{0} ({1})".format(self.name, self.code_name)

    @classmethod
    def get_type_by_name(cls, name):
        if name in ("int", "integer"):
            return cls.TYPE_INT
        elif name in ("str", "string", "text"):
            return cls.TYPE_TEXT
        raise ValueError("Unsupported dimension type '%s'" % name)
    
    def get_type_string(self):
        if self.type == self.TYPE_INT:
            return "integer"
        elif self.type == self.TYPE_TEXT:
            return "string"
        raise ValueError("Unsupported dimension type '%d'" % self.type)
    
    def get_str_to_int_remap(self):
        result = dict([tuple(values)
                       for values in 
                       self.texttointmap_set.all().values_list("text","id")])
        return result
    
    def get_int_to_str_remap(self, lang="en"):
        result = {}
        for pk, text, local in self.texttointmap_set.all().\
                                values_list("id","text","text_local_"+lang):
            string = local if local else text
            result[pk] = string
        return result
    
    def get_int_to_str_and_custom_values_remap(self, lang="en"):
        result = {}
        for pk, text, local, cclr, cord in self.texttointmap_set.all().\
                                values_list("id","text","text_local_"+lang,
                                            "custom_color","custom_order"):
            string = local if local else text
            result[pk] = (string, cclr, cord)
        return result

    def get_int_to_locale_remap(self):
        result = {}
        for values in self.texttointmap_set.all().values():
            new_values = {}
            for key, val in values.iteritems():
                if key == "text":
                    new_values[None] = val
                elif key.startswith("text_local_"):
                    new_values[key[11:]] = val
            result[values['id']] = new_values
        return result

    def remap_value(self, value):
        if self.type == self.TYPE_INT:
            return value
        ttim = self.texttointmap_set.get(id=value)
        return ttim
    
    def value_to_int(self, value):
        if self.type == self.TYPE_INT:
            try:
                return int(value)
            except ValueError:
                return None
        try:
            ttim = self.texttointmap_set.get(text=value)
            return ttim.id
        except TextToIntMap.DoesNotExist:
            return None


class DimensionToDataType(models.Model):
    """
    Mapping between data type and its dimensions providing an extra position
    attribute
    """
    ORDER_BY_AMOUNT_ASC = 0
    ORDER_BY_LABEL_ASC = 1
    ORDER_BY_AMOUNT_DESC = 2
    ORDER_BY_LABEL_DESC = 3
    
    SORTING_CHOICES = ((ORDER_BY_AMOUNT_ASC, _('by amount in ascending order')),
                       (ORDER_BY_LABEL_ASC, _('by label in ascending order')),
                       (ORDER_BY_AMOUNT_DESC, _('by amount in descending order')),
                       (ORDER_BY_LABEL_DESC, _('by label in descending order')),
                       )
    
    data_type = models.ForeignKey(DataType)
    dimension = models.ForeignKey(Dimension)
    position = models.PositiveSmallIntegerField()
    partitioning = models.BooleanField(
                        default=True, 
                        help_text=_("Does this dimension "
                                    "partition the data into summable parts"))
    sorting = models.PositiveSmallIntegerField(choices=SORTING_CHOICES,
                                               null=True,
                                               default=None,
                                               help_text=_("Preferred sorting of "
                                               "connected dimension when used in "
                                               "charts without time dimensions"))
    
    class Meta:
        unique_together = (("data_type", "dimension", "position"),)
        verbose_name = _("Dimension to data type mapping")
        verbose_name_plural = _("Dimension to data type mappings")
        ordering = ("position",)

    @classmethod
    def get_sorting_by_name(cls, name):
        if name in ("amount", "amount_asc"):
            return cls.ORDER_BY_AMOUNT_ASC
        elif name in ("label", "label_asc"):
            return cls.ORDER_BY_LABEL_ASC
        elif name in ("amount_desc",):
            return cls.ORDER_BY_AMOUNT_DESC
        elif name in ("label_desc",):
            return cls.ORDER_BY_LABEL_DESC
        elif name in ("none", None):
            return None
        raise ValueError("Unsupported sorting type '%s'" % name)
    
    def get_sorting_string(self):
        if self.sorting == self.ORDER_BY_AMOUNT_ASC:
            return "amount_asc"
        elif self.sorting == self.ORDER_BY_LABEL_ASC:
            return "label_asc"
        elif self.sorting == self.ORDER_BY_AMOUNT_DESC:
            return "amount_desc"
        elif self.sorting == self.ORDER_BY_LABEL_DESC:
            return "label_desc"
        elif self.sorting == None:
            return "none"
        raise ValueError("Unsupported sorting type '%d'" % self.sorting)


class DataPoint(models.Model):
    """One data point"""

    data_type = models.ForeignKey(DataType)
    date = models.DateTimeField()
    amount = models.BigIntegerField()
    value1 = models.IntegerField(null=True)
    value2 = models.IntegerField(null=True)
    value3 = models.IntegerField(null=True)
    value4 = models.IntegerField(null=True)
    
    class Meta:
        unique_together = (("data_type", "date", "value1", "value2", "value3", "value4"),)
        verbose_name = _("Data point")
        verbose_name_plural = _("Data points")


class DataPointAggreg(models.Model):
    """One data point aggregated in time from several data points;
    serves mostly for performance improvement;
    
    time_period - a text value describing the aggregated interval;
                  must be compatible with time_sampling used above
    amount - stores the value obtained by the preferred aggregation method for
             this data type
    amount_x - stores amount values aggregated using other methods, if they make
               sense, otherwise it is null
    """

    data_type = models.ForeignKey(DataType)
    date = models.DateTimeField()
    time_period = models.CharField(max_length=16)
    amount = models.BigIntegerField()
    value1 = models.IntegerField(null=True)
    value2 = models.IntegerField(null=True)
    value3 = models.IntegerField(null=True)
    value4 = models.IntegerField(null=True)
    # optional values for different aggregation methods
    amount_min = models.BigIntegerField(null=True)
    amount_max = models.BigIntegerField(null=True)
    amount_avg = models.BigIntegerField(null=True)
    amount_first = models.BigIntegerField(null=True)
    amount_last = models.BigIntegerField(null=True)
    amount_sum = models.BigIntegerField(null=True)
    amount_median = models.BigIntegerField(null=True)
    amount_middle = models.BigIntegerField(null=True)
    
    class Meta:
        unique_together = (("data_type", "date", "time_period", "value1",
                            "value2", "value3", "value4"),)
        verbose_name = _("Data point aggregation")
        verbose_name_plural = _("Data points aggregations")


class TextToIntMap(models.Model):
    """
    Mapping between string and integer values for a specific dimension
    """
    __metaclass__ = TransMeta
        
    id = models.AutoField(primary_key=True)
    dimension = models.ForeignKey(Dimension)
    text = models.CharField(max_length=255, db_index=True)
    text_local = models.CharField(max_length=255)
    custom_color = models.CharField(max_length=16, default="")
    custom_order = models.IntegerField(default=0)

    class Meta:
        verbose_name = _("Text to int map")
        verbose_name_plural = _("Text to int maps")
        unique_together = (("dimension", "text"),)
        translate = ("text_local",)
        
    def __unicode__(self):
        if self.text_local:
            return self.text_local
        return self.text


def delete_cached_menus():
    for lang, _ in settings.LANGUAGES:
        cache.delete('navigation_levels-%s' % lang)


class MenuUnCacheQuerySet(QuerySet):
    def delete(self, *args, **kwargs):
        delete_cached_menus()
        super(MenuUnCacheQuerySet, self).delete(*args, **kwargs)
        
    def update(self, *args, **kwargs):
        delete_cached_menus()
        super(MenuUnCacheQuerySet, self).update(*args, **kwargs)


class MenuItemManager(models.Manager):
    def get_query_set(self):    
        return MenuUnCacheQuerySet(self.model)


class MenuItem(models.Model):

    __metaclass__ = TransMeta

    label = models.CharField(max_length=255,
                             help_text=_("The display name on the web site."))
    level = models.PositiveIntegerField()
    position = models.PositiveIntegerField()
    active = models.BooleanField(default=True)
    href = models.CharField(editable=False, max_length=255)
    objects = MenuItemManager()

    class Meta:
        unique_together = (("level", "position"),)
        ordering = ('level', 'position')
        translate = ('label', 'href')


class AccessKey(models.Model):
    """
    Access key enables logged users to gain access to private statistics. It actually adds user
    to group connected with key, which in turn enables used to see statistics for given group.
    Usage of access key can constrained by time when it is valid and number of uses.
    """

    secret = models.CharField(max_length=16, blank=True, unique=True,
                              default=lambda: get_random_string(16))
    valid_until = models.DateTimeField(null=True, blank=True)
    remaining_uses = models.PositiveIntegerField(default=1, null=True, blank=True)
    group = models.ForeignKey(Group)

    def is_valid(self):
        if (self.valid_until and self.valid_until < datetime.now()) or \
                (self.remaining_uses == 0):
            return False
        return True
    is_valid.boolean = True

    def apply_key(self, user):
        """
        Returns pair (group, added_to_group):

        * group - group associated with key (False if used is not authenticated
                  or key is invalid)
        * added_to_group - True, if used was added to this group, False if user
                           was already member
        """
        if (not self.group) or \
                (not user.is_authenticated()) or \
                (not self.is_valid()):
            return (False, False)
        if not user in self.group.user_set.all():
            self.group.user_set.add(user)
            if self.remaining_uses:
                self.remaining_uses -= 1
                self.save()
            return (self.group, True)
        else:
            return (self.group, False)

    def __unicode__(self):
        if self.group:
            return "%s: %s" % (self.group.name, self.secret)
        return self.secret
