#!/usr/bin/env python

import os
import glob

from freddist.core import setup

PROJECT_NAME = 'stat_server'

data_files = []
data_files.extend([(os.path.join('DATADIR', root), [os.path.join(root, file_)
                                                     for file_ in files]) for root,_dir,files in list(os.walk('unifiedstatistics/static/js'))])
data_files.extend([(os.path.join('DATADIR', root), [os.path.join(root, file_)
                                                     for file_ in files]) for root,_dir,files in list(os.walk('unifiedstatistics/static/img'))])
data_files.extend([(os.path.join('DATADIR', root), [os.path.join(root, file_)
                                                     for file_ in files]) for root,_dir,files in list(os.walk('unifiedstatistics/static/css'))])
data_files.extend([(os.path.join('SYSCONFDIR', PROJECT_NAME, 'settings'), glob.glob('stat_server/settings/*.py') +
                                                                          glob.glob('stat_server/settings/*.example'))])
data_files.extend([(os.path.join('SYSCONFDIR', PROJECT_NAME), ['data/apache2.conf'])])

package_data = {}
package_data['unifiedstatistics'] = ['templates/*',
                                     'locale/*/LC_MESSAGES/django.*',
                                     'fixtures/*',
                                     'testdata/csv/bad/*',
                                     'testdata/csv/*.csv',
                                     'testdata/*.yaml',
                                     'testdata/*.json',
                                     'testdata/*.dat',
                                     'testdata/*.txt']

packages = ['unifiedstatistics',
            'stat_server',
            'freddist',
            'freddist.nicms',
            'freddist.command',
            'unifiedstatistics.migrations',
            'unifiedstatistics.management',
            'unifiedstatistics.management.commands']

setup(name=PROJECT_NAME,
      version='0.3',
      description='Unified statistics server for CZ.NIC',
      author='Bedrich Kosata, CZ.NIC',
      author_email='bedrich.kosata@nic.cz',
      url='https://redmine.labs.nic.cz/projects/unified-statistics',
      license='GNU General Public License (GPL)',
      packages=packages,
      platforms=['posix'],
      long_description=open('README.rst').read(),
      package_data=package_data,
      data_files=data_files) 
