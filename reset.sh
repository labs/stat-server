#!/bin/bash

python manage.py reset unifiedstatistics
python manage.py load_type_defs -f data/data_types.json
python manage.py load_stats_defs -f data/statistics.yaml
python manage.py load_reports_defs -f data/reports.yaml
python manage.py loaddata unifiedstatistics/fixtures/dashboard.json
