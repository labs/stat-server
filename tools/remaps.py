#!/usr/bin/env python

RNMAP = {
    '1API GmbH': 'REG-1API',
    '1X s.r.o.': 'REG-1X',
    'ACTIVE 24, s.r.o.': 'REG-ACTIVE24',
    'AERO Trip PRO s.r.o.': 'REG-AERO-TRIP',
    'AKREDIT, spol. s r.o.': 'REG-AKREDIT',
    'ASPone, s.r.o.': 'REG-ASPONE',
    'AXFONE s.r.o.': 'REG-AXFONE',
    'Above.com Pty. Ltd': 'REG-ABOVE',
    'Advio Network, s.r.o.': 'REG-ADVIO',
    'Ascio Technologies inc.': 'REG-ASCIO',
    'CORE ASSOCIATION': 'REG-CORE',
    'CZ.NIC, z.s.p.o.': 'REG-CZNIC',
    'Centrum Holdings s.r.o.': 'REG-CENTRUM',
    'Dial Telecom, a.s.': 'REG-DIAL-TEL',
    'EuroDNS S.A.': 'REG-EURODNS',
    'Explorer, a.s.': 'REG-EXPLORER',
    'GENERAL REGISTRY, s.r.o.': 'REG-GENREG',
    'GTS NOVERA a.s.': 'REG-NEXTRA',
    'Gandi SAS': 'REG-GANDI',
    'Gransy s.r.o.': 'REG-GRANSY',
    'IGNUM, s.r.o.': 'REG-IGNUM',
    'INTERNET CZ, a.s.': 'REG-INTERNET-CZ',
    'IP Mirror Pte Ltd': 'REG-IPMIRROR',
    'IPEX, a.s.': 'REG-IPEX',
    'InWay, a.s.': 'REG-INWAY',
    'Instra Corporation Pty Ltd': 'REG-INSTRA',
    'InterNetX GmbH': 'REG-INTERNETX',
    'KRAXNET s.r.o.': 'REG-KRAXNET',
    'Key-Systems GmbH': 'REG-KEYSYSTEMS',
    'MARCARIA.COM INTERNATIONAL INC': 'REG-MARCARIA',
    'MASANTA.COM s.r.o.': 'REG-MASANTA',
    'MATTES AD, s.r.o.': 'REG-MATTES',
    'MIRAMO spol.s r.o.': 'REG-MIRAMO',
    'MITON CZ, s.r.o.': 'REG-MITON',
    'MarkMonitor Inc.': 'REG-MARKMONITOR',
    'Marketer, s.r.o.': 'REG-MARKETER',
    'Media4Web s.r.o': 'REG-MEDIA4WEB',
    'Mesh Digital Limited': 'REG-MESH-DIGITAL',
    'MojeID': 'REG-MOJEID',
    'NETWAY.CZ s.r.o.': 'REG-NETWAY',
    'NEW MEDIA GROUP s.r.o.': 'REG-NEWMEDIA',
    'ONE.CZ s.r.o.': 'REG-ONE',
    'ONEsolution s.r.o.': 'REG-ONESOLUTION',
    'OVH, Sas': 'REG-OVH',
    'P.E.S. consulting, s.r.o.': 'REG-PES',
    'PIPNI s. r. o.': 'REG-PIPNI',
    'ProfiHOSTING s.r.o.': 'REG-WINSOFT',
    'RIO Media a.s.': 'REG-CLNET',
    'SW Systems s.r.o.': 'REG-SWS',
    'Safenames Ltd.': 'REG-SAFENAMES',
    'Seonet Multimedia s.r.o.': 'REG-SEONET',
    'SkyNet, a.s.': 'REG-SKYNET',
    'Stable.cz s.r.o.': 'REG-STABLE',
    'TELE3 s.r.o.': 'REG-TELE3',
    'TERMS a.s.': 'REG-TERMS',
    'Telef\xc3\xb3nica Czech Republic, a.s.': 'REG-CT',
    'VOLN\xc3\x9d, a. s.': 'REG-COL',
    'Variomedia AG': 'REG-VARIOMEDIA',
    'WEB SOLUTIONS ApS': 'REG-WEBSOLUTIONS',
    'WEDOS Internet, a.s.': 'REG-WEDOS',
    'Web4U s.r.o.': 'REG-WEB4U',
    'Websupport, s.r.o.': 'REG-WEBSUPPORT',
    'WinSoft Company, s.r.o.': 'REG-WINSOFT-OLD',
    'ZONER software, a. s.': 'REG-ZONER',
    'banan s.r.o.': 'REG-BANAN',
    'e-internet.cz s.r.o.': 'REG-E-INTERNET',
    'ha-vel family s.r.o.': 'REG-HA-VEL',
    'ha-vel internet s.r.o.': 'REG-HAVEL',
    'united-domains AG': 'REG-UN-DOMAINS',
    '\xc4\x8cesk\xc3\xbd registr\xc3\xa1tor, z.s.p.o.': 'REG-CESKYREG',
    '\xc4\x8cesk\xc3\xbd server .cz s.r.o.': 'REG-CES-SERVER'
}

CHANGE_REMAP = {
    'Registrace': 'registration',
    'Stornovan\xc3\xa1 registrace': 'cancelled_registration',
    'Vstoupilo do ochrann\xc3\xa9 lh\xc5\xafty': 'got_into_protection_period',
    'Zru\xc5\xa1eno (nepro\xc5\xa1lo tech. kontrolou)': 'revoked_tech_check_failed',
    'Zru\xc5\xa1eno (nezapl. udr\xc5\xbe. poplatek)': 'deleted_not_paid',
    'Zru\xc5\xa1eno (nezaplacen reg. poplatek)': 'revoked_reg_fee_not_paid',
    'Zru\xc5\xa1eno CZ.NICem': 'revoked_by_cznic',
    'Zru\xc5\xa1eno majitelem': 'revoked_by_owner'
}

OPERATION_REMAP = {
    'Chyby': 'error',
    'Obnoven\xc3\xad dom\xc3\xa9ny': 'domain_renew',
    'Registrace dom\xc3\xa9ny': 'domain_create',
    'Registrace kontaktu': 'contact_create',
    'Registrace sady jmen. server\xc5\xaf': 'nsset_create',
    'Registrace subjektu': 'entity_create',
    'Zm\xc4\x9bna dom\xc3\xa9ny': 'domain_update',
    'Zm\xc4\x9bna kontaktu': 'contact_update',
    'Zm\xc4\x9bna registr\xc3\xa1tora': 'domain_transfer',
    'Zm\xc4\x9bna sady jmen. server\xc5\xaf': 'nsset_update',
    'Zm\xc4\x9bna subjektu': 'entity_update',
    'Zru\xc5\xa1en\xc3\xad dom\xc3\xa9ny': 'domain_delete'
}

def registrar_name_remap(name):
    return RNMAP.get(name, name)

def change_remap(name):
    return CHANGE_REMAP.get(name, name)

def operation_remap(name):
    return OPERATION_REMAP.get(name, name)

