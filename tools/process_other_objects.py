# encoding: utf-8
import csv
import os

def _out_cvs(fname, data, separator="|"):
    with file(fname, 'w') as outfile:
        writer = csv.writer(outfile, delimiter=separator) 
        writer.writerows(data)
    

def process_other(infile, outdir, separator="|"):
    reader = csv.DictReader(infile, delimiter=separator)
    for record in reader:
        date = record['Den'].replace("-","")
        data = [
            ["total","subject",record.get("Celkem subjektů", 0)],
            ["deleted", "subject",record.get("Zrušených subjektů", 0)],
            ["total","contact",record.get("Celkem kontaktů", 0)],
            ["deleted","contact",record.get("Zrušených kontaktů", 0)],
            ["total","nsset",record.get("Celkem sad jmenných serverů", 0)],
            ["deleted","nsset",record.get("Zrušených sad jmenných serverů", 0)],
        ]
        _out_cvs(os.path.join(outdir, date+"-registry_other_objects.csv"), data)
    

if __name__ == "__main__":
    import sys
    fname = sys.argv[1]
    if len(sys.argv) > 2:
        outdir = sys.argv[2]
    else:
        outdir = "./"
    with file(fname, 'r') as infile:
        process_other(infile, outdir)
