import csv

def split_files(infile1, infile2, outbase, separator="|", split_col="Den"):
    reader1 = csv.DictReader(infile1, delimiter=separator)
    reader2 = csv.DictReader(infile2, delimiter=separator)
    records1 = dict([(record[split_col],record) for record in reader1])
    records2 = dict([(record[split_col],record) for record in reader2])
    for splitval, values1 in records1.iteritems():
        to_write_records = []
        values2 = records2.get(splitval, {})
        for series, val1 in values1.iteritems():
            if series and series != split_col:
                val1 = 0 if val1 in ("","-") else int(val1)
                val2 = values2.get(series, 0)
                val2 = 0 if val2 in ("","-") else int(val2)
                to_write_records.append(["dnssec", series, val2])
                to_write_records.append(["no_dnssec", series, val1-val2])

        outname = outbase.replace("{}", splitval)
        with file(outname, 'w') as outfile:
            writer = csv.writer(outfile, delimiter=separator)
            for rec in to_write_records:
                writer.writerow(rec)

if __name__ == "__main__":
    import sys
    all_regs_file = sys.argv[1]
    dnssec_regs_file = sys.argv[2]
    outbase = sys.argv[3]
    
    with file(all_regs_file, 'r') as file1, file(dnssec_regs_file, 'r') as file2:
        split_files(file1, file2, outbase)
