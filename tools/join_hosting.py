from __future__ import print_function
import os
import csv
import sys
from pprint import pprint

dirname = sys.argv[1]

def join_files(outfname, type_to_fname):
    with file(outfname, 'w') as outfile:

        writer = csv.writer(outfile, delimiter="|")
        for type_name, fname in type_to_fname.iteritems():
            if not os.path.isfile(fname):
                print("{0} file not found {1}".format(type_name, fname))
                return False
            
            with file(fname, 'r') as infile:
                importer = csv.reader(infile, delimiter="|")
                for key, value in importer:
                    writer.writerow([type_name, key, value])

    

for fname in os.listdir(dirname):
    if fname.endswith("-www.csv") and not fname.startswith("init"):
        www_fullname = os.path.join(dirname, fname)
        mx_fullname = os.path.join(dirname, fname[:-7]+"mx.csv")
        ns_fullname = os.path.join(dirname, fname[:-7]+"ns.csv")
        outname = fname[:-7]+"hosting.csv"
        print("Processing {0} to {1}".format(fname, outname))
        join_files(fname[:-7]+"hosting.csv",
                   dict(mx=mx_fullname, www=www_fullname, ns=ns_fullname))
