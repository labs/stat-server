# encoding: utf-8

import urllib2
import urllib
import os
import shutil
import sys

if len(sys.argv) > 1:
    outdir = sys.argv[1]
else:
    outdir = "./"
    
if len(sys.argv) > 2:
    from_date = sys.argv[2]
else:
    from_date = "2006-01-01"
    
if len(sys.argv) > 3:
    to_date = sys.argv[3]
else:
    to_date = "2012-12-31"
    
from_year, from_month, from_day = map(int, from_date.split("-"))
to_year, to_month, to_day = map(int, to_date.split("-"))

CSVFIX = "/usr/local/bin/csvfix"

params = dict(from_year=from_year, from_month=from_month, from_day=from_day,
              to_year=to_year, to_month=to_month, to_day=to_day,
              time_step='day', csv=1, submit=1,
              zone=1)

stats = {"number_of_enum_domains": 1,
         }

url_base = "http://www.nic.cz/stats/"

def create_fname(stat, prefix=""):
    base = "{stat}-{from_year}-{from_month:02d}-{from_day:02d}_{to_year}-"\
           "{to_month:02d}-{to_day:02d}.csv".format(stat=stat, **params)
    return os.path.join(outdir, prefix+base)

for stat, stat_id in stats.iteritems():
    params['stat_type'] = stat_id
    url = url_base + "?" + urllib.urlencode(params)
    outfname = create_fname(stat, prefix="_raw-")
    if not os.path.isfile(outfname):
        print outfname, "- downloading"
        doc = urllib2.urlopen(url)
        with file(outfname, "w") as outfile:
            outfile.write(doc.read())
        doc.close()
    else:
        print outfname, "- already exists"

    # postprocessing
    if stat == "number_of_enum_domains":
        out_csv = create_fname("number_of_enum_domains")
        print "  - creating", out_csv
        command_t = '{csvfix} order -smq -rsep "|" -f 1,2 '\
                    '{source_csv} > {out_csv}'
        command = command_t.format(csvfix=CSVFIX, out_csv=out_csv,
                                   source_csv=outfname)
        os.system(command)
    else:
        out_csv = create_fname(stat)
        print "  - creating", out_csv
        shutil.copy(outfname, out_csv)       
        

