import csv

def _out_cvs(fname, data, separator="|"):
    with file(fname, 'w') as outfile:
        writer = csv.writer(outfile, delimiter=separator) 
        writer.writerows(data)
    

def process_ipv6(infile, outbase, separator="|"):
    reader = csv.reader(infile, delimiter=separator)
    data = {}
    for name, value in reader:
        value = int(float(value)) # float is here to pare 3.14e+10
        data[name] = value

    # domains
    total = data['DOMAINS']
    ipv6_domains = data['IPV6_DOMAIN']
    ipv6_only_domains = data['IPV6_ONLY']
    ipv4_domains = total - ipv6_domains
    dualstack_domains = ipv6_domains - ipv6_only_domains
    out = [("ipv4", ipv4_domains),
           ("dualstack", dualstack_domains),
           ("ipv6", ipv6_only_domains)]
    _out_cvs(outbase.format("domains"), out)
    # domains by service
    ipv6_ns = data['IPV6_NS']
    ipv6_mx = data['IPV6_MX']
    out = [("ipv6", "mx", ipv6_mx),
           ("no_ipv6", "mx", total-ipv6_mx),
           ("ipv6", "ns", ipv6_ns),
           ("no_ipv6", "ns", total-ipv6_ns),
           ("ipv6", "domain", ipv6_domains),
           ("no_ipv6", "domain", total-ipv6_domains),
           ]
    _out_cvs(outbase.format("services"), out)
    # traffic
    total = data['QUERIES']
    ipv6_queries = data['IPV6_QUERIES']
    out = [("ipv6", ipv6_queries),
           ("ipv4", total-ipv6_queries)]
    _out_cvs(outbase.format("queries"), out)
    # recursive nameservers
    total = data['ALL_RS']
    ipv6_rs = data['IPV6_RS']
    out = [("ipv6", ipv6_rs),
           ("ipv4", total-ipv6_rs)]
    _out_cvs(outbase.format("resolvers"), out)
    # AS
    total = data["CZ_AS_ALL"]
    ipv6_only = data["CZ_AS_IPV6_ROUTE_ONLY"]
    ipv4_route = data["CZ_AS_IPV4_ROUTE"]
    ipv6_route = data["CZ_AS_IPV6_ROUTE"]
    ipv6_route_active = data["CZ_AS_IPV6_ROUTE_ACTIVE"]
    out = [("ipv6", ipv6_only),
           ("dualstack", ipv6_route-ipv6_only),
           ("ipv4", ipv4_route-ipv6_route),
           ]
    _out_cvs(outbase.format("as"), out)
    out = [("active", ipv6_route_active),
           ("inactive", ipv6_route - ipv6_route_active),]
    _out_cvs(outbase.format("as_active"), out)
    

if __name__ == "__main__":
    import sys
    import os
    for fname in sys.argv[1:]:
        base, rest = os.path.basename(fname).split("-")
        with file(fname, 'r') as infile:
            process_ipv6(infile, base+"-ipv6_{0}.csv")