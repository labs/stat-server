from __future__ import print_function
import os
import csv
import sys

def split_file(fname, outbase, separator="|", split_col="Date",
               ignore_single=False):
    with file(fname, 'r') as infile:
        reader = csv.DictReader(infile, delimiter=separator)
        for i, record in enumerate(reader):
            splitval = record.get(split_col)
            if splitval is None:
                print("No split value in record #{0}.".format(i),
                      file=sys.stderr)
            else:
                outname = outbase.replace("{}", splitval)
                with file(outname, 'w') as outfile:
                    writer = csv.writer(outfile, delimiter=separator)
                    for key, value in record.iteritems():
                        if key != split_col and (key or value):
                            if ignore_single and len(reader.fieldnames) == 2:
                                writer.writerow((value,))
                            else:
                                writer.writerow((key, value))

if __name__ == "__main__":
    from argparse import ArgumentParser

    aprsr = ArgumentParser(description="Split CSV to separate files according "
                           "to one column with unique values")
    #usage="python %(prog)s [options] dsc_data_directory")
    aprsr.add_argument("files", metavar="FILE", nargs='+', type=str,
                       help="CSV file to process")
    aprsr.add_argument("-o", "--out-basename", type=str, dest="out_basename",
                       help="Basis for the output name - {} in name will be "
                            "filled in with the split value")
    aprsr.add_argument("-s", "--separator", type=str, dest="separator",
                       default="|", help="Used CSV column separator")
    aprsr.add_argument("-c", "--split-column", type=str, dest="split_col",
                       default="Den", help="Name of column by which to split")
    aprsr.add_argument("-i", "--ignore-single-column",
                       dest="ignore_single", action='store_true',
                       default=False, help="Do not output column name if only "
                       "one column besides split-column is present")
    
    args = aprsr.parse_args()
    
    for fname in args.files:
        if not os.path.isfile(fname):
            print("File '{0}' does not exist.".format(fname), file=sys.stderr)
            continue
        if args.out_basename:
            if "{}" not in args.out_basename:
                print("out_basename must contain {}", file=sys.stderr)
                sys.exit(10)
            outbase = args.out_basename
        else:
            outbase = "{}-"+os.path.basename(fname)
        split_file(fname, outbase, separator=args.separator,
                   split_col=args.split_col, ignore_single=args.ignore_single)