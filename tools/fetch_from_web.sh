#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Usage: `basename $0` start_date end_date dest_dir"
  exit 100
fi

STARTDATE=$1
ENDDATE=$2
DESTDIR="$3"
NAMEEND="${STARTDATE}_${ENDDATE}.csv"
TMPDIR="../_tmp"
TMPTMPDIR="${TMPDIR}/tmp/"
echo "Putting data into ${DESTDIR}"
# start
mkdir -p ${TMPTMPDIR}
if [ $? -ne 0 ]; then
    echo "Cannot create temporary directory ${TMPTMPDIR}"
    exit 1
fi
mkdir -p ${DESTDIR}
if [ $? -ne 0 ]; then
    echo "Cannot create destination directory ${DESTDIR}"
    exit 1
fi
find ${TMPTMPDIR} -type f -name '*.csv' -exec rm {} \;
# ------------ everything from standard stats ------------
python download_source_data.py ${TMPDIR} ${STARTDATE} ${ENDDATE} 
# registrars
# takes domains_by_registrar and dnssec_by_registrar to calculate no_dnssec series
python process_regs.py ${TMPDIR}/domains_by_registrar-${NAMEEND} ${TMPDIR}/dnssec_by_registrar-${NAMEEND} "${TMPTMPDIR}{}-dnssec_by_registrar.csv"
python split_csv.py ${TMPDIR}/domains_by_registrar-${NAMEEND} -o "${TMPTMPDIR}{}-domains_by_registrar.csv"
# removes dashes in the date prefix
for year in `seq ${STARTDATE::4} ${ENDDATE::4}`; do
    rename.ul - "" ${TMPTMPDIR}${year}*by_registrar.csv
    rename.ul - "" ${TMPTMPDIR}${year}*by_registrar.csv
    mv ${TMPTMPDIR}${year}*.csv ${DESTDIR}
done
#rename.ul - "" ${TMPTMPDIR}*by_registrar.csv
#rename.ul - "" ${TMPTMPDIR}*by_registrar.csv
#mv ${TMPTMPDIR}*.csv ${DESTDIR}
# domains
python split_csv.py ${TMPDIR}/domains_by_status-${NAMEEND} -o "${TMPTMPDIR}{}-domains_by_status.csv"
sed -i "s/Bez DNSSEC/no_dnssec/" ${TMPDIR}/domains_by_dnssec-${NAMEEND}
sed -i "s/Zabezpečeno DNSSEC/dnssec/" ${TMPDIR}/domains_by_dnssec-${NAMEEND}
python split_csv.py ${TMPDIR}/domains_by_dnssec-${NAMEEND} -o "${TMPTMPDIR}{}-domains_by_dnssec.csv"
python split_csv.py -i ${TMPDIR}/number_of_domains-${NAMEEND} -o "${TMPTMPDIR}{}-number_of_domains.csv"
for year in `seq ${STARTDATE::4} ${ENDDATE::4}`; do
    rename.ul - "" ${TMPTMPDIR}${year}*domains*.csv
    rename.ul - "" ${TMPTMPDIR}${year}*domains*.csv
    mv ${TMPTMPDIR}${year}*.csv ${DESTDIR}
done
#rename.ul - "" ${TMPTMPDIR}*domains*.csv
#rename.ul - "" ${TMPTMPDIR}*domains*.csv
#mv ${TMPTMPDIR}*.csv ${DESTDIR}
# other
python split_csv.py ${TMPDIR}/domain_number_changes-${NAMEEND} -o "${TMPTMPDIR}{}-domain_number_changes.csv"
python split_csv.py ${TMPDIR}/registry_operations-${NAMEEND} -o "${TMPTMPDIR}{}-registry_operations.csv"
for year in `seq ${STARTDATE::4} ${ENDDATE::4}`; do
    rename.ul - "" ${TMPTMPDIR}${year}*.csv
    rename.ul - "" ${TMPTMPDIR}${year}*.csv
done
#rename.ul - "" ${TMPTMPDIR}*.csv
#rename.ul - "" ${TMPTMPDIR}*.csv
python process_other_objects.py ${TMPDIR}/number_of_other_objects-${NAMEEND} ${TMPTMPDIR}
for year in `seq ${STARTDATE::4} ${ENDDATE::4}`; do
    mv ${TMPTMPDIR}${year}*.csv ${DESTDIR}
done
#mv ${TMPTMPDIR}*.csv ${DESTDIR}
# ------- enum --------
# python download_enum_data.py ${TMPDIR} ${STARTDATE} ${ENDDATE}
# python split_csv.py -i ${TMPDIR}/number_of_enum_domains-${NAMEEND} -o "${TMPTMPDIR}{}-number_of_enum_domains.csv"
# rename.ul - "" ${TMPTMPDIR}*number_of_enum_domains.csv
# rename.ul - "" ${TMPTMPDIR}*number_of_enum_domains.csv
# ------- all the special stuff ---------
#cp ${TMPDIR}/prepared/*.csv ${DESTDIR}
# ipv6
#python process_ipv6.py ${TMPDIR}/raw/ipv6/201*.csv
#mv *.csv ${DESTDIR}
# hosting
#python join_hosting.py ${TMPDIR}/raw/hosting/
#mv *.csv ${DESTDIR}
# the grand finale
echo "Data are ready in ${DESTDIR}"

exit 0
