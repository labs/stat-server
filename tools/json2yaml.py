import json
import yaml
import sys

json_file = sys.argv[1]
yaml_file = sys.argv[2]

with file(json_file, 'r') as jsonf, file(yaml_file, 'w') as yamlf:
    data = json.loads(jsonf.read().decode("utf-8"))
    print type(data['statistics']['ipv6_as_active']['name']['cs'])
    out = yaml.safe_dump(data, encoding=None, allow_unicode=True,
                         default_flow_style=False, indent=4)
    print type(out)
    yamlf.write(out.encode('utf-8'))


