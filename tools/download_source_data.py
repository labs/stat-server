# encoding: utf-8

import urllib2
import urllib
import os
import shutil
import sys

from remaps import registrar_name_remap, change_remap, operation_remap

if len(sys.argv) > 1:
    outdir = sys.argv[1]
else:
    outdir = "./"
    
if len(sys.argv) > 2:
    from_date = sys.argv[2]
else:
    from_date = "1999-01-01"
    
if len(sys.argv) > 3:
    to_date = sys.argv[3]
else:
    to_date = "2012-12-31"
    
from_year, from_month, from_day = map(int, from_date.split("-"))
to_year, to_month, to_day = map(int, to_date.split("-"))

CSVFIX = "/usr/local/bin/csvfix"

params = dict(from_year=from_year, from_month=from_month, from_day=from_day,
              to_year=to_year, to_month=to_month, to_day=to_day,
              time_step='day', csv=1, submit=1,
              zone=2)

stats = {"number_of_domains": 1,
         "number_of_other_objects": 2,
         "domains_by_registrar": 3,
         "dnssec_by_registrar": 6,
         "domain_number_changes": 4,
         "registry_operations": 5,
         }

url_base = "http://www.nic.cz/stats/"

def create_fname(stat, prefix=""):
    base = "{stat}-{from_year}-{from_month:02d}-{from_day:02d}_{to_year}-"\
           "{to_month:02d}-{to_day:02d}.csv".format(stat=stat, **params)
    return os.path.join(outdir, prefix+base)

for stat, stat_id in stats.iteritems():
    params['stat_type'] = stat_id
    url = url_base + "?" + urllib.urlencode(params)
    outfname = create_fname(stat, prefix="_raw-")
    if not os.path.isfile(outfname):
        print outfname, "- downloading"
        doc = urllib2.urlopen(url)
        with file(outfname, "w") as outfile:
            outfile.write(doc.read())
        doc.close()
    else:
        print outfname, "- already exists"

    # postprocessing
    if stat == "number_of_domains":
        command_t = '{csvfix} exec -ifn -rsep "|" -c \'{cmd}\' {source_csv} | '\
                    '{csvfix} order -smq -rsep "|" -f {columns} >> {out_csv}'
        # dnssec
        out_csv = create_fname("domains_by_dnssec")
        print "  - creating", out_csv
        with file(out_csv, "w") as out_csv_file:
            out_csv_file.write("Den|Zabezpečeno DNSSEC|Bez DNSSEC\n")
        command = command_t.format(csvfix=CSVFIX, out_csv=out_csv,
                                   source_csv=outfname,
                                   cmd='if [ %5 = "-" ]; then echo %2; else expr %2 - %5; fi',
                                   columns="1,5,7")
        os.system(command)
        # ochrana lhuta
        out_csv = create_fname("domains_by_status")
        print "  - creating", out_csv
        with file(out_csv, "w") as out_csv_file:
            #out_csv_file.write("Den|Celkem domén|Domén v zóně|V ochranné lhůtě\n")
            out_csv_file.write("Den|total|in_zone|in_protection_period\n")
        command_t = '{csvfix} order -smq -ifn -rsep "|" -f 1:4 '\
                    '{source_csv} >> {out_csv}'
        command = command_t.format(csvfix=CSVFIX, out_csv=out_csv,
                                   source_csv=outfname)
        os.system(command)
        # total count
        out_csv = create_fname("number_of_domains")
        print "  - creating", out_csv
        command_t = '{csvfix} order -smq -rsep "|" -f 1,2 '\
                    '{source_csv} > {out_csv}'
        command = command_t.format(csvfix=CSVFIX, out_csv=out_csv,
                                   source_csv=outfname)
        os.system(command)
    elif stat == "registry_operations":
        with open(outfname, 'r') as infile:
            lines = list(infile)
            if len(lines) > 0:
                lines[0] = '|'.join(map(lambda x: operation_remap(x),
                                        lines[0].split('|')))
            remap_csv = create_fname("registry_operations", prefix='_remap-')
            with open(remap_csv, 'w') as outfile:
                for line in lines:
                    outfile.write(line)
        out_csv = create_fname("registry_operations")
        print "  - creating", out_csv
        command_t = '{csvfix} order -smq -rsep "|" -f 1,3:12 '\
                    '{source_csv} > {out_csv}'
        command = command_t.format(csvfix=CSVFIX, 
                                   out_csv=out_csv, source_csv=remap_csv)
        os.system(command)
    elif stat in ("domains_by_registrar", "dnssec_by_registrar"):
        with open(outfname, 'r') as infile:
            lines = list(infile)
            if len(lines) > 0:
                lines[0] = '|'.join(map(lambda x: registrar_name_remap(x),
                                        lines[0].split('|')))
            out_csv = create_fname(stat)
            with open(out_csv, 'w') as outfile:
                print "  - creating", out_csv
                for line in lines:
                    outfile.write(line)
    elif stat == "domain_number_changes":
        with open(outfname, 'r') as infile:
            lines = list(infile)
            if len(lines) > 0:
                lines[0] = '|'.join(map(lambda x: change_remap(x),
                                        lines[0].split('|')))
            out_csv = create_fname(stat)
            with open(out_csv, 'w') as outfile:
                print "  - creating", out_csv
                for line in lines:
                    outfile.write(line)
    else:
        out_csv = create_fname(stat)
        print "  - creating", out_csv
        shutil.copy(outfname, out_csv)

