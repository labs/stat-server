""" Common settings for stat_server project. """

import os
from django_mojeid import mojeid

BASE_URL = "/"

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Prague'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'cs'

# Keep english as default language for transmeta
TRANSMETA_DEFAULT_LANGUAGE = 'en'

SITE_ID = 1

ALLOWED_HOSTS = ['.nic.cz']

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# supported languages
gettext = lambda s: s
LANGUAGES = (
    ("cs", gettext("Czech")),
    ("en", gettext("English")),           
)

# Locales for correct sorting of unicode strings
LANGUAGE_LOCALES = {
    'cs': 'cs_CZ.UTF-8',
    'en': 'en_US.UTF-8'
}

LANGUAGE_COOKIE_DOMAIN = '.nic.cz'
LANGUAGE_COOKIE_MAX_AGE = 2 * 365 * 24 * 60 * 60 # seconds

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False


# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
ABS_CUR_DIR = os.path.abspath(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(os.path.dirname(ABS_CUR_DIR), "media") + "/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(os.path.dirname(ABS_CUR_DIR), "static") + "/"


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
#    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '-mnl(-8^mocl4sj*8soth8peya%f1%^x41ge6^j0y&amp;sa@)r+-5'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
#    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.locale.LocaleMiddleware',
    'unifiedstatistics.middleware.StatsLocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'stat_server.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'stat_server.wsgi.application'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'django.contrib.markup',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'south', # for db schema migrations
    'unifiedstatistics',
    # OpenID authentication
    'django_mojeid',
    # Per object permissions
    'guardian'
)

# OpenID and Guardian authentication backend
AUTHENTICATION_BACKENDS = (
    'django_mojeid.auth.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend'
)

ANONYMOUS_USER_ID = -1

# Create users automatically as they login
OPENID_CREATE_USERS = True

# Update user details each time user login
OPENID_UPDATE_DETAILS_FROM_SREG = True

# Allow redirecting back to an external URL
ALLOWED_EXTERNAL_OPENID_REDIRECT_DOMAINS = ['stats.nic.cz']

# Login URL
LOGIN_URL = '/openid/initiate/'
LOGIN_REDIRECT_URL = '/'

MOJEID_ATTRIBUTES = [
    mojeid.Email('auth', 'User', 'email', 'pk', updatable=True),
    mojeid.FirstName('auth', 'User', 'first_name', 'pk', updatable=True),
    mojeid.LastName('auth', 'User', 'last_name', 'pk', updatable=True),
    mojeid.LoginID('auth', 'User', 'username', 'pk')
]

# MojeID Single Sign-On
MOJEID_ENDPOINT_URL = 'https://mojeid.cz/endpoint/'
MOJEID_REGISTRATION_URL = 'https://mojeid.cz/registration/endpoint/'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "unifiedstatistics.context_processors.piwik",
    "unifiedstatistics.context_processors.navigation_levels"
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'classic': {
            'format': '%(levelname)s:%(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'classic',
            #'formatter': 'simple'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'unifiedstatistics': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'agora-cache'
    }
}

CACHE_MIDDLEWARE_ALIAS = "default"
CACHE_MIDDLEWARE_SECONDS = 120
CACHE_MIDDLEWARE_KEY_PREFIX = "unifiedstatistics"

# if you use PIWIK, override this in local settings
# URL is *without* the http:// !
PIWIK_URL = ""
PIWIK_ID = None

DEBUG = False
TEMPLATE_DEBUG = DEBUG

# directory where to look for arriving updates
INCOMING_DATA_DIR = os.path.join(os.path.dirname(ABS_CUR_DIR), "incoming") + "/"

# Access-Control-Allow-Origin header may be specified here if desired
ACCESS_CONTROL_ALLOW_ORIGIN = None

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = BASE_URL + "static/"
    
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = BASE_URL + "media/"
