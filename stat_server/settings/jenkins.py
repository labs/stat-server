""" Development settings and globals. """

from base import *

# Database connection parameters
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'unifiedstatistics',
        'USER': 'beast',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

# ADMINS - these are used for sending email in case of problems
ADMINS = ()

MANAGERS = ADMINS

# the SECRET_KEY is used in password hashing, etc. Make it unique, for example
# by using a value returned by `head -c 36 /dev/urandom | base64`
SECRET_KEY = 'QxWxotZUzZZRU2yMG0kWdXeKo7Bsap1bOryyTuJhOZuQ7Kf5'

# base URL - if you wish to have the server running under a specific url and not
# the root
BASE_URL = "/"

# URL from which static files are served
STATIC_URL= "/static/"

# URL from which media files are served
MEDIA_URL= "/media/"


# directory where to look for arriving updates
INCOMING_DATA_DIR = ""

# caching - for development it recommended not to use caching
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# debugging info - if you want to see debugging info on errors
DEBUG = True
TEMPLATE_DEBUG = DEBUG

#try:
#    import devserver
#except ImportError:
#    pass
#else:
#    INSTALLED_APPS += ('devserver',)

# Access-Control-Allow-Origin header may be specified here if desired
# it allows browsers to access data from this server from JavaScript
# having origin controlled by this header
ACCESS_CONTROL_ALLOW_ORIGIN = None

# jenkins-related config
INSTALLED_APPS += ('django_jenkins',)

# jenkins will run the tests from the following applications
PROJECT_APPS = (
    'unifiedstatistics',
    # [ appname for appname in INSTALLED_APPS if not appname.startswith( 'django')]
)

JENKINS_TASKS = (
    "django_jenkins.tasks.with_coverage",
    "django_jenkins.tasks.django_tests",   # select one django or
    #'django_jenkins.tasks.dir_tests'      # directory tests discovery
    # 'django_jenkins.tasks.run_pep8',
    # 'django_jenkins.tasks.run_pyflakes',
    # 'django_jenkins.tasks.run_jslint', # runs jshint tools over <app>/static/*/*.js files
    # 'django_jenkins.tasks.run_csslint',
    # 'django_jenkins.tasks.run_sloccount',
    # 'django_jenkins.tasks.lettuce_tests',
)