from django.conf.urls import patterns, url, include
from django.conf import settings
from django.shortcuts import redirect

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

BASE_URL_RE = r"^" + settings.BASE_URL.lstrip("/")

handler500 = 'unifiedstatistics.views.error500'

urlpatterns = patterns('',
    # set language
    url(BASE_URL_RE + 'setlang/$', 'unifiedstatistics.views.set_language'),
    # include urls from the unifiedstatistics app
    url(BASE_URL_RE, include('unifiedstatistics.urls')),
    # Redirect to home page
    url(BASE_URL_RE+ '$', lambda request: redirect('unifiedstatistics.views.dashboard')),
    # OpenID URLs
    url(BASE_URL_RE + 'openid/', include('django_mojeid.urls')),
    
    # Django admin:
    url(BASE_URL_RE + 'admin/', include(admin.site.urls)),
    # media for test client
    url(BASE_URL_RE + 'media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': 'media'}),
)
