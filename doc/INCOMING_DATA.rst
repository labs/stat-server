============================
Statistics - incoming data
============================

When data are added into the database
--------------------------------------

The ``unifiedstatistics`` Django application contains several commands, one of which is
``update_db``. This command does not take any parameters. Instead it uses
settings parameter ``INCOMING_DATA_DIR`` to determine where to look for new
data. If new data is found, it is processed, added into the database and the
data files are moved into the subdirectory ``done`` of ``INCOMING_DATA_DIR``.


There is no periodic checking of the incoming directory for new files. If such
functionality is required, it must be implemented externally via cron or similar
program.


Format of data
----------------

Arriving data must be in CSV format with "|" used as delimiter. For each time a
separate file is used. The name of the file specifies both the time for which
the data is and the associated data_type, for example
``20121128-domains_by_status.csv``.

In the CSV file each row describes one data point. Depending on the number of
dimensions, there is the corresponding amount of columns.

For 1D (not counting the time dimension) data, only a single value is present::

  1004007

For 2D data two columns are present, where the first one always specifies the
series and the second is a number::

  Registrace|796
  Zrušeno (nezapl. udrž. poplatek)|161
  Vstoupilo do ochranné lhůty|716

For 3D data three columns are present. The first two describe dimensions of the
data and occur in the same order in which dimensions are associated with the
data_type. The last column is the amount::

  www|ACTIVE 24|71629
  www|AERO Trip PRO, s.r.o.|1823
  www|AIVision, s.r.o.|346
  www|Amazon.com, Inc|30
  www|Angel hosting|9321
  ns|ACTIVE 24|112899
  ns|AERO Trip PRO, s.r.o.|1932
  ns|AIVision, s.r.o.|325
  ns|Amazon.com, Inc|1
  ns|Angel hosting|7247
  mx|ACTIVE 24|26835
  mx|AERO Trip PRO, s.r.o.|7
  mx|AIVision, s.r.o.|325
  mx|Amazon.com, Inc|2
  mx|Angel hosting|7622

While the ordering of columns is important, ordering of rows does not matter.

For data with a higher number of dimensions the same format would be used but
with higher number of columns.



